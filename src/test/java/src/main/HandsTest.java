package src.main;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;

import src.main.indicator.Indicator;
import src.main.kernel.Logics;
import src.main.kernel.Statistics;
import src.main.model.CardsEnum;
import src.main.model.Hand;
import src.main.model.Profile;
import src.main.model.Table;
import src.main.simulator.SimProfile;
import src.main.utils.Util;

public class HandsTest {

	@Test
	public void sim() {
		SimProfile p = new SimProfile(null, 1000.0, 1);
		Table table = null;
		Indicator i = null;
		List<CardsEnum> board = null;
		List<CardsEnum> hand = Arrays.asList(CardsEnum.s2,CardsEnum.c4);
		p.setWeight(new Indicator(1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0));
		
		assertTrue("-1.0 -0.68 0.0 0.6800000000000002 1.0".equals(Util.lnorm(-1) + " " + Util.lnorm(-0.5) + " " + Util.lnorm(0) + " " + Util.lnorm(0.5) + " " + Util.lnorm(1)));
		
		p.setDescriptors(Profile.KindOfCurve.Log, new Profile().new Point(1, 1, 1, 1), new Profile().new Point(1, 1, 1, 1));
		board = Arrays.asList(null,null,null,null,null);
		table = new Table().update(true, 20.0, p.getMyStack(), Arrays.asList(1.0,10.0), 0.0, Arrays.asList(20.0,40.0), 1000, 1000, 1000, Arrays.asList(1), board, hand, 1, 10.0);
		i = new Indicator(table);
		assertTrue(p.getLinearCurve(i, table) == 0.047619047619047616);
		board = Arrays.asList(CardsEnum.c2,CardsEnum.h2,CardsEnum.d2,CardsEnum.c3,CardsEnum.c4);
		table = new Table().update(true, 20.0, p.getMyStack(), Arrays.asList(1.0,10.0), 0.0, Arrays.asList(20.0,40.0), 1000, 1000, 1000, Arrays.asList(1), board, hand, 1, 10.0);
		i = new Indicator(table);
		assertTrue(p.getLinearCurve(i, table) == 0.9984917043740573);
		p.setDescriptors(Profile.KindOfCurve.Linear, new Profile().new Point(1, 1, 1, 1), new Profile().new Point(1, 1, 1, 1));
		board = Arrays.asList(null,null,null,null,null);
		table = new Table().update(true, 20.0, p.getMyStack(), Arrays.asList(1.0,10.0), 0.0, Arrays.asList(20.0,40.0), 1000, 1000, 1000, Arrays.asList(1), board, hand, 1, 10.0);
		i = new Indicator(table);
		assertTrue(p.getLogCurve(i, table) == 0.01863045923316564);
		board = Arrays.asList(CardsEnum.c2,CardsEnum.h2,CardsEnum.d2,CardsEnum.c3,CardsEnum.c4);
		table = new Table().update(true, 20.0, p.getMyStack(), Arrays.asList(1.0,10.0), 0.0, Arrays.asList(20.0,40.0), 1000, 1000, 1000, Arrays.asList(1), board, hand, 1, 10.0);
		i = new Indicator(table);
		assertTrue(p.getLogCurve(i, table) == 0.999473274133055);
		
		hand = Arrays.asList(CardsEnum.sA,CardsEnum.cA);
		p.setDescriptors(Profile.KindOfCurve.Log, new Profile().new Point(1, 1, 1, 1), new Profile().new Point(1, 1, 1, 1));
		board = Arrays.asList(null,null,null,null,null);
		table = new Table().update(true, 20.0, p.getMyStack(), Arrays.asList(1.0,10.0), 0.0, Arrays.asList(20.0,40.0), 1000, 1000, 1000, Arrays.asList(1), board, hand, 1, 10.0);		
		i = new Indicator(table);
		assertTrue(p.getLinearCurve(i, table)==1);
		board = Arrays.asList(CardsEnum.c2,CardsEnum.h2,CardsEnum.d3,CardsEnum.sA,CardsEnum.dA);
		table = new Table().update(true, 20.0, p.getMyStack(), Arrays.asList(1.0,10.0), 0.0, Arrays.asList(20.0,40.0), 1000, 1000, 1000, Arrays.asList(1), board, hand, 1, 10.0);		
		i = new Indicator(table);
		assertTrue(p.getLinearCurve(i, table)==1);
		p.setDescriptors(Profile.KindOfCurve.Linear, new Profile().new Point(1, 1, 1, 1), new Profile().new Point(1, 1, 1, 1));
		board = Arrays.asList(null,null,null,null,null);
		table = new Table().update(true, 20.0, p.getMyStack(), Arrays.asList(1.0,10.0), 0.0, Arrays.asList(20.0,40.0), 1000, 1000, 1000, Arrays.asList(1), board, hand, 1, 10.0);		
		i = new Indicator(table);
		assertTrue(p.getLogCurve(i, table)==1);
		board = Arrays.asList(CardsEnum.c2,CardsEnum.h2,CardsEnum.d3,CardsEnum.sA,CardsEnum.dA);
		table = new Table().update(true, 20.0, p.getMyStack(), Arrays.asList(1.0,10.0), 0.0, Arrays.asList(20.0,40.0), 1000, 1000, 1000, Arrays.asList(1), board, hand, 1, 10.0);		
		i = new Indicator(table);
		assertTrue(p.getLogCurve(i, table)==1);
		
		hand = Arrays.asList(CardsEnum.s2,CardsEnum.d7);
		p.setDescriptors(Profile.KindOfCurve.Log, new Profile().new Point(1, 1, 1, 1), new Profile().new Point(1, 1, 1, 1));
		board = Arrays.asList(null,null,null,null,null);
		table = new Table().update(true, 20.0, p.getMyStack(), Arrays.asList(1.0,10.0), 0.0, Arrays.asList(20.0,40.0), 1000, 1000, 1000, Arrays.asList(1), board, hand, 1, 10.0);		
		i = new Indicator(table);
		assertTrue(p.getLinearCurve(i, table)==0.0);
		board = Arrays.asList(CardsEnum.c2,CardsEnum.h2,CardsEnum.d3,CardsEnum.s2,CardsEnum.dA);
		table = new Table().update(true, 20.0, p.getMyStack(), Arrays.asList(1.0,10.0), 0.0, Arrays.asList(20.0,40.0), 1000, 1000, 1000, Arrays.asList(1), board, hand, 1, 10.0);		
		i = new Indicator(table);
		assertTrue(p.getLinearCurve(i, table)==1);
		p.setDescriptors(Profile.KindOfCurve.Linear, new Profile().new Point(1, 1, 1, 1), new Profile().new Point(1, 1, 1, 1));
		board = Arrays.asList(null,null,null,null,null);
		table = new Table().update(true, 20.0, p.getMyStack(), Arrays.asList(1.0,10.0), 0.0, Arrays.asList(20.0,40.0), 1000, 1000, 1000, Arrays.asList(1), board, hand, 1, 10.0);		
		i = new Indicator(table);
		assertTrue(p.getLogCurve(i, table)==0.0);
		board = Arrays.asList(CardsEnum.c2,CardsEnum.h2,CardsEnum.d3,CardsEnum.s2,CardsEnum.dA);
		table = new Table().update(true, 20.0, p.getMyStack(), Arrays.asList(1.0,10.0), 0.0, Arrays.asList(20.0,40.0), 1000, 1000, 1000, Arrays.asList(1), board, hand, 1, 10.0);		
		i = new Indicator(table);
		assertTrue(p.getLogCurve(i, table)==1);
	}
	
	@Test
	public void cardsOut() {
		Table table = new Table(null);
		table.board = Arrays.asList(CardsEnum.s6,CardsEnum.s7,CardsEnum.d7, CardsEnum.d5, CardsEnum.cJ);
		table.hand  = Arrays.asList(CardsEnum.dA,CardsEnum.hJ);
		assertTrue(Statistics.getNumberOfOutHands(table) == 157);
		assertTrue(Statistics.getNumberOfOutHandsPerc(table)==11.840120663650076);
		
		table.board = Arrays.asList(CardsEnum.dA,CardsEnum.d4,CardsEnum.d5, CardsEnum.cK, null);
		table.hand  = Arrays.asList(CardsEnum.d2,CardsEnum.h7);
		System.out.println(Statistics.getStrenghtImprovementOverSupremumPerc(table));
		System.out.println(Statistics.getStrenghtImprovementPerc(table));
		table.board = Arrays.asList(CardsEnum.d3,CardsEnum.d4,CardsEnum.d5, CardsEnum.cK, null);
		table.hand  = Arrays.asList(CardsEnum.c2,CardsEnum.d7);
		System.out.println(Statistics.getStrenghtImprovementOverSupremumPerc(table));
		System.out.println(Statistics.getStrenghtImprovementPerc(table));
		table.board = Arrays.asList(CardsEnum.c4,CardsEnum.c3,CardsEnum.dK, CardsEnum.h7, null);
		table.hand  = Arrays.asList(CardsEnum.c2,CardsEnum.cA);
		System.out.println(Statistics.getStrenghtImprovementOverSupremumPerc(table));
		System.out.println(Statistics.getStrenghtImprovementPerc(table));
	}
	
	@Test
	public void strengths() {
		double values[] = {
				Hand.getHandRank(Arrays.asList(CardsEnum.cT,CardsEnum.cJ,CardsEnum.cQ,CardsEnum.cK,CardsEnum.cA)).getScore(),
				Hand.getHandRank(Arrays.asList(CardsEnum.c9,CardsEnum.cT,CardsEnum.cJ,CardsEnum.cQ,CardsEnum.cK)).getScore(),
				Hand.getHandRank(Arrays.asList(CardsEnum.cA,CardsEnum.c2,CardsEnum.c3,CardsEnum.c4,CardsEnum.c5)).getScore(),

				Hand.getHandRank(Arrays.asList(CardsEnum.c9,CardsEnum.s9,CardsEnum.h9,CardsEnum.d9,CardsEnum.c2)).getScore(),
				Hand.getHandRank(Arrays.asList(CardsEnum.c2,CardsEnum.s2,CardsEnum.h2,CardsEnum.d2,CardsEnum.cA)).getScore(),
				Hand.getHandRank(Arrays.asList(CardsEnum.c2,CardsEnum.s2,CardsEnum.h2,CardsEnum.d2,CardsEnum.c5)).getScore(),
				Hand.getHandRank(Arrays.asList(CardsEnum.c2,CardsEnum.s2,CardsEnum.h2,CardsEnum.d2,CardsEnum.c4)).getScore(),
				
				Hand.getHandRank(Arrays.asList(CardsEnum.c3,CardsEnum.s3,CardsEnum.h3,CardsEnum.d2,CardsEnum.c2)).getScore(),
				Hand.getHandRank(Arrays.asList(CardsEnum.c2,CardsEnum.s2,CardsEnum.h2,CardsEnum.dA,CardsEnum.cA)).getScore(),

				Hand.getHandRank(Arrays.asList(CardsEnum.c2,CardsEnum.c3,CardsEnum.c4,CardsEnum.c6,CardsEnum.cA)).getScore(),
				Hand.getHandRank(Arrays.asList(CardsEnum.c2,CardsEnum.c4,CardsEnum.cT,CardsEnum.cQ,CardsEnum.cK)).getScore(),

				Hand.getHandRank(Arrays.asList(CardsEnum.c2,CardsEnum.s3,CardsEnum.h4,CardsEnum.d5,CardsEnum.c6)).getScore(),
				Hand.getHandRank(Arrays.asList(CardsEnum.c2,CardsEnum.s3,CardsEnum.h4,CardsEnum.d5,CardsEnum.cA)).getScore(),

				Hand.getHandRank(Arrays.asList(CardsEnum.c3,CardsEnum.s3,CardsEnum.h3,CardsEnum.d2,CardsEnum.c4)).getScore(),
				Hand.getHandRank(Arrays.asList(CardsEnum.c2,CardsEnum.s2,CardsEnum.h2,CardsEnum.dA,CardsEnum.cK)).getScore(),

				Hand.getHandRank(Arrays.asList(CardsEnum.cA,CardsEnum.sA,CardsEnum.h2,CardsEnum.d3,CardsEnum.c2)).getScore(),
				Hand.getHandRank(Arrays.asList(CardsEnum.cK,CardsEnum.sK,CardsEnum.h2,CardsEnum.dQ,CardsEnum.cQ)).getScore(),
				Hand.getHandRank(Arrays.asList(CardsEnum.cK,CardsEnum.sK,CardsEnum.hA,CardsEnum.d5,CardsEnum.c5)).getScore(),
				Hand.getHandRank(Arrays.asList(CardsEnum.c2,CardsEnum.s2,CardsEnum.h4,CardsEnum.d4,CardsEnum.c3)).getScore(),
				Hand.getHandRank(Arrays.asList(CardsEnum.c2,CardsEnum.s2,CardsEnum.h3,CardsEnum.d3,CardsEnum.cA)).getScore(),					  

				Hand.getHandRank(Arrays.asList(CardsEnum.cA,CardsEnum.s2,CardsEnum.hA,CardsEnum.d3,CardsEnum.c4)).getScore(),
				Hand.getHandRank(Arrays.asList(CardsEnum.c6,CardsEnum.s6,CardsEnum.h2,CardsEnum.d3,CardsEnum.c4)).getScore(),
				Hand.getHandRank(Arrays.asList(CardsEnum.c3,CardsEnum.s3,CardsEnum.hA,CardsEnum.dK,CardsEnum.cQ)).getScore(),

				Hand.getHandRank(Arrays.asList(CardsEnum.c9,CardsEnum.sT,CardsEnum.hJ,CardsEnum.dQ,CardsEnum.cA)).getScore(),
				Hand.getHandRank(Arrays.asList(CardsEnum.c2,CardsEnum.s4,CardsEnum.h3,CardsEnum.d6,CardsEnum.cA)).getScore(),
				Hand.getHandRank(Arrays.asList(CardsEnum.cK,CardsEnum.sQ,CardsEnum.hJ,CardsEnum.dT,CardsEnum.c8)).getScore()
		};
		double vcola[] = {125.91714389837568, 
						124.84022282382341,
						116.22485422740525,
						106.04101242537831,
						99.80512807163682,
						99.20512807163682,
						99.13846140497016,
						85.93333333333334,
						85.8,
						83.37320387338609, 
						82.83275197834236,
						61.30177530195752,
						60.224854227405245,
						44.07692107455227,
						43.85750902401777,
						40.20824249276072,
						39.937996682859236,
						39.478293503299376,
						30.86836436141339,
						29.98650924970046,
						27.062975496320977,
						19.02488025822574,
						16.794269748715813,
						13.840222823823407,
						13.373203873386089,
						12.840196793002915 };
		for (int i = 0; i < values.length; i++) {
			assertTrue(vcola[i]==values[i]);
			if (i > 0) 
				assertTrue(values[i-1]>values[i]);
		}
	}	
	
	@Test
	public void besthands() {
		List<CardsEnum> table = Arrays.asList(CardsEnum.c2,CardsEnum.sT,CardsEnum.cJ,CardsEnum.dQ,CardsEnum.cA);
		assertTrue(Logics.getBestHand(table, Arrays.asList(CardsEnum.cQ,CardsEnum.cK)).getScore()==83.91693565181174);
		assertTrue(Statistics.getNumberOfOutHandsPerc(table, Arrays.asList(CardsEnum.cQ,CardsEnum.cK))==0.0);
		assertTrue(Logics.getBestHand(table, Arrays.asList(CardsEnum.c8,CardsEnum.cK)).getScore()==83.91074031653477);
		assertTrue(Statistics.getNumberOfOutHandsPerc(table, Arrays.asList(CardsEnum.c8,CardsEnum.cK))==0.0);
		assertTrue(Logics.getBestHand(table, Arrays.asList(CardsEnum.h8,CardsEnum.dK)).getScore()==69.91714389837568);
		assertTrue(Statistics.getNumberOfOutHandsPerc(table, Arrays.asList(CardsEnum.h8,CardsEnum.dK))==3.3936651583710407);			
		assertTrue(Logics.getBestHand(table, Arrays.asList(CardsEnum.h2,CardsEnum.dK)).getScore()==69.91714389837568);
		assertTrue(Statistics.getNumberOfOutHandsPerc(table, Arrays.asList(CardsEnum.h2,CardsEnum.dK))==3.3936651583710407);
		assertTrue(Logics.getBestHand(table, Arrays.asList(CardsEnum.h8,CardsEnum.d9)).getScore()==67.76330174927114);
		assertTrue(Statistics.getNumberOfOutHandsPerc(table, Arrays.asList(CardsEnum.h8,CardsEnum.d9))==15.535444947209653);
		assertTrue(Logics.getBestHand(table, Arrays.asList(CardsEnum.hJ,CardsEnum.dJ)).getScore()==52.256042621130085);
		assertTrue(Statistics.getNumberOfOutHandsPerc(table, Arrays.asList(CardsEnum.hJ,CardsEnum.dJ))==17.11915535444947);
		assertTrue(Logics.getBestHand(table, Arrays.asList(CardsEnum.hQ,CardsEnum.dJ)).getScore()==38.947136214965724);
		assertTrue(Statistics.getNumberOfOutHandsPerc(table, Arrays.asList(CardsEnum.hQ,CardsEnum.dJ))==19.75867269984917);
		assertTrue(Logics.getBestHand(table, Arrays.asList(CardsEnum.hA,CardsEnum.h2)).getScore()==40.20846011535478);
		assertTrue(Statistics.getNumberOfOutHandsPerc(table, Arrays.asList(CardsEnum.hA,CardsEnum.h2))==18.85369532428356);
		table = Arrays.asList(CardsEnum.s6,CardsEnum.s8,CardsEnum.s9,CardsEnum.dA,CardsEnum.cA);
		assertTrue(Logics.getBestHand(table, Arrays.asList(CardsEnum.c9,CardsEnum.hA)).getScore()==96.66666666666667);
		assertTrue(Statistics.getNumberOfOutHandsPerc(table, Arrays.asList(CardsEnum.c9,CardsEnum.hA))==0.15082956259426847);
	}
	
	@Test
	public void headsup() {
		assertTrue(Statistics.getHeadsUpPerc(Arrays.asList(CardsEnum.sA,CardsEnum.hA)) == 84.93061224489796);
		assertTrue(Statistics.getHeadsUpPerc(Arrays.asList(CardsEnum.sT,CardsEnum.hT)) == 80.51755102040816);
		assertTrue(Statistics.getHeadsUpPerc(Arrays.asList(CardsEnum.s6,CardsEnum.h6)) == 68.99102040816327);
		assertTrue(Statistics.getHeadsUpPerc(Arrays.asList(CardsEnum.s5,CardsEnum.h5)) == 64.99918367346939);
		assertTrue(Statistics.getHeadsUpPerc(Arrays.asList(CardsEnum.sA,CardsEnum.hK)) == 63.31428571428572);
		assertTrue(Statistics.getHeadsUpPerc(Arrays.asList(CardsEnum.sA,CardsEnum.hJ)) == 61.159183673469386);
		assertTrue(Statistics.getHeadsUpPerc(Arrays.asList(CardsEnum.s4,CardsEnum.h4)) == 60.563265306122446);
		assertTrue(Statistics.getHeadsUpPerc(Arrays.asList(CardsEnum.h8,CardsEnum.sA)) == 59.562448979591835);
		assertTrue(Statistics.getHeadsUpPerc(Arrays.asList(CardsEnum.s7,CardsEnum.sA)) == 58.25959183673469);
		assertTrue(Statistics.getHeadsUpPerc(Arrays.asList(CardsEnum.hT,CardsEnum.hJ)) == 54.13224489795918);
	}

	@Test
	public void rankHand() {

		Table table = new Table(null);
		table.board = Arrays.asList(CardsEnum.s6,CardsEnum.s8,CardsEnum.s9, CardsEnum.dJ, CardsEnum.cJ);
		table.hand  = Arrays.asList(CardsEnum.s7,CardsEnum.sT);
		assertTrue(Logics.isNuts(table));
		assertFalse(Logics.isTopPocketPair(table));
		assertFalse(Logics.isCardsUpPocketPair(table));
		assertFalse(Logics.isSinglePocketPair(table));
		assertFalse(Logics.isFullDoubleCardsPocketPair(table));
		assertFalse(Logics.isSingleDoubleCardsPocketPair(table));
		assertFalse(Logics.isDoubleCardsPair(table));
		assertFalse(Logics.isPocketTriple(table));
		assertFalse(Logics.isFullPocketTriple(table));
		assertFalse(Logics.isTriple(table));
		assertFalse(Logics.isStraight(table));
		assertFalse(Logics.isFlush(table));
		assertFalse(Logics.isFullHouse(table));
		assertFalse(Logics.isForth(table));
		assertTrue(Logics.isStraightFlush(table));
		
		table.hand  = Arrays.asList(CardsEnum.sJ,CardsEnum.hJ);
		assertFalse(Logics.isNuts(table));
		assertFalse(Logics.isTopPocketPair(table));
		assertFalse(Logics.isCardsUpPocketPair(table));
		assertFalse(Logics.isSinglePocketPair(table));
		assertFalse(Logics.isFullDoubleCardsPocketPair(table));
		assertFalse(Logics.isSingleDoubleCardsPocketPair(table));
		assertFalse(Logics.isDoubleCardsPair(table));
		assertFalse(Logics.isPocketTriple(table));
		assertFalse(Logics.isFullPocketTriple(table));
		assertFalse(Logics.isTriple(table));
		assertFalse(Logics.isStraight(table));
		assertFalse(Logics.isFlush(table));
		assertFalse(Logics.isFullHouse(table));
		assertTrue(Logics.isForth(table));
		assertFalse(Logics.isStraightFlush(table));
		
		table.board = Arrays.asList(CardsEnum.s6,CardsEnum.s8,CardsEnum.d9, CardsEnum.sJ, CardsEnum.cJ);
		table.hand  = Arrays.asList(CardsEnum.d9,CardsEnum.dJ);
		assertTrue(Logics.isNuts(table));
		assertFalse(Logics.isTopPocketPair(table));
		assertFalse(Logics.isCardsUpPocketPair(table));
		assertFalse(Logics.isSinglePocketPair(table));
		assertFalse(Logics.isFullDoubleCardsPocketPair(table));
		assertFalse(Logics.isSingleDoubleCardsPocketPair(table));
		assertFalse(Logics.isDoubleCardsPair(table));
		assertFalse(Logics.isPocketTriple(table));
		assertFalse(Logics.isFullPocketTriple(table));
		assertFalse(Logics.isTriple(table));
		assertFalse(Logics.isStraight(table));
		assertFalse(Logics.isFlush(table));
		assertTrue(Logics.isFullHouse(table));
		assertFalse(Logics.isForth(table));
		assertFalse(Logics.isStraightFlush(table));

		table.hand  = Arrays.asList(CardsEnum.s2,CardsEnum.sA);
		assertFalse(Logics.isNuts(table));
		assertFalse(Logics.isTopPocketPair(table));
		assertFalse(Logics.isCardsUpPocketPair(table));
		assertFalse(Logics.isSinglePocketPair(table));
		assertFalse(Logics.isFullDoubleCardsPocketPair(table));
		assertFalse(Logics.isSingleDoubleCardsPocketPair(table));
		assertFalse(Logics.isDoubleCardsPair(table));
		assertFalse(Logics.isPocketTriple(table));
		assertFalse(Logics.isFullPocketTriple(table));
		assertFalse(Logics.isTriple(table));
		assertFalse(Logics.isStraight(table));
		assertTrue(Logics.isFlush(table));
		assertFalse(Logics.isFullHouse(table));
		assertFalse(Logics.isForth(table));
		assertFalse(Logics.isStraightFlush(table));
		
		table.board = Arrays.asList(CardsEnum.s6,CardsEnum.h8,CardsEnum.c9, CardsEnum.sA, CardsEnum.cK);
		table.hand  = Arrays.asList(CardsEnum.s7,CardsEnum.sT);
		assertTrue(Logics.isNuts(table));
		assertFalse(Logics.isTopPocketPair(table));
		assertFalse(Logics.isCardsUpPocketPair(table));
		assertFalse(Logics.isSinglePocketPair(table));
		assertFalse(Logics.isFullDoubleCardsPocketPair(table));
		assertFalse(Logics.isSingleDoubleCardsPocketPair(table));
		assertFalse(Logics.isDoubleCardsPair(table));
		assertFalse(Logics.isPocketTriple(table));
		assertFalse(Logics.isFullPocketTriple(table));
		assertFalse(Logics.isTriple(table));
		assertTrue(Logics.isStraight(table));
		assertFalse(Logics.isFlush(table));
		assertFalse(Logics.isFullHouse(table));
		assertFalse(Logics.isForth(table));
		assertFalse(Logics.isStraightFlush(table));
		
		table.board = Arrays.asList(CardsEnum.s3,CardsEnum.h8,CardsEnum.c9, CardsEnum.sA, CardsEnum.cK);
		table.hand  = Arrays.asList(CardsEnum.hA,CardsEnum.dA);
		assertTrue(Logics.isNuts(table));
		assertFalse(Logics.isTopPocketPair(table));
		assertFalse(Logics.isCardsUpPocketPair(table));
		assertFalse(Logics.isSinglePocketPair(table));
		assertFalse(Logics.isFullDoubleCardsPocketPair(table));
		assertFalse(Logics.isSingleDoubleCardsPocketPair(table));
		assertFalse(Logics.isDoubleCardsPair(table));
		assertTrue(Logics.isPocketTriple(table));
		assertTrue(Logics.isFullPocketTriple(table));
		assertTrue(Logics.isTriple(table));
		assertFalse(Logics.isStraight(table));
		assertFalse(Logics.isFlush(table));
		assertFalse(Logics.isFullHouse(table));
		assertFalse(Logics.isForth(table));
		assertFalse(Logics.isStraightFlush(table));
		
		table.board = Arrays.asList(CardsEnum.s3,CardsEnum.h8,CardsEnum.c9, CardsEnum.sA, CardsEnum.cA);
		table.hand  = Arrays.asList(CardsEnum.hA,CardsEnum.d5);
		assertFalse(Logics.isNuts(table));
		assertFalse(Logics.isTopPocketPair(table));
		assertFalse(Logics.isCardsUpPocketPair(table));
		assertFalse(Logics.isSinglePocketPair(table));
		assertFalse(Logics.isFullDoubleCardsPocketPair(table));
		assertFalse(Logics.isSingleDoubleCardsPocketPair(table));
		assertFalse(Logics.isDoubleCardsPair(table));
		assertTrue(Logics.isPocketTriple(table));
		assertFalse(Logics.isFullPocketTriple(table));
		assertTrue(Logics.isTriple(table));
		assertFalse(Logics.isStraight(table));
		assertFalse(Logics.isFlush(table));
		assertFalse(Logics.isFullHouse(table));
		assertFalse(Logics.isForth(table));
		assertFalse(Logics.isStraightFlush(table));
		
		table.hand  = Arrays.asList(CardsEnum.h3,CardsEnum.d5);
		assertFalse(Logics.isNuts(table));
		assertFalse(Logics.isTopPocketPair(table));
		assertFalse(Logics.isCardsUpPocketPair(table));
		assertFalse(Logics.isSinglePocketPair(table));
		assertFalse(Logics.isFullDoubleCardsPocketPair(table));
		assertTrue(Logics.isSingleDoubleCardsPocketPair(table));
		assertTrue(Logics.isDoubleCardsPair(table));
		assertFalse(Logics.isPocketTriple(table));
		assertFalse(Logics.isFullPocketTriple(table));
		assertFalse(Logics.isTriple(table));
		assertFalse(Logics.isStraight(table));
		assertFalse(Logics.isFlush(table));
		assertFalse(Logics.isFullHouse(table));
		assertFalse(Logics.isForth(table));
		assertFalse(Logics.isStraightFlush(table));
		
		table.board = Arrays.asList(CardsEnum.s3,CardsEnum.h8,CardsEnum.c9, CardsEnum.sQ, CardsEnum.cA);
		table.hand  = Arrays.asList(CardsEnum.h3,CardsEnum.d8);
		assertFalse(Logics.isNuts(table));
		assertFalse(Logics.isTopPocketPair(table));
		assertFalse(Logics.isCardsUpPocketPair(table));
		assertFalse(Logics.isSinglePocketPair(table));
		assertTrue(Logics.isFullDoubleCardsPocketPair(table));
		assertTrue(Logics.isSingleDoubleCardsPocketPair(table));
		assertTrue(Logics.isDoubleCardsPair(table));
		assertFalse(Logics.isPocketTriple(table));
		assertFalse(Logics.isFullPocketTriple(table));
		assertFalse(Logics.isTriple(table));
		assertFalse(Logics.isStraight(table));
		assertFalse(Logics.isFlush(table));
		assertFalse(Logics.isFullHouse(table));
		assertFalse(Logics.isForth(table));
		assertFalse(Logics.isStraightFlush(table));
		
		table.hand  = Arrays.asList(CardsEnum.h2,CardsEnum.d8);
		assertFalse(Logics.isNuts(table));
		assertFalse(Logics.isTopPocketPair(table));
		assertFalse(Logics.isCardsUpPocketPair(table));
		assertTrue(Logics.isSinglePocketPair(table));
		assertFalse(Logics.isFullDoubleCardsPocketPair(table));
		assertFalse(Logics.isSingleDoubleCardsPocketPair(table));
		assertFalse(Logics.isDoubleCardsPair(table));
		assertFalse(Logics.isPocketTriple(table));
		assertFalse(Logics.isFullPocketTriple(table));
		assertFalse(Logics.isTriple(table));
		assertFalse(Logics.isStraight(table));
		assertFalse(Logics.isFlush(table));
		assertFalse(Logics.isFullHouse(table));
		assertFalse(Logics.isForth(table));
		assertFalse(Logics.isStraightFlush(table));
		
		table.hand  = Arrays.asList(CardsEnum.hA,CardsEnum.d5);
		assertFalse(Logics.isNuts(table));
		assertFalse(Logics.isTopPocketPair(table));
		assertTrue(Logics.isCardsUpPocketPair(table));
		assertTrue(Logics.isSinglePocketPair(table));
		assertFalse(Logics.isFullDoubleCardsPocketPair(table));
		assertFalse(Logics.isSingleDoubleCardsPocketPair(table));
		assertFalse(Logics.isDoubleCardsPair(table));
		assertFalse(Logics.isPocketTriple(table));
		assertFalse(Logics.isFullPocketTriple(table));
		assertFalse(Logics.isTriple(table));
		assertFalse(Logics.isStraight(table));
		assertFalse(Logics.isFlush(table));
		assertFalse(Logics.isFullHouse(table));
		assertFalse(Logics.isForth(table));
		assertFalse(Logics.isStraightFlush(table));
		
		table.board = Arrays.asList(CardsEnum.s2,CardsEnum.h8,CardsEnum.c9, CardsEnum.sQ, CardsEnum.cJ);
		table.hand  = Arrays.asList(CardsEnum.hK,CardsEnum.dK);
		assertFalse(Logics.isNuts(table));
		assertTrue(Logics.isTopPocketPair(table));
		assertFalse(Logics.isCardsUpPocketPair(table));
		assertTrue(Logics.isSinglePocketPair(table));
		assertFalse(Logics.isFullDoubleCardsPocketPair(table));
		assertFalse(Logics.isSingleDoubleCardsPocketPair(table));
		assertFalse(Logics.isDoubleCardsPair(table));
		assertFalse(Logics.isPocketTriple(table));
		assertFalse(Logics.isFullPocketTriple(table));
		assertFalse(Logics.isTriple(table));
		assertFalse(Logics.isStraight(table));
		assertFalse(Logics.isFlush(table));
		assertFalse(Logics.isFullHouse(table));
		assertFalse(Logics.isForth(table));
		assertFalse(Logics.isStraightFlush(table));
	}
		
	@Test
	public void improvement() {
		Table table = new Table(null);
		table.board = Arrays.asList(null,null,null,null,null);
		assertTrue(Statistics.getStrenghtImprovementPerc(table) == 0);
		table.board = Arrays.asList(CardsEnum.s6,CardsEnum.s8,CardsEnum.s9,CardsEnum.dA,CardsEnum.cA);
		assertTrue(Statistics.getStrenghtImprovementPerc(table) == 0);
		table.board = Arrays.asList(CardsEnum.s6,CardsEnum.s8,CardsEnum.s9, null, null);
		table.hand  = Arrays.asList(CardsEnum.s7,CardsEnum.sT);
		assertTrue(Statistics.getStrenghtImprovementPerc(table) == 0.10322454897090014);
		assertTrue(Statistics.getStrenghtImprovementOverSupremumPerc(table) == 100.0);
		table.board = Arrays.asList(CardsEnum.s6,CardsEnum.s8,CardsEnum.s9,CardsEnum.dA, null);
		table.hand  = Arrays.asList(CardsEnum.sT,CardsEnum.d8);
		assertTrue(Statistics.getStrenghtImprovementPerc(table) == 44.710541092328576);
		assertTrue(Statistics.getStrenghtImprovementOverSupremumPerc(table) == 34.311855363642316);
		table.board = Arrays.asList(CardsEnum.s6,CardsEnum.s8,CardsEnum.s9,CardsEnum.dA, null);
		table.hand  = Arrays.asList(CardsEnum.hA,CardsEnum.d8);
		assertTrue(Statistics.getStrenghtImprovementPerc(table) == 10.245284715195023);
		assertTrue(Statistics.getStrenghtImprovementOverSupremumPerc(table) == 37.2123716680318);
		table.board = Arrays.asList(CardsEnum.s6,CardsEnum.s8,CardsEnum.s9, null, null);
		table.hand  = Arrays.asList(CardsEnum.s7,CardsEnum.s5);
		assertTrue(Statistics.getStrenghtImprovementPerc(table) == 0.10322454897287386);
		assertTrue(Statistics.getStrenghtImprovementOverSupremumPerc(table) == 99.15294548423701);
		table.board = Arrays.asList(CardsEnum.s5,CardsEnum.s8,CardsEnum.s9, null, null);
		table.hand  = Arrays.asList(CardsEnum.s7,CardsEnum.dA);
		assertTrue(Statistics.getStrenghtImprovementPerc(table) == 77.5187549400743);
		assertTrue(Statistics.getStrenghtImprovementOverSupremumPerc(table) == 40.46371329000036);
		table.board = Arrays.asList(CardsEnum.s7,CardsEnum.s8,CardsEnum.s9, null, null);
		table.hand  = Arrays.asList(CardsEnum.s6,CardsEnum.dA);
		assertTrue(Statistics.getStrenghtImprovementPerc(table) == 90.14237806826586);
		assertTrue(Statistics.getStrenghtImprovementOverSupremumPerc(table) == 44.42061294189113);
		table.board = Arrays.asList(CardsEnum.s7,CardsEnum.s8,CardsEnum.s9, CardsEnum.s4, null);
		table.hand  = Arrays.asList(CardsEnum.s6,CardsEnum.dA);
		assertTrue(Statistics.getStrenghtImprovementPerc(table) == 4.763091478686822);
		assertTrue(Statistics.getStrenghtImprovementOverSupremumPerc(table) == 65.77180804075513);
		table.board = Arrays.asList(CardsEnum.s9,CardsEnum.s8,CardsEnum.dK, CardsEnum.dA, null);
		table.hand  = Arrays.asList(CardsEnum.s6,CardsEnum.s7);
		assertTrue(Statistics.getStrenghtImprovementPerc(table) == 53.87188891607273);
		assertTrue(Statistics.getStrenghtImprovementOverSupremumPerc(table) == 69.6192119211948);
		table.board = Arrays.asList(CardsEnum.s7,CardsEnum.s8,CardsEnum.dK, CardsEnum.dA, null);
		table.hand  = Arrays.asList(CardsEnum.s6,CardsEnum.s9);
		assertTrue(Statistics.getStrenghtImprovementPerc(table) == 53.871946136903496);
		assertTrue(Statistics.getStrenghtImprovementOverSupremumPerc(table) == 69.61926124967597);
		table.board = Arrays.asList(CardsEnum.s9,CardsEnum.s8,CardsEnum.d7, CardsEnum.dA, null);
		table.hand  = Arrays.asList(CardsEnum.s6,CardsEnum.s7);
		assertTrue(Statistics.getStrenghtImprovementPerc(table) == 53.44238501339962);
		assertTrue(Statistics.getStrenghtImprovementOverSupremumPerc(table) == 67.10330420002589);
		table.board = Arrays.asList(CardsEnum.s6,CardsEnum.s8,CardsEnum.s9, CardsEnum.s7, null);
		table.hand  = Arrays.asList(CardsEnum.h5,CardsEnum.dT);
		assertTrue(Statistics.getStrenghtImprovementPerc(table) == 0.20644909795099045);
		assertTrue(Statistics.getStrenghtImprovementOverSupremumPerc(table) == 53.55370706158271);
		table.board = Arrays.asList(CardsEnum.s3,CardsEnum.s4,CardsEnum.s5, null, null);
		table.hand  = Arrays.asList(CardsEnum.s2,CardsEnum.h7);
		assertTrue(Statistics.getStrenghtImprovementPerc(table) == 99.41542122220513);
		assertTrue(Statistics.getStrenghtImprovementOverSupremumPerc(table) == 43.41711130597357);
		table.board = Arrays.asList(CardsEnum.sK,CardsEnum.sJ,CardsEnum.sT,CardsEnum.sA, null);
		table.hand  = Arrays.asList(CardsEnum.sQ,CardsEnum.d8);
		assertTrue(Statistics.getStrenghtImprovementPerc(table) == 2.4671622769447923E-13);
		assertTrue(Statistics.getStrenghtImprovementOverSupremumPerc(table) == 100.0);
	}
}
