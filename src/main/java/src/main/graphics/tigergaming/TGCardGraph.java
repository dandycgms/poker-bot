package src.main.graphics.tigergaming;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import src.main.model.CardsEnum;
import src.main.utils.Util;
import src.main.utils.WindowReader;

public class TGCardGraph {

	public static List<List<Integer>> numBlacksTable, numRedsTable, numBlacksHand, numRedsHand;
	
	public static void init() {
		if (numBlacksHand == null) {
			numBlacksHand = new ArrayList<List<Integer>>();
			numBlacksHand.add(Arrays.asList(48195, 31266, 41310, 41346, 16431, 28386, 31314, 32805, 21990));
			numBlacksHand.add(Arrays.asList(47094, 30894, 33762, 42840, 27177, 27240, 41544, 20298, 32181));
			numBlacksHand.add(Arrays.asList(46272, 31407, 34437, 42840, 29664, 23688, 40530, 26682, 26127));
			numBlacksHand.add(Arrays.asList(6872, 31116, 38547, 30744, 25968, 29133, 36063, 25983, 28023));
			numBlacksHand.add(Arrays.asList(47535, 29070, 38250, 41151, 21969, 29172, 40701, 26634, 28077));
			numBlacksHand.add(Arrays.asList(46872, 31116, 38547, 30744, 25968, 29133, 36063, 25983, 28023));
			numBlacksHand.add(Arrays.asList(44328, 29847, 36558, 42840, 24156, 35610, 41982, 26340, 36720));
			numBlacksHand.add(Arrays.asList(44292, 31023, 36312, 34500, 21597, 26277, 34875, 26577, 27207));
			numBlacksHand.add(Arrays.asList(44613, 30870, 37722, 31116, 27291, 21768, 40497, 25950, 31269));
			numBlacksHand.add(Arrays.asList(40392, 34758, 32307, 33138, 22413, 25566, 33084, 25263, 24336));
			numBlacksHand.add(Arrays.asList(48195, 32373, 36204, 42840, 31239, 26727, 37362, 22977, 31983));
			numBlacksHand.add(Arrays.asList(44925, 30300, 37116, 28482, 36720, 22398, 34476, 26337, 22659));
			numBlacksHand.add(Arrays.asList(41592, 32025, 30954, 34671, 14667, 36132, 33210, 20613, 24867));
		}
		if (numRedsHand == null) {
			numRedsHand = new ArrayList<List<Integer>>();
			numRedsHand.add(Arrays.asList(48195, 34430, 41310, 41817, 22821, 31012, 34945, 34038, 26628));
			numRedsHand.add(Arrays.asList(47440, 34175, 36139, 42840, 30183, 30228, 41952, 25465, 33613));
			numRedsHand.add(Arrays.asList(46877, 34528, 36602, 42840, 31887, 27791, 41258, 29844, 29464));
			numRedsHand.add(Arrays.asList(48195, 37050, 41310, 40317, 25642, 33713, 41225, 29494, 32653));
			numRedsHand.add(Arrays.asList(47744, 32926, 39214, 41682, 26616, 31552, 41374, 29809, 30801));
			numRedsHand.add(Arrays.asList(47289, 34327, 39417, 34555, 29355, 31524, 38199, 29365, 30763));
			numRedsHand.add(Arrays.asList(45545, 33456, 38055, 42840, 28115, 35958, 42252, 29608, 36720));
			numRedsHand.add(Arrays.asList(45523, 34262, 37888, 37124, 26360, 29568, 37381, 29772, 30203));
			numRedsHand.add(Arrays.asList(45742, 34159, 38852, 34812, 30261, 26476, 41234, 29348, 32986));
			numRedsHand.add(Arrays.asList(42852, 36820, 35140, 36189, 26928, 29078, 36155, 28874, 28237));
			numRedsHand.add(Arrays.asList(48195, 35188, 37814, 42840, 32962, 29874, 39089, 27304, 33477));
			numRedsHand.add(Arrays.asList(45953, 33770, 38438, 33008, 36720, 26912, 37107, 29611, 27088));
			numRedsHand.add(Arrays.asList(43671, 34948, 34217, 37242, 21616, 36317, 36242, 25686, 28600));
		}
		if (numBlacksTable == null) {
			numBlacksTable = new ArrayList<List<Integer>>();
			numBlacksTable.add(Arrays.asList(48195, 31266, 41310, 41346, 16431, 28386, 31314, 32805, 21990));
			numBlacksTable.add(Arrays.asList(47094, 30894, 33762, 42840, 27177, 27240, 41544, 20298, 32181));
			numBlacksTable.add(Arrays.asList(46272, 31407, 34437, 42840, 29664, 23688, 40530, 26682, 26127));
			numBlacksTable.add(Arrays.asList(48195, 35088, 41310, 39156, 20553, 32334, 40482, 26169, 30786));
			numBlacksTable.add(Arrays.asList(47535, 29070, 38250, 41151, 21969, 29172, 40701, 26634, 28077));
			numBlacksTable.add(Arrays.asList(46872, 31116, 38547, 30744, 25968, 29133, 36063, 25983, 28023));
			numBlacksTable.add(Arrays.asList(44328, 29847, 36558, 42840, 24156, 35610, 41982, 26340, 36720));
			numBlacksTable.add(Arrays.asList(44292, 31023, 36312, 34500, 21597, 26277, 34875, 26577, 27207));
			numBlacksTable.add(Arrays.asList(44613, 30870, 37722, 31116, 27291, 21768, 40497, 25950, 31269));
			numBlacksTable.add(Arrays.asList(40392, 34758, 32307, 33138, 22413, 25566, 33084, 25263, 24336));
			numBlacksTable.add(Arrays.asList(48195, 32373, 36204, 42840, 31239, 26727, 37362, 22977, 31983));
			numBlacksTable.add(Arrays.asList(44925, 30300, 37116, 28482, 36720, 22398, 34476, 26337, 22659));
			numBlacksTable.add(Arrays.asList(41592, 32025, 30954, 34671, 14667, 36132, 33210, 20613, 24867));		
		}
		if (numRedsTable == null) {
			numRedsTable = new ArrayList<List<Integer>>();
			numRedsTable.add(Arrays.asList(48195, 34430, 41310, 41817, 22821, 31012, 34945, 34038, 26628));
			numRedsTable.add(Arrays.asList(47440, 34175, 36139, 42840, 30183, 30228, 41952, 25465, 33613));
			numRedsTable.add(Arrays.asList(46877, 34528, 36602, 42840, 31887, 27791, 41258, 29844, 29464));
			numRedsTable.add(Arrays.asList(48195, 37050, 41310, 40317, 25642, 33713, 41225, 29494, 32653));
			numRedsTable.add(Arrays.asList(47744, 32926, 39214, 41682, 26616, 31552, 41374, 29809, 30801));
			numRedsTable.add(Arrays.asList(47289, 34327, 39417, 34555, 29355, 31524, 38199, 29365, 30763));
			numRedsTable.add(Arrays.asList(45545, 33456, 38055, 42840, 28115, 35958, 42252, 29608, 36720));
			numRedsTable.add(Arrays.asList(45523, 34262, 37888, 37124, 26360, 29568, 37381, 29772, 30203));
			numRedsTable.add(Arrays.asList(45742, 34159, 38852, 34812, 30261, 26476, 41234, 29348, 32986));
			numRedsTable.add(Arrays.asList(42852, 36820, 35140, 36189, 26928, 29078, 36155, 28874, 28237));
			numRedsTable.add(Arrays.asList(48195, 35188, 37814, 42840, 32962, 29874, 39089, 27304, 33477));
			numRedsTable.add(Arrays.asList(45953, 33770, 38438, 33008, 36720, 26912, 37107, 29611, 27088));
			numRedsTable.add(Arrays.asList(43671, 34948, 34217, 37242, 21616, 36317, 36242, 25686, 28600));
		}
	}
	
	public static List<CardsEnum> getBoardCards() {
		List<CardsEnum> ret = new ArrayList<CardsEnum>();
		List<Integer> n1 = new WindowReader(TGCardPointDef.num1Flop, TGCardPointDef.dimCard).getFeatures(3, 3), s1 = new WindowReader(TGCardPointDef.suit1Flop, TGCardPointDef.dimCard).getFeatures(3, 3);
		List<Integer> n2 = new WindowReader(TGCardPointDef.num2Flop, TGCardPointDef.dimCard).getFeatures(3, 3), s2 = new WindowReader(TGCardPointDef.suit2Flop, TGCardPointDef.dimCard).getFeatures(3, 3);
		List<Integer> n3 = new WindowReader(TGCardPointDef.num3Flop, TGCardPointDef.dimCard).getFeatures(3, 3), s3 = new WindowReader(TGCardPointDef.suit3Flop, TGCardPointDef.dimCard).getFeatures(3, 3);
		List<Integer> n4 = new WindowReader(TGCardPointDef.numTurn, TGCardPointDef.dimCard).getFeatures(3, 3),  s4 = new WindowReader(TGCardPointDef.suitTurn, TGCardPointDef.dimCard).getFeatures(3, 3);
		List<Integer> n5 = new WindowReader(TGCardPointDef.numRiver, TGCardPointDef.dimCard).getFeatures(3, 3), s5 = new WindowReader(TGCardPointDef.suitRiver, TGCardPointDef.dimCard).getFeatures(3, 3);
		ret.add(getCardFromTable(n1,s1));
		ret.add(getCardFromTable(n2,s2));
		ret.add(getCardFromTable(n3,s3));
		ret.add(getCardFromTable(n4,s4));
		ret.add(getCardFromTable(n5,s5));
		return ret;
	}
	
	public static List<CardsEnum> getHandCards() {
		List<CardsEnum> ret = new ArrayList<CardsEnum>();
		List<Integer> n1 = new WindowReader(TGCardPointDef.num1Hand, TGCardPointDef.dimCard).getFeatures(3, 3), s1 = new WindowReader(TGCardPointDef.suit1Hand, TGCardPointDef.dimCard).getFeatures(3, 3);
		List<Integer> n2 = new WindowReader(TGCardPointDef.num2Hand, TGCardPointDef.dimCard).getFeatures(3, 3), s2 = new WindowReader(TGCardPointDef.suit2Hand, TGCardPointDef.dimCard).getFeatures(3, 3);
		ret.add(getCardFromHand(n1,s1));
		ret.add(getCardFromHand(n2,s2));
		return ret;
	}
	
	
	private static CardsEnum getCardFromHand(List<Integer> num, List<Integer> suit) {
		init();
		double s = Util.score(suit, TGCardPointDef.suitSpadesHand), d = Util.score(suit, TGCardPointDef.suitDiamondsHand);
		double h = Util.score(suit, TGCardPointDef.suitHeartsHand), c = Util.score(suit, TGCardPointDef.suitClubsHand);
		String numStr = "", suitStr = "";
		if (s < d && s < h && s < c) {
			if (s < Util.thrsCards)
				suitStr = "s";
		} else if (d < h && d < c) {
			if (d < Util.thrsCards)
				suitStr = "d";
		} else if (h < c) {
			if (h < Util.thrsCards)
				suitStr = "h";
		} else {
			if (c < Util.thrsCards)
				suitStr = "c";
		}
		int idxScore = -1;
		double valScore = Double.MAX_VALUE;
		for (int k = 0; k < numBlacksHand.size(); k++) {
			double v = Util.score(num, numBlacksHand.get(k));
			if (v < valScore) {
				valScore = v;
				idxScore = k;
			}
		}
		for (int k = 0; k < numRedsHand.size(); k++) {
			double v = Util.score(num, numRedsHand.get(k));
			if (v < valScore) {
				valScore = v;
				idxScore = k;
			}
		}
		if (suitStr.isEmpty())
			return null;
		idxScore = idxScore + 1;
		numStr = idxScore == 1 ? "A" : (idxScore == 10 ? "T" : (idxScore == 11 ? "J" : 
			(idxScore == 12 ? "Q" : (idxScore == 13 ? "K" : (idxScore + ""))))); 
		return CardsEnum.valueOf(suitStr+numStr);
	}

	public static CardsEnum getCardFromTable(List<Integer> num, List<Integer> suit) {
		init();
		double s = Util.score(suit, TGCardPointDef.suitSpadesTable), d = Util.score(suit, TGCardPointDef.suitDiamondsTable);
		double h = Util.score(suit, TGCardPointDef.suitHeartsTable), c = Util.score(suit, TGCardPointDef.suitClubsTable);
		String numStr = "", suitStr = "";
		if (s < d && s < h && s < c) {
			if (s < Util.thrsCards)
				suitStr = "s";
		} else if (d < h && d < c) {
			if (d < Util.thrsCards)
				suitStr = "d";
		} else if (h < c) {
			if (h < Util.thrsCards)
				suitStr = "h";
		} else {
			if (c < Util.thrsCards)
				suitStr = "c";
		}
		int idxScore = -1;
		double valScore = Double.MAX_VALUE;
		for (int k = 0; k < numBlacksTable.size(); k++) {
			double v = Util.score(num, numBlacksTable.get(k));
			if (v < valScore) {
				valScore = v;
				idxScore = k;
			}
		}
		for (int k = 0; k < numRedsTable.size(); k++) {
			double v = Util.score(num, numRedsTable.get(k));
			if (v < valScore) {
				valScore = v;
				idxScore = k;
			}
		}
		if (suitStr.isEmpty())
			return null;
		idxScore = idxScore + 1;
		numStr = idxScore == 1 ? "A" : (idxScore == 10 ? "T" : (idxScore == 11 ? "J" : 
			(idxScore == 12 ? "Q" : (idxScore == 13 ? "K" : (idxScore + ""))))); 
		return CardsEnum.valueOf(suitStr+numStr);
	}
}