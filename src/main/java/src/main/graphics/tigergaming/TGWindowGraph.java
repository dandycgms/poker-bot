package src.main.graphics.tigergaming;

import java.awt.Point;
import java.util.Arrays;
import java.util.List;

import src.main.utils.Util;
import src.main.utils.WindowReader;

public class TGWindowGraph {

	public final static List<Integer> outTurn = Arrays.asList(8677, 7701, 6965, 9231, 10048, 8132, 8216, 7621, 7756);
	public final static List<Integer> inTurn = Arrays.asList(29382, 29811, 26379, 25962, 26055, 22971, 21264, 21264, 18606);
	public final static List<Integer> sittingOut = Arrays.asList(110694, 109446, 116910, 73353, 54873, 68010, 85731, 81711, 81729);

	public final static Point minimizedPoint = new Point(12, 57);
	public final static Point tableclosedPoint1 = new Point(517,295);
	public final static Point tableclosedPoint2 = new Point(521,298);
	public final static Point lobbyPoint = new Point(3,59);
	public final static Point lobbyOpenPoint = new Point(64,716);
	public final static Point sittingOutPoint = new Point(771,652);
	public final static Point tourneyOnLobbyPoint = new Point(34,264);
	public final static Point tourneyLobbyOpenPoint = new Point(709,563);
	public final static Point nothingPoint = new Point(500,122);
					
	public final static Point foldPoint = new Point(721, 657);
	public final static Point callPoint = new Point(985, 669);
	public final static Point betPoint = new Point(1156,666);
	public final static Point betInputPoint = new Point(1340,615);
	public final static Point allinPoint = new Point(1290,582);
	public final static Point betpotPoint = new Point(1161,584);
	public final static Point infoOnPoint = new Point(283,579);
	
	public final static List<Integer> minimized = Arrays.asList(17860, 24591, 25011, 23544, 30713, 30192, 15028, 17872, 17612);
	public final static List<Integer> tableclosed1 = Arrays.asList(90242, 90736, 100499, 137585, 75486, 95409, 94559, 53631, 63903);
	public final static List<Integer> tableclosed2 = Arrays.asList(121698, 112179, 128940, 286086, 100578, 106170, 85959, 54465, 59490);
	public final static List<Integer> lobby = Arrays.asList(329105, 326660, 336945, 430606, 448210, 512235, 62900, 63002, 185328);
	public final static List<Integer> tourneyLobbyOpen = Arrays.asList(31266, 31266, 31266, 27864, 51093, 27864, 20169, 37896, 20169);
	public final static List<Integer> historyLobby = Arrays.asList(32562, 45207, 32562, 21789, 50100, 21789, 15714, 15714, 15714);
	public final static List<Integer> infoOn = Arrays.asList(36153, 33002, 41294, 29455, 34298, 36813, 16038, 56802, 51234);
	public final static List<Integer> registerOpen = Arrays.asList(36291, 30892, 29148, 43675, 47116, 28418, 22878, 23512, 17006);
	public final static List<Integer> registerPreviouslyOpen = Arrays.asList(9056, 7616, 7140, 9555, 12309, 8654, 4787, 6293, 4630);
	public final static List<Integer> passwordRegister = Arrays.asList(19971, 19864, 19958, 66293, 66331, 21358, 18486, 18364, 18387);
	public final static List<Integer> ticketRegister = Arrays.asList(71782, 78293, 19303, 20545, 20919, 19284, 19221, 27557, 31846);
	
	public final static Point infoPoint = new Point(28,619);
	public final static Point maximizePoint = new Point(739,15);
	public final static Point closeWindowPoint = new Point(1336,18);
	public final static Point closeTourneyLobbyOpenPoint = new Point(787,16);
	public final static Point abortPasswordRegisterPoint = new Point(811,483);
	public final static Point abortTicketRegisterPoint = new Point(818,440);
	
	public final static Point registerPoint = new Point(1219,687);
	public final static Point registerPoint1 = new Point(1256,693);
	public final static Point registerPoint2 = new Point(721,440);
	public final static Point passwordRegisterPoint = new Point(510,408);
	public final static Point ticketRegisterPoint = new Point(763,366);
	
	public final static List<Point> button_10 = Arrays.asList(new Point(585,454), new Point(535,443), new Point(392,372), new Point(395,277), new Point(558,162), new Point(750,172), new Point(900,198), new Point(937,277), new Point(942,372), new Point(775,454));
	public final static List<Point> dimbtn_10 = Arrays.asList(new Point(611-585,478-454),new Point(559-535,469-443),new Point(418-392,398-372),new Point(421-395,302-277),new Point(585-558,185-162),new Point(777-750,199-172),new Point(933-900,224-198),new Point(966-937,302-277),new Point(967-942,396-372),new Point(801-775,477-454));

	public final static List<List<Integer>> vb_10 = Arrays.asList(
			Arrays.asList(13141, 20274, 16038, 27088, 24646, 26282, 19679, 28304, 19905),
			Arrays.asList(12314, 16746, 11306, 30288, 23944, 28735, 18236, 24903, 17302),
			Arrays.asList(9942, 17558, 10960, 28726, 26241, 29510, 14841, 27504, 17532),
			Arrays.asList(12307, 20649, 11122, 30224, 25950, 27821, 13681, 23870, 12177),
			Arrays.asList(12941, 20119, 13309, 28080, 22345, 24767, 16713, 26687, 14338),
			Arrays.asList(11848, 17748, 9925, 30732, 26778, 27325, 17811, 27439, 14991),
			Arrays.asList(14365, 25766, 15370, 27624, 32532, 29757, 13476, 29196, 17364),
			Arrays.asList(10636, 22554, 12571, 26210, 29745, 30125, 11224, 26136, 14361),
			Arrays.asList(10777, 17402, 9027, 26715, 24646, 23599, 18798, 28273, 15849),
			Arrays.asList(9504, 13996, 9737, 24847, 26389, 25415, 19933, 28483, 22421)
	);

	public static int getButtonPosition() {
		double o = Double.MAX_VALUE;
		int i = 0, pos = 0;
		for (Point point : button_10) {
			List<Integer> f = new WindowReader(point, dimbtn_10.get(pos)).getFeatures(3, 3);
			double s = Util.score(f, vb_10.get(pos));
			if (s < o) {
				o = s;
				pos = i;
			}
			i++;
		}		
		return pos;
	}

	public static void calibratePointInfo() {
		WindowReader windowReader = new WindowReader(TGNumberPointDef.wstats, new Point(433,120));
		Integer color[] = {73,225,120};
		Point P = windowReader.findFirstPixmap(color);
		if (P != null) {
			P = Util.sum(P, TGNumberPointDef.wstats);
			TGNumberPointDef.shft = P.x - 5;		
		}
	}
	
	public static boolean isRegisterOpen() {		 
		List<Integer> f = new WindowReader(registerPoint, new Point(63,18)).getFeatures(3, 3);
		double o = Util.score(f, registerOpen);
		if (o < Util.thrsCards)
			return true;
		return Util.score(f, registerPreviouslyOpen) < Util.thrsCards;
	}

	public static boolean isInsideLobby() {
		List<Integer> f = new WindowReader(lobbyPoint, new Point(245,43)).getFeatures(3, 3);
		return Util.score(f, lobby) < Util.thrsCards;
	}
	
	public static boolean isPasswordRegister() {
		List<Integer> f = new WindowReader(passwordRegisterPoint, new Point(593-510,445-408)).getFeatures(3, 3);
		return Util.score(f, passwordRegister) < Util.thrsCards;
	}

	public static boolean isTicketRegister() {
		List<Integer> f = new WindowReader(ticketRegisterPoint, new Point(829-763,413-366)).getFeatures(3, 3);
		return Util.score(f, ticketRegister) < Util.thrsCards;
	}

	public static boolean isTourneyLobbyOpen() {
		WindowReader windowReader = new WindowReader(tourneyLobbyOpenPoint, new Point(80,30));
		List<Integer> f = windowReader.getFeatures(3, 3);
		double o = Util.score(f, tourneyLobbyOpen);
		if (o < Util.thrsCards)
			return true;
		return Util.score(f, historyLobby) < Util.thrsCards;
	}

	public static boolean isTableClosed() {
		List<Integer> f1 = new WindowReader(tableclosedPoint1, new Point(836-517,313-295)).getFeatures(3, 3);
		List<Integer> f2 = new WindowReader(tableclosedPoint2, new Point(833-521,314-298)).getFeatures(3, 3);
		return Util.score(f1, tableclosed1) < Util.thrsCards || Util.score(f2, tableclosed2) < Util.thrsCards;
	}
	
	public static boolean isMinimized() {
		List<Integer> f = new WindowReader(minimizedPoint, new Point(21,30)).getFeatures(3, 3);
		return Util.score(f, minimized) < Util.thrsCards;
	}
	
	public static boolean isSittingOut() {
		List<Integer> f = new WindowReader(sittingOutPoint, new Point(57,38)).getFeatures(3, 3);
		return Util.score(f, sittingOut) < Util.thrsCards;
	}
	
	public static boolean isMyTurn() {
		List<Integer> f = new WindowReader(foldPoint, new Point(22,25)).getFeatures(3, 3);
		double o = Util.score(f, outTurn), i = Util.score(f, inTurn);
		return i < o && i < 2*Util.thrsCards;
	}

	public static boolean isInfoOn() {
		List<Integer> f = new WindowReader(infoOnPoint, new Point(65,29)).getFeatures(3, 3);
		return Util.score(f, infoOn) < Util.thrsCards;
	}

	public static boolean canBet() {
		List<Integer> f = new WindowReader(betPoint, new Point(22,25)).getFeatures(3, 3);
		double o = Util.score(f, outTurn), i = Util.score(f, inTurn);
		return i < o;
	}

}