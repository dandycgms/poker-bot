package src.main.graphics.tigergaming;

import java.awt.Point;
import java.util.Arrays;
import java.util.List;

public class TGCardPointDef {

	public final static Point dimCard = new Point(18,24);
	
	public final static Integer sftSuit = 24;

	public final static Point num1Flop = new Point(502,263);
	public final static Point num2Flop = new Point(574,263);
	public final static Point num3Flop = new Point(646,263);
	public final static Point numTurn = new Point(718,263);
	public final static Point numRiver = new Point(790,263);
	
	public final static Point suit1Flop = new Point(502,263+sftSuit);
	public final static Point suit2Flop = new Point(574,263+sftSuit);
	public final static Point suit3Flop = new Point(646,263+sftSuit);
	public final static Point suitTurn = new Point(718,263+sftSuit);
	public final static Point suitRiver = new Point(790,263+sftSuit);
	
	public final static Point num1Hand = new Point(635,440);
	public final static Point num2Hand = new Point(657,440);
	public final static Point suit1Hand = new Point(635,440+sftSuit);
	public final static Point suit2Hand = new Point(657,440+sftSuit);

	public final static List<Integer> suitSpadesTable = Arrays.asList(46377, 25197, 37818, 22521, 1320, 10281, 42264, 28575, 35760);
	public final static List<Integer> suitDiamondsTable = Arrays.asList(48195, 31743, 41048, 34756, 11568, 26802, 42840, 30297, 36720);
	public final static List<Integer> suitHeartsTable = Arrays.asList(39401, 32022, 30420, 29004, 11756, 18880, 42840, 32888, 36720);
	public final static List<Integer> suitClubsTable = Arrays.asList(47793, 22029, 38241, 20439, 5085, 8892, 42795, 28959, 36675);
	
	public final static List<Integer> suitDiamondsHand = Arrays.asList(48195, 31743, 41048, 34756, 11568, 26802, 42840, 30297, 36720);
	public final static List<Integer> suitHeartsHand = Arrays.asList(39401, 32022, 30420, 29004, 11756, 18880, 42840, 32888, 36720);
	public final static List<Integer> suitSpadesHand = Arrays.asList(46377, 25197, 37818, 22521, 1320, 10281, 42264, 28575, 35760);
	public final static List<Integer> suitClubsHand = Arrays.asList(47793, 22029, 38241, 20439, 5085, 8892, 42795, 28959, 36675);
}