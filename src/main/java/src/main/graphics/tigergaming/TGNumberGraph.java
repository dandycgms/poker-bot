package src.main.graphics.tigergaming;

import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

import src.main.utils.Util;
import src.main.utils.WindowReader;

public class TGNumberGraph {

	private static List<Integer> getConnectedComponents(WindowReader reader, DIGIT_KIND opt) {
		List<Integer> ret = new ArrayList<Integer>();
		TGNumberPointDef.init();
		Integer[][][] mat = reader.binarize();
		List<Point[]> comp = reader.connectedComponents(mat);
		if (opt.equals(DIGIT_KIND.CALL) && comp.size() == 6) {
			List<Integer> feat = reader.getFeatures(3, 3, mat, comp.get(0)[0], comp.get(0)[1]);
			if (Util.score(feat, TGNumberPointDef.cc) < Util.thrsNumber)
				return new ArrayList<Integer>(0);
		}
		for (Point[] points : comp) {
			int dim = (points[1].x - points[0].x + 1) * (points[1].y - points[0].y + 1);
			List<Integer> feat = reader.getFeatures(3, 3, mat, points[0], points[1]);
			int idxScore = -1;
			double valScore = Double.MAX_VALUE;
			switch (opt) {
				case STAT:
					if (dim == 1) { 
						idxScore = 13;
					} else if (dim == 3)
						idxScore = 14;
					else {
						for (int k = 0; k < TGNumberPointDef.statsDigits.size(); k++) {
							double v = Util.score(feat, TGNumberPointDef.statsDigits.get(k));
							if (v < valScore) { valScore = v; idxScore = k; }
						}
					}				
					break;
				case STAK:
					for (int k = 0; k < TGNumberPointDef.stackDigits.size(); k++) {
						double v = Util.score(feat, TGNumberPointDef.stackDigits.get(k));
						if (v < valScore) { valScore = v; idxScore = k; }
					}
					break;
				case CALL:
					for (int k = 0; k < TGNumberPointDef.callDigits.size(); k++) {
						double v = Util.score(feat, TGNumberPointDef.callDigits.get(k));
						if (v < valScore) { valScore = v; idxScore = k; }
					}
					break;
				case POT:
					for (int k = 0; k < TGNumberPointDef.potDigits.size(); k++) {
						double v = Util.score(feat, TGNumberPointDef.potDigits.get(k));
						if (v < valScore) { valScore = v; idxScore = k; }
					}
					break;
				default:
					break;
			}
			if (!opt.equals(DIGIT_KIND.POT) || (opt.equals(DIGIT_KIND.POT) && idxScore < 10))
				ret.add(idxScore);
		}
		return ret;
	}
	
	private static List<Integer> getConnectedComponents(WindowReader reader) {
		return getConnectedComponents(reader, DIGIT_KIND.STAT);
	}
	
	public static double getLargest() {
		WindowReader reader = new WindowReader(Util.sum(TGNumberPointDef.largest, new Point(TGNumberPointDef.shft, 0)), TGNumberPointDef.dimNumericStats);
		return Util.list2double(getConnectedComponents(reader));
	}
	
	public static double getAverage() {
		WindowReader reader = new WindowReader(Util.sum(TGNumberPointDef.average, new Point(TGNumberPointDef.shft, 0)), TGNumberPointDef.dimNumericStats);
		return Util.list2double(getConnectedComponents(reader));
	}
	
	public static double getSmallest() {
		WindowReader reader = new WindowReader(Util.sum(TGNumberPointDef.smallest, new Point(TGNumberPointDef.shft, 0)), TGNumberPointDef.dimNumericStats);
		return Util.list2double(getConnectedComponents(reader));
	}
	
	public static Double getMyStake() {
		WindowReader reader = new WindowReader(TGNumberPointDef.myStake, TGNumberPointDef.dimNumericMyStake);
		return Util.list2double(getConnectedComponents(reader, DIGIT_KIND.STAK));
	}

	public static Double getCall() {
		WindowReader reader = new WindowReader(TGNumberPointDef.call, TGNumberPointDef.dimNumericCall);
		reader.negative();
		return Util.list2double(getConnectedComponents(reader, DIGIT_KIND.CALL));
	}

	public static List<Integer> getTableID() {
		WindowReader reader = new WindowReader(TGNumberPointDef.tableId, TGNumberPointDef.dimTableId);
		return reader.getFeatures(3, 3);
	}

	public static Double getTotalPlayers() {
		WindowReader reader = new WindowReader(Util.sum(TGNumberPointDef.players, new Point(TGNumberPointDef.shft, 0)), TGNumberPointDef.dimNumericStats);
		List<Integer> ints = getConnectedComponents(reader);
		List<Integer> temp = new ArrayList<Integer>();
		for (int i= 0; i < ints.size()-1; i++)
			temp.add(ints.get(i));
		return Util.list2double(temp);
	}

	public static List<Double> getRank() {
		WindowReader reader = new WindowReader(Util.sum(TGNumberPointDef.rank, new Point(TGNumberPointDef.shft, 0)), TGNumberPointDef.dimNumericStats);
		List<Double> ret = new ArrayList<Double>();
		List<Integer> ints = getConnectedComponents(reader);
		List<Integer> temp = new ArrayList<Integer>();
		for (Integer integer : ints) {
			if (integer == 10) 
				ret.add(Util.list2double(temp));
			else if (integer == 11) 
				temp.clear();
			else 
				temp.add(integer);
		}
		ret.add(Util.list2double(temp));
		return ret;
	}

	public static List<Double> getStakes() {
		WindowReader reader = new WindowReader(Util.sum(TGNumberPointDef.stakes, new Point(TGNumberPointDef.shft, 0)), TGNumberPointDef.dimNumericStats);
		List<Double> ret = new ArrayList<Double>();
		List<Integer> ints = getConnectedComponents(reader);
		List<Integer> temp = new ArrayList<Integer>();
		for (Integer integer : ints) {
			if (integer == 12) {
				ret.add(Util.list2double(temp));
				temp.clear();
			} else 
				temp.add(integer);
		}
		ret.add(Util.list2double(temp));
		return ret;
	}

	public static Double getPot() {
		WindowReader reader = new WindowReader(TGNumberPointDef.pot, TGNumberPointDef.dimPot);
		List<Integer> ints = getConnectedComponents(reader, DIGIT_KIND.POT);
		List<Integer> temp = new ArrayList<Integer>();
		for (Integer integer : ints)
			temp.add(integer);
		return Util.list2double(temp);
	}		

	public enum DIGIT_KIND {
		STAK, CALL, STAT, POT;
	}
}