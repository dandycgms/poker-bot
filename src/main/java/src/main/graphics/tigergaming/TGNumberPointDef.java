package src.main.graphics.tigergaming;

import java.awt.Point;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TGNumberPointDef {

	public static int shft = 407;
	
	public final static Point dimNumericStats = new Point(90,13);
	public final static Point wstats = new Point(94,578);
	public final static Point rank = new Point(0,581);
	public final static Point players = new Point(0,641);					
	public final static Point largest = new Point(47,596);
	public final static Point average = new Point(56,611);
	public final static Point smallest = new Point(55,626);
	public final static Point stakes = new Point(44,671);
	
	public final static Point dimNumericMyStake = new Point(110,17);
	public final static Point myStake = new Point(651,523);

	public final static Point dimTableId = new Point(100,16);
	public final static Point tableId = new Point(35,6);
	
	public final static Point dimNumericCall = new Point(186,20);
	public final static Point call = new Point(927,669);

	public final static Point dimPot = new Point(207,20);
	public final static Point pot = new Point(587,203);
	
	public final static List<Integer> d0 = Arrays.asList(2292, 1528, 764, 2292, 0, 0, 2292, 0, 0);
	public final static List<Integer> d1 = Arrays.asList(764, 764, 0, 0, 0, 0, 0, 0, 0);
	public final static List<Integer> d2 = Arrays.asList(2292, 1528, 764, 0, 0, 764, 764, 1528, 0);
	public final static List<Integer> d3 = Arrays.asList(2292, 1528, 764, 0, 1528, 764, 1528, 0, 0);
	public final static List<Integer> d4 = Arrays.asList(0, 764, 3056, 764, 1528, 2292, 2292, 1528, 3056);
	public final static List<Integer> d5 = Arrays.asList(2292, 1528, 764, 2292, 1528, 764, 1528, 0, 764);
	public final static List<Integer> d6 = Arrays.asList(1528, 1528, 764, 3056, 1528, 764, 2292, 0, 0);
	public final static List<Integer> d7 = Arrays.asList(1528, 1528, 1528, 0, 1528, 764, 0, 2292, 0);
	public final static List<Integer> d8 = Arrays.asList(2292, 1528, 764, 2292, 1528, 764, 2292, 0, 0);
	public final static List<Integer> d9 = Arrays.asList(2292, 1528, 764, 2292, 1528, 764, 764, 0, 764);
	public final static List<Integer> f = Arrays.asList(764, 1528, 1528, 0, 2292, 0, 0, 2292, 0);
	public final static List<Integer> o = Arrays.asList(2292, 1528, 764, 1528, 0, 0, 1528, 0, 0);
	public final static List<Integer> slash = Arrays.asList(0, 0, 764, 0, 764, 1528, 764, 1528, 0);
	
	public final static List<Integer> s0 = Arrays.asList(6112, 3056, 4584, 6112, 0, 3056, 5348, 1528, 3820);
	public final static List<Integer> s1 = Arrays.asList(2292, 2292, 3056, 764, 0, 3056, 0, 0, 2292);
	public final static List<Integer> s2 = Arrays.asList(6112, 3056, 5348, 0, 2292, 4584, 4584, 3820, 1528);
	public final static List<Integer> s3 = Arrays.asList(5348, 3056, 5348, 0, 3056, 5348, 5348, 1528, 3820);
	public final static List<Integer> s4 = Arrays.asList(0, 3820, 6112, 6112, 2292, 6112, 4584, 3056, 4584);
	public final static List<Integer> s5 = Arrays.asList(7640, 3056, 3056, 6876, 3056, 4584, 5348, 1528, 3820);
	public final static List<Integer> s6 = Arrays.asList(6112, 3056, 3820, 8404, 3056, 4584, 5348, 1528, 3820);
	public final static List<Integer> s7 = Arrays.asList(4584, 3820, 6112, 0, 5348, 1528, 2292, 3056, 0);
	public final static List<Integer> s8 = Arrays.asList(7640, 3056, 5348, 7640, 3056, 6112, 6112, 1528, 3820);
	public final static List<Integer> s9 = Arrays.asList(6112, 3056, 4584, 6112, 3056, 5348, 3820, 1528, 3820);
	public final static List<Integer> scolon = Arrays.asList(764, 0, 0, 764, 0, 0, 0, 0, 0);

	public final static List<Integer> c0 = Arrays.asList(8404, 6112, 6876, 9168, 0, 6112, 7640, 3820, 6112);
	public final static List<Integer> c1 = Arrays.asList(3820, 5348, 3820, 764, 3056, 3056, 0, 3056, 3056);
	public final static List<Integer> c2 = Arrays.asList(6876, 4584, 6876, 0, 5348, 5348, 6112, 6876, 1528);
	public final static List<Integer> c3 = Arrays.asList(6876, 6112, 6876, 0, 3820, 6112, 6112, 3820, 6112);
	public final static List<Integer> c4 = Arrays.asList(764, 8404, 7640, 5348, 4584, 6112, 4584, 6112, 7640);
	public final static List<Integer> c5 = Arrays.asList(7640, 8404, 3820, 4584, 3056, 6112, 6112, 3820, 6112);
	public final static List<Integer> c6 = Arrays.asList(8404, 7640, 5348, 9168, 3820, 6112, 7640, 3820, 6112);
	public final static List<Integer> c7 = Arrays.asList(4584, 6876, 6876, 1528, 7640, 0, 6112, 4584, 0);
	public final static List<Integer> c8 = Arrays.asList(9168, 5348, 6876, 6112, 7640, 4584, 8404, 3820, 6112);
	public final static List<Integer> c9 = Arrays.asList(9168, 6112, 6876, 8404, 3820, 6112, 5348, 5348, 6112);
	public final static List<Integer> ccolon = Arrays.asList(1528, 1528, 0, 764, 764, 0, 0, 764, 0);
	public final static List<Integer> cc = Arrays.asList(4584, 0, 0, 5348, 764, 3820, 3820, 6112, 4584);
	
	public final static List<Integer> p0 = Arrays.asList(6112, 2292, 4584, 4584, 0, 2292, 5348, 0, 3820);
	public final static List<Integer> p1 = Arrays.asList(1528, 6112, 0, 0, 4584, 0, 0, 4584, 0);
	public final static List<Integer> p2 = Arrays.asList(6112, 2292, 4584, 0, 1528, 3820, 2292, 3820, 0);
	public final static List<Integer> p3 = Arrays.asList(6112, 2292, 4584, 0, 2292, 4584, 3820, 0, 3056);
	public final static List<Integer> p4 = Arrays.asList(0, 2292, 6112, 3820, 764, 4584, 2292, 2292, 4584);
	public final static List<Integer> p5 = Arrays.asList(6112, 2292, 1528, 1528, 2292, 3820, 3820, 0, 3056);
	public final static List<Integer> p6 = Arrays.asList(4584, 3056, 1528, 5348, 2292, 3820, 5348, 764, 3056);
	public final static List<Integer> p7 = Arrays.asList(2292, 3056, 5348, 0, 4584, 764, 1528, 3820, 0);
	public final static List<Integer> p8 = Arrays.asList(5348, 2292, 3820, 5348, 5348, 4584, 4584, 0, 3056);
	public final static List<Integer> p9 = Arrays.asList(5348, 2292, 4584, 5348, 2292, 3056, 0, 764, 4584);
	public final static List<Integer> pcolon = Arrays.asList(0, 1528, 1528, 0, 764, 764, 764, 764, 764);
	public final static List<Integer> pp = Arrays.asList(1528, 1528, 0, 2292, 2292, 0, 2292, 2292, 1528);
	public final static List<Integer> po = Arrays.asList(6876, 3056, 4584, 5348, 3056, 3056, 4584, 0, 0);
	public final static List<Integer> pt = Arrays.asList(5348, 1528, 3820, 3056, 0, 1528, 3056, 0, 2292);
	public final static List<Integer> p_ = Arrays.asList(3056, 2292, 764, 2292, 2292, 0, 2292, 2292, 0);
	public final static List<Integer> pdot = Arrays.asList(764, 0, 0, 764, 0, 0, 0, 0, 0);

	public static List<List<Integer>> statsDigits = null, stackDigits = null, callDigits = null, potDigits = null;
	
	public static void init() {
		if (statsDigits == null) {
			initStats();
			initStack();
			initCall();
			initPot();
		}
	}

	private static void initPot() {
		potDigits = new ArrayList<List<Integer>>();
		potDigits.add(p0);
		potDigits.add(p1);
		potDigits.add(p2);
		potDigits.add(p3);
		potDigits.add(p4);
		potDigits.add(p5);
		potDigits.add(p6);
		potDigits.add(p7);
		potDigits.add(p8);
		potDigits.add(p9);
		potDigits.add(pcolon);
		potDigits.add(pp);
		potDigits.add(po);
		potDigits.add(pt);
		potDigits.add(p_);
		potDigits.add(pdot);
	}

	private static void initCall() {
		callDigits = new ArrayList<List<Integer>>();
		callDigits.add(c0);
		callDigits.add(c1);
		callDigits.add(c2);
		callDigits.add(c3);
		callDigits.add(c4);
		callDigits.add(c5);
		callDigits.add(c6);
		callDigits.add(c7);
		callDigits.add(c8);
		callDigits.add(c9);
		callDigits.add(ccolon);
	}

	private static void initStack() {
		stackDigits = new ArrayList<List<Integer>>();
		stackDigits.add(s0);
		stackDigits.add(s1);
		stackDigits.add(s2);
		stackDigits.add(s3);
		stackDigits.add(s4);
		stackDigits.add(s5);
		stackDigits.add(s6);
		stackDigits.add(s7);
		stackDigits.add(s8);
		stackDigits.add(s9);
		stackDigits.add(scolon);
	}

	private static void initStats() {
		statsDigits = new ArrayList<List<Integer>>();
		statsDigits.add(d0);
		statsDigits.add(d1);
		statsDigits.add(d2);
		statsDigits.add(d3);
		statsDigits.add(d4);
		statsDigits.add(d5);
		statsDigits.add(d6);
		statsDigits.add(d7);
		statsDigits.add(d8);
		statsDigits.add(d9);
		statsDigits.add(o);
		statsDigits.add(f);
		statsDigits.add(slash);
	}
	
}