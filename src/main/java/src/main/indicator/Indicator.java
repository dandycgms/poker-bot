package src.main.indicator;

import src.main.kernel.Statistics;
import src.main.model.Table;
import src.main.utils.Util;

public class Indicator {
	
	public double h_order, h_headsup, h_stats, h_outs, h_sup, h_imp;
	public double s_rank, s_avg, s_maxmin, s_bb;
	public double m_rem, m_bb;
	public double w_io, w_avgio, w_call, w_avgcall, w_button, w_player;
	
	public Indicator() {
		h_order = Util.nrand();
		h_headsup = Util.nrand();
		h_stats = Util.nrand();
		h_outs = Util.nrand();
		h_sup = Util.nrand();
		h_imp = Util.nrand();
		s_rank = Util.nrand();
		s_avg = Util.nrand();
		s_maxmin = Util.nrand();
		s_bb = Util.nrand();
		m_rem = Util.nrand();
		m_bb = Util.nrand();
		w_io = Util.nrand();
		w_avgio = Util.nrand();
		w_call = Util.nrand();
		w_avgcall = Util.nrand();
		w_button = Util.nrand();
		w_player = Util.nrand();
	}
	
	public Indicator(double h_order, double h_headsup, double h_stats, double h_outs, double h_sup, double h_imp, double s_rank, double s_avg, double s_maxmin, double s_bb, double m_rem, double m_bb, double w_io, double w_avgio, double w_call, double w_avgcall, double w_button, double w_player) {
		this.h_order = h_order;
		this.h_headsup = h_headsup;
		this.h_stats = h_stats;
		this.h_outs = h_outs;
		this.h_sup = h_sup;
		this.h_imp = h_imp;
		this.s_rank = s_rank;
		this.s_avg = s_avg;
		this.s_maxmin = s_maxmin;
		this.s_bb = s_bb;
		this.m_rem = m_rem;
		this.m_bb = m_bb;
		this.w_io = w_io;
		this.w_avgio = w_avgio;
		this.w_call = w_call;
		this.w_avgcall = w_avgcall;
		this.w_button = w_button;
		this.w_player = w_player;
	}

	public Indicator(Table table) {
		this();
		update(table);
	}

	public void update(Table table) {
		
		/** Hand strenght **/
		h_order = Util.norm((Statistics.MAX_ORDER_CARD-table.preflopOrder)/(Statistics.MAX_ORDER_CARD-1.0));
		h_headsup = Util.norm(table.headsUp/100.0);
		h_stats = Util.norm(table.preflopStats);
		if (table.isPreflop()) {
			h_outs = 0;
			h_sup = 0;
			h_imp = 0;
		} else {
			h_outs = Util.norm(1-Statistics.getNumberOfOutHandsPerc(table)/100.0);
			h_sup = Util.norm(Statistics.getStrenghtImprovementOverSupremumPerc(table)/100.0);
			h_imp = Util.norm(Statistics.getStrenghtImprovementPerc(table)/100.0);
		}
		
		/** Situation **/
		s_rank = Util.norm(1-(table.rank - 1)/(table.remainingPlayers - 1));
		s_avg = table.myStack >= table.averageStack ?
				((table.myStack-table.averageStack+0.001)/(table.largestStack-table.averageStack+0.001)) :
				((table.myStack-table.averageStack+0.001)/(table.averageStack-table.smallestStack+0.001));
		s_maxmin = Util.norm((table.myStack-table.smallestStack+0.001)/(table.largestStack-table.smallestStack+0.001));
		s_bb = Util.norm(Math.min(Math.log10(Math.ceil((table.myStack+0.001)/(table.bigBlind+0.001))), 3)/3);
		
		/** Moment **/
		m_rem = Util.norm(table.remainingPlayers/(table.totalPlayers + 0.001));
		m_bb = Util.norm(Math.min(Math.log10(Math.ceil((table.averageStack+0.001)/(table.bigBlind + 0.001))), 3)/3);
		
		/** Worthness **/
		w_io = Util.norm(1-table.getImpliedOdds());
		w_avgio = Util.norm(1-((table.mavgcall+0.0001)/(table.mavgpot+0.0001)));
		w_call = Util.norm(1-table.getPercCallOverMyStack()/100.0);
		w_avgcall = Util.norm(1-((table.mavgcall+0.0001)/(table.myStack+0.0001)));
		w_button = Util.norm(0.5); // falta estimar layouttable e tableplayeronsit
		w_player = Util.norm(0.5); // falta estimar jogadores ativos na mão corrente
	}
	
	public double getLinCurve(Indicator i, Table table) {
	      return Util.denorm(((table.isPreflop() ? 
	    		  		  h_order*i.h_order + h_headsup*i.h_headsup + h_stats*i.h_stats : 
	    		  		  table.isRiver() ? h_outs*i.h_outs : h_outs*i.h_outs + h_sup*i.h_sup + h_imp*i.h_imp) +
	    	      s_rank*i.s_rank + s_avg*i.s_avg + s_maxmin*i.s_maxmin + s_bb*i.s_bb +
	    	      m_rem*i.m_rem + m_bb*i.m_bb + 
	    	      w_io*i.w_io + w_avgio*i.w_avgio + w_call*i.w_call + w_avgcall*i.w_avgcall + w_button*i.w_button + w_player*i.w_player)/
	    	      ((table.isPreflop() ? 
	    	    		  Math.abs(h_order) + Math.abs(h_headsup) + Math.abs(h_stats) :
	    	    		  table.isRiver() ? Math.abs(h_outs) : Math.abs(h_outs) + Math.abs(h_sup) + Math.abs(h_imp)) +
	               Math.abs(s_rank) + Math.abs(s_avg) + Math.abs(s_maxmin) + Math.abs(s_bb) +
	               Math.abs(m_rem) + Math.abs(m_bb) + 
	               Math.abs(w_io) + Math.abs(w_avgio) + Math.abs(w_call) + Math.abs(w_avgcall) + Math.abs(w_button) + Math.abs(w_player)));	
	}
	
	public double getLogCurve(Indicator i, Table table) {
	      return  Util.denorm(((table.isPreflop() ? 
	    		  		Util.lnorm(h_order*i.h_order) + Util.lnorm(h_headsup*i.h_headsup) + Util.lnorm(h_stats*i.h_stats) :
	    		  		table.isRiver() ? Util.lnorm(h_outs*i.h_outs) : Util.lnorm(h_outs*i.h_outs) + Util.lnorm(h_sup*i.h_sup) + Util.lnorm(h_imp*i.h_imp)) +
	    		   Util.lnorm(s_rank*i.s_rank) + Util.lnorm(s_avg*i.s_avg) + Util.lnorm(s_maxmin*i.s_maxmin) + Util.lnorm(s_bb*i.s_bb) +
	    		   Util.lnorm(m_rem*i.m_rem) + Util.lnorm(m_bb*i.m_bb) + 
	               Util.lnorm(w_io*i.w_io) + Util.lnorm(w_avgio*i.w_avgio) + Util.lnorm(w_call*i.w_call) + Util.lnorm(w_avgcall*i.w_avgcall) + Util.lnorm(w_button*i.w_button) + Util.lnorm(w_player*i.w_player))/
	               ((table.isPreflop() ? 
	            		Util.lnorm(Math.abs(h_order)) + Util.lnorm(Math.abs(h_headsup)) + Util.lnorm(Math.abs(h_stats)) :
	            		table.isRiver() ? Util.lnorm(Math.abs(h_outs)) : Util.lnorm(Math.abs(h_outs)) + Util.lnorm(Math.abs(h_sup)) + Util.lnorm(Math.abs(h_imp))) +
	                Util.lnorm(Math.abs(s_rank)) + Util.lnorm(Math.abs(s_avg)) + Util.lnorm(Math.abs(s_maxmin)) + Util.lnorm(Math.abs(s_bb)) +
	                Util.lnorm(Math.abs(m_rem)) + Util.lnorm(Math.abs(m_bb)) + 
	                Util.lnorm(Math.abs(w_io)) + Util.lnorm(Math.abs(w_avgio)) + Util.lnorm(Math.abs(w_call)) + Util.lnorm(Math.abs(w_avgcall)) + Util.lnorm(Math.abs(w_button)) + Util.lnorm(Math.abs(w_player))));
	}

	@Override
	public String toString() {		
		return "H: <" + Util.round(h_order) + ", " + Util.round(h_headsup) + ", " + Util.round(h_stats) + ", " + Util.round(h_outs) + ", " + Util.round(h_sup) + ", " + Util.round(h_imp) + ">\n" +
			   "S: <" + Util.round(s_rank) + ", " + Util.round(s_avg) + ", " + Util.round(s_maxmin) + ", " + Util.round(s_bb) + ">\n" +
			   "M: <" + Util.round(m_rem) + ", " + Util.round(m_bb) + ">\n" +
			   "W: <" + Util.round(w_io) + ", " + Util.round(w_avgio) + ", " + Util.round(w_call) + ", " + Util.round(w_avgcall) + ", " + Util.round(w_button) + ", " + Util.round(w_player) + ">\n\n" + 
			   "i(" + Util.round(h_order) + ", " + Util.round(h_headsup) + ", " + Util.round(h_stats) + ", " + Util.round(h_outs) + ", " + Util.round(h_sup) + ", " + Util.round(h_imp) + ", " + Util.round(s_rank) + ", " + Util.round(s_avg) + ", " + Util.round(s_maxmin) + ", " + Util.round(s_bb) + ", " + Util.round(m_rem) + ", " + Util.round(m_bb) + ", " + Util.round(w_io) + ", " + Util.round(w_avgio) + ", " + Util.round(w_call) + ", " + Util.round(w_avgcall) + ", " + Util.round(w_button) + ", " + Util.round(w_player) + ")\n";
	}
}
