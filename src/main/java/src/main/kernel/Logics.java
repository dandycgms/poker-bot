package src.main.kernel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import src.main.model.CardsEnum;
import src.main.model.Hand;
import src.main.model.HandsEnum;
import src.main.model.NumEnum;
import src.main.model.Table;

public class Logics {

	private static boolean isOpenHand(List<CardsEnum> hand) {
		if (hand.size() != 2)
			return false;
		if (hand.get(0) == null || hand.get(1) == null)
			return false;
		return true;
	}
	
	private static boolean isOpenBoard(List<CardsEnum> board) {
		if (board.size() < 3)
			return false;
		if (board.get(0) == null || board.get(1) == null || board.get(2) == null)
			return false;
		return true;
	}
	
	private static boolean isOpenTable(Table table) {
		return isOpenBoard(table.board) && isOpenHand(table.hand);
	}

	public static boolean hasPair(List<CardsEnum> set) {
		if (!isOpenHand(set) && !isOpenBoard(set))
			return false;
		for (int i = 0; i < set.size(); i++) 
			for (int j = i+1; j < set.size(); j++) {
				if (set.get(j) == null)
					return false;
				if (set.get(i).getNum() == set.get(j).getNum())
					return true;
			}
		return false;
	}

	public static boolean hasNothing(List<CardsEnum> board) {
		return Hand.getHandRank(board).getRank().equals(HandsEnum.HIGH_CARD);
	}
	
	public static boolean hasTop(List<CardsEnum> hand, NumEnum card) {
		for (CardsEnum cardEnum : hand) {
			if (cardEnum == null)
				return false; 
			if (cardEnum.getNum() >= card.getNum())
				return true;
		}
		return false;
	}
	
	public static boolean isTop(List<CardsEnum> hand, NumEnum card) {
		for (CardsEnum cardEnum : hand) {
			if (cardEnum == null)
				return false; 
			if (cardEnum.getNum() < card.getNum())
				return false;
		}
		return true;
	}

	private static CardsEnum getTop(List<CardsEnum> hand) {
		for (NumEnum x : NumEnum.values())
			for (CardsEnum cardEnum : hand) 
				if (cardEnum.getNum() == x.getNum())
					return cardEnum;	
		return null;
	}
	
	
	public static boolean isPocketPair(List<CardsEnum> hand) {
		if (!isOpenHand(hand))
			return false;
		return hand.get(0) != null && hand.get(0).getNum() == hand.get(1).getNum();
	}
	
	public static boolean isSuited(List<CardsEnum> hand) {
		if (!isOpenHand(hand))
			return false;
		int suit = hand.get(0).getSuit();
		for (int i = 1; i < hand.size(); i++)
			if (hand.get(i).getSuit() != suit)
				return false;
		return true;
	}
	
	public static boolean isConnected(List<CardsEnum> hand) {
		if (!isOpenHand(hand))
			return false;
		if (hand.size() == 2) {
			if (hand.get(0).getNum() == hand.get(1).getNum()+1 || 
				hand.get(0).getNum()+1 == hand.get(1).getNum() ||
			    (hand.get(0).getNum() == 13 && hand.get(1).getNum() == 1) ||
			    (hand.get(0).getNum() == 1 && hand.get(1).getNum() == 13))
				return true;
			return false;
		}
		List<Integer> list = new ArrayList<Integer>();
		for (int i = 0; i < hand.size(); i++)
			if (hand.get(i) != null)
				list.add(hand.get(i).getNum());
		Collections.sort(list);
		if (list.get(0) == 1 && list.get(list.size()-1) == 13) {
			list.set(list.size()-1,0);
			Collections.sort(list);
		}
		for (int i = 1; i < list.size(); i++)
			if (list.get(i) - list.get(i-1) != 1)
				return false;
		return true;
	}
	
	public static boolean isSuitConnected(List<CardsEnum> hand) {
		return isSuited(hand) && isConnected(hand);
	}

	public static boolean hasAce(List<CardsEnum> hand) {
		return hasAn(hand, NumEnum._A);
	}
	
	public static boolean hasFigure(List<CardsEnum> hand) {
		for (CardsEnum cardEnum : hand) {
			if (cardEnum == null)
				return false; 
			if (cardEnum.getNum() >= NumEnum._J.getNum())
				return true;
		}
		return false;
	}

	public static boolean hasOnlyFigure(List<CardsEnum> hand) {
		for (CardsEnum cardEnum : hand) {
			if (cardEnum == null)
				return false; 
			if (cardEnum.getNum() < NumEnum._J.getNum())
				return false;
		}
		return true;
	}

	public static boolean hasAn(List<CardsEnum> hand, NumEnum card) {
		for (CardsEnum cardEnum : hand) {
			if (cardEnum == null)
				return false;
			if (card.getNum() == cardEnum.getNum())
				return true;
		}
		return false;
	}
	
	public static Hand getBestHand(Table table) {
		return getBestHand(table.board, table.hand);
	}

	public static Hand getBestHand(List<CardsEnum> board, List<CardsEnum> hand) {
		return getBestHand(board, hand, true);
	}
	
	public static Hand getBestHand(List<CardsEnum> board, List<CardsEnum> hand, boolean pocket) {
		List<CardsEnum> joined = new ArrayList<CardsEnum>(getOpenCards(board)); 
		joined.addAll(getOpenCards(hand));
		if (joined.size() < 5)
			return null;
		int combi[][];
		if (joined.size() == 5)
			combi = combi5;
		else if (joined.size() == 6)
			combi = combi6;
		else
			combi = combi7;
		double score = 0;
		Hand bestHand = null; 
		for (int i = 0; i < combi.length; i++) {
			ArrayList<CardsEnum> candHand = new ArrayList<CardsEnum>();
			for (int j = 0; j < combi[i].length; j++)
				candHand.add(joined.get(combi[i][j]));
			Hand rank = Hand.getHandRank(candHand);
			if (rank.getStrength() > score && (!pocket || 
					(pocket && (candHand.contains(hand.get(0)) || candHand.contains(hand.get(1)))))) {
				score = rank.getStrength();
				bestHand = rank;
			} 
		}
		return bestHand;
	}
	
	private static List<CardsEnum> getOpenCards(List<CardsEnum> set) {
		List<CardsEnum> ret = new ArrayList<CardsEnum>();
		for (CardsEnum cardsEnum : set)
			if (cardsEnum != null)
				ret.add(cardsEnum);
		return ret;
	}

	public static boolean isNuts(Table table) {
		if (!isOpenTable(table))
			return false;
		return Statistics.getNumberOfOutHandsPerc(table) == 0.0;
	}
	
	public static boolean isTopPocketPair(Table table) {
		if (!isOpenTable(table))
			return false;
		List<CardsEnum> hand = getOpenCards(table.hand);
		if (!isPocketPair(hand))
			return false;
		CardsEnum t = getTop(getOpenCards(table.board));
		if (t == null)
			return false;
		return t.getNum() < hand.get(0).getNum();
	}
	
	public static boolean isCardsUpPocketPair(Table table) {
		if (!isOpenTable(table))
			return false;
		List<CardsEnum> hand = getOpenCards(table.hand);
		if (!isSinglePocketPair(table))
			return false;
		List<CardsEnum> board = getOpenCards(table.board);
		CardsEnum t = getTop(board);
		if (t == null)
			return false;
		return t.getNum() == hand.get(0).getNum() || t.getNum() == hand.get(1).getNum();
	}

	public static boolean isSinglePocketPair(Table table) {
		if (!isOpenTable(table))
			return false;
		Hand h = getBestHand(table);
		return h.getRank().equals(HandsEnum.ONE_PAIR) && (h.getCards().contains(table.hand.get(0)) || h.getCards().contains(table.hand.get(1)));
	}

	public static boolean isFullDoubleCardsPocketPair(Table table) {
		Hand h = getBestHand(table);
		return isDoubleCardsPair(table) && h.getCards().contains(table.hand.get(0)) && h.getCards().contains(table.hand.get(1));
	}

	public static boolean isSingleDoubleCardsPocketPair(Table table) {
		Hand h = getBestHand(table);
		return isDoubleCardsPair(table) && (h.getCards().contains(table.hand.get(0)) || h.getCards().contains(table.hand.get(1)));
	}

	public static boolean isDoubleCardsPair(Table table) {
		if (!isOpenTable(table))
			return false;
		return getBestHand(table).getRank().equals(HandsEnum.TWO_PAIR);
	}
	
	public static boolean isPocketTriple(Table table) {
		if (!isOpenTable(table))
			return false;
		Hand h = getBestHand(table);		
		return isTriple(table) && (h.getCards().contains(table.hand.get(0)) || h.getCards().contains(table.hand.get(1)));
	}
	
	public static boolean isFullPocketTriple(Table table) {
		if (!isOpenTable(table))
			return false;
		if (!isPocketPair(table.hand))
			return false;
		return isPocketTriple(table) ;
	}
	
	public static boolean isTriple(Table table) {
		if (!isOpenTable(table))
			return false;
		return getBestHand(table).getRank().equals(HandsEnum.THREE_OF_A_KIND);
	}

	public static boolean isForth(Table table) {
		if (!isOpenTable(table))
			return false;
		return getBestHand(table).getRank().equals(HandsEnum.FOUR_OF_A_KIND);
	}

	public static boolean isFullHouse(Table table) {
		if (!isOpenTable(table))
			return false;
		return getBestHand(table).getRank().equals(HandsEnum.FULL_HOUSE);
	}
	
	public static boolean isStraight(Table table) {
		if (!isOpenTable(table))
			return false;
		return getBestHand(table).getRank().equals(HandsEnum.STRAIGHT);
	}
	
	public static boolean isFlush(Table table) {
		if (!isOpenTable(table))
			return false;
		return getBestHand(table).getRank().equals(HandsEnum.FLUSH);
	}
	
	public static boolean isStraightFlush(Table table) {
		if (!isOpenTable(table))
			return false;
		return getBestHand(table).getRank().equals(HandsEnum.STRAIGHT_FLUSH);
	}
	
	public static final int combi5[][] = {{0,1,2,3,4}}; 
	public static final int combi6[][] = {{0,1,2,3,4},
									{0,1,2,3,5},
									{0,1,2,4,5},
									{0,1,3,4,5},
									{0,2,3,4,5},
									{1,2,3,4,5}};
	public static final int combi7[][] = {{0,1,2,3,4},
									{0,1,2,3,5},
									{0,1,2,3,6},
									{0,1,2,4,5},
									{0,1,2,4,6},
									{0,1,2,5,6},
									{0,1,3,4,5},
									{0,1,3,4,6},
									{0,1,3,5,6},
									{0,1,4,5,6},
									{0,2,3,4,5},
									{0,2,3,4,6},
									{0,2,3,5,6},
									{0,2,4,5,6},
									{0,3,4,5,6},
									{1,2,3,4,5},
									{1,2,3,4,6},
									{1,2,3,5,6},
									{1,2,4,5,6},
									{1,3,4,5,6},
									{2,3,4,5,6}};
}