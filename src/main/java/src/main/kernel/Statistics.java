package src.main.kernel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import src.main.model.CardsEnum;
import src.main.model.Hand;
import src.main.model.NumEnum;
import src.main.model.Table;

public class Statistics {

	public static final int TOTAL_CARDS_COMBI = 1326;
	public static final int MAX_ORDER_CARD = 169;
	public static final double BEST_IMPROVEMENT = 0.36;
	
	public static double oSuiteds[][] = {
		{0.000,133.0,118.6,114.3,129.2,133.6,129.1,118.3,104.2,87.20,74.20,50.10,41.00},
		{133.0,0.000,104.0,95.60,108.5,118.5,125.3,114.2,95.50,82.40,68.20,50.20,33.10},
		{118.6,104.0,0.000,78.10,87.70,102.1,104.3,108.4,87.30,78.20,63.20,49.00,31.10},
		{114.3,95.60,78.10,0.000,74.10,78.30,87.60,87.50,87.40,74.30,59.30,46.10,26.40},
		{129.2,108.5,87.70,74.10,0.000,59.20,63.40,68.50,68.40,68.30,56.00,41.10,33.00},
		{133.6,118.5,102.1,78.30,59.20,0.000,50.30,56.20,50.50,56.10,50.40,36.30,26.30},
		{129.1,125.3,104.3,87.60,63.40,50.30,0.000,46.20,36.40,41.40,41.30,33.20,22.00},
		{118.3,114.2,108.4,87.50,68.50,56.20,46.20,0.000,22.30,22.20,22.10,20.10,18.10},
		{104.2,95.60,87.40,87.30,68.40,50.50,36.40,22.30,0.000,15.10,15.00,13.10,11.00},
		{87.20,82.40,78.20,74.30,68.30,56.10,41.40,22.20,15.10,0.000,11.10,9.001,8.000},
		{76.20,68.20,63.20,59.30,56.00,50.40,41.30,22.10,15.00,11.10,0.000,7.000,6.000},
		{50.10,50.20,49.00,46.10,41.10,36.30,33.20,20.10,13.10,9.001,7.000,0.000,4.000},
		{41.00,33.10,31.10,26.40,33.00,26.30,22.00,18.10,11.00,8.000,6.000,4.000,0.000}};

	public static double oUnsuiteds[][] = {
		{82.00,165.2,161.2,159.1,165.1,169.0,168.0,164.0,156.0,151.0,143.0,125.0,108.0},
		{165.2,78.00,154.1,148.2,156.2,161.1,165.0,161.0,154.0,148.0,140.0,118.0,102.0},
		{161.2,154.1,68.00,133.5,143.2,151.2,156.1,159.0,151.1,146.0,133.1,114.0,95.10},
		{159.1,148.2,133.5,59.00,129.3,133.4,143.1,148.1,146.1,140.2,129.0,108.1,82.20},
		{165.1,156.2,143.2,129.3,50.00,118.4,125.2,133.3,133.2,140.1,125.1,104.1,95.00},
		{169.0,161.1,151.2,133.4,118.4,46.00,108.3,114.1,108.2,118.2,118.1,95.20,82.10},
		{168.0,165.0,156.1,143.1,125.2,108.3,26.00,87.10,82.30,95.40,95.30,87.00,74.00},
		{164.0,161.0,159.0,148.1,133.3,114.1,87.10,17.00,59.10,63.30,68.10,63.10,63.00},
		{156.0,154.0,151.1,146.1,133.2,108.2,82.30,59.10,9.000,36.20,41.20,36.10,36.00}, 
		{151.0,148.0,146.0,140.2,140.1,118.2,95.40,63.30,36.20,5.000,31.00,26.20,26.10}, 
		{143.0,140.0,133.1,129.0,125.1,118.1,95.30,68.10,41.20,31.00,3.000,20.00,18.00},
		{125.0,118.0,114.0,108.1,104.1,95.20,87.00,63.10,36.10,26.20,20.00,2.000,13.00},
		{108.0,102.0,95.10,82.20,95.00,82.10,74.00,63.00,36.00,26.10,18.00,13.00,1.000}};
		
	public static double suiteds[][] = {
		{0.00,0.13,0.16,0.17,0.14,0.13,0.14,0.16,0.19,0.22,0.25,0.30,0.34},
		{0.13,0.00,0.19,0.21,0.18,0.16,0.15,0.17,0.21,0.23,0.26,0.30,0.36},
		{0.16,0.19,0.00,0.24,0.22,0.20,0.19,0.18,0.22,0.24,0.27,0.32,0.37},
		{0.17,0.21,0.24,0.00,0.25,0.24,0.22,0.22,0.22,0.25,0.28,0.33,0.38},
		{0.14,0.18,0.22,0.25,0.00,0.28,0.27,0.26,0.26,0.26,0.29,0.34,0.36},
		{0.13,0.16,0.20,0.24,0.28,0.00,0.30,0.29,0.30,0.29,0.30,0.35,0.38},
		{0.14,0.15,0.19,0.22,0.27,0.30,0.00,0.33,0.35,0.34,0.34,0.36,0.39},
		{0.16,0.17,0.18,0.22,0.26,0.29,0.33,0.00,0.39,0.39,0.39,0.41,0.42},
		{0.19,0.21,0.22,0.22,0.26,0.30,0.35,0.39,0.00,0.47,0.47,0.48,0.49},
		{0.22,0.23,0.24,0.25,0.26,0.29,0.34,0.39,0.47,0.00,0.49,0.50,0.52},
		{0.25,0.26,0.27,0.28,0.29,0.30,0.34,0.39,0.47,0.49,0.00,0.53,0.55},
		{0.30,0.30,0.32,0.33,0.34,0.35,0.36,0.41,0.48,0.50,0.53,0.00,0.60},
		{0.34,0.36,0.37,0.38,0.36,0.38,0.39,0.42,0.49,0.52,0.55,0.60,0.00}};

	public static double unsuiteds[][] = {
		{0.24,0.02,0.04,0.05,0.02,0.00,0.01,0.03,0.06,0.08,0.11,0.15,0.18},
		{0.02,0.24,0.07,0.09,0.06,0.04,0.02,0.04,0.07,0.09,0.12,0.16,0.20},
		{0.04,0.07,0.26,0.13,0.11,0.08,0.06,0.05,0.08,0.10,0.13,0.17,0.21},
		{0.05,0.09,0.13,0.28,0.14,0.13,0.11,0.09,0.10,0.12,0.14,0.18,0.23},
		{0.02,0.06,0.11,0.14,0.30,0.16,0.15,0.13,0.13,0.12,0.15,0.19,0.21},
		{0.00,0.04,0.08,0.13,0.16,0.33,0.18,0.17,0.18,0.16,0.16,0.21,0.23},
		{0.01,0.02,0.06,0.11,0.15,0.18,0.38,0.22,0.23,0.21,0.21,0.22,0.25},
		{0.03,0.04,0.05,0.09,0.13,0.17,0.22,0.43,0.28,0.27,0.26,0.27,0.27},
		{0.06,0.07,0.08,0.10,0.13,0.18,0.23,0.28,0.50,0.35,0.34,0.35,0.35},
		{0.08,0.09,0.10,0.12,0.12,0.16,0.21,0.27,0.35,0.58,0.37,0.38,0.38},
		{0.11,0.12,0.13,0.14,0.15,0.16,0.21,0.26,0.34,0.37,0.69,0.41,0.42},
		{0.15,0.16,0.17,0.18,0.19,0.21,0.22,0.27,0.35,0.38,0.41,0.81,0.48},
		{0.18,0.20,0.21,0.23,0.21,0.23,0.25,0.27,0.35,0.38,0.42,0.48,1.00}};

	public static double getHeadsUpPerc(List<CardsEnum> myHand) {
		if (myHand.size() != 2 || myHand.get(0) == null || myHand.get(1) == null)
			return 0.0;
		CardsEnum[] values = CardsEnum.values();
		double myHandStrength = 0;
		int n = 0;
		for (int i = 0; i < values.length; i++) {
			if (!myHand.contains(values[i]))
				for (int j = i+1; j < values.length; j++) 
					if (!myHand.contains(values[j])) {
						List<CardsEnum> otherHand = Arrays.asList(values[i], values[j]);
						myHandStrength += getHeadsUpPerc(myHand, otherHand);
						n++;
					}
		}
		return myHandStrength/n;
	}
	
	public static double getHeadsUpPerc(List<CardsEnum> myHand, List<CardsEnum> otherHand) {
		int aux;
		if (Logics.isPocketPair(myHand)) {
			int myN = myHand.get(0).getNum();
			if (Logics.isPocketPair(otherHand)) {
				int otherN = otherHand.get(0).getNum();
				//AA vs KK	AA wins 80%
				if (myN > otherN)
					return 80;
				//AA vs KK	AA wins 80%
				else if (myN < otherN)
					return 20;
				//AA vs AA	AA wins 100%
				else
					return 100; 
			} else {
				int ominN = otherHand.get(0).getNum();
				int omajN = otherHand.get(1).getNum();
				if (ominN > omajN) {
					aux = ominN;
					ominN = omajN;
					omajN = aux;
				}
				//AK vs 22	22 wins 51%
				if (myN < ominN) 
					return 51;
				//AK vs KK	KK wins 66%
				else if (myN == ominN)
					return 66;
				else if (myN == omajN) {
					//AK vs AA	AA wins 87%
					if (Logics.isConnected(otherHand) || Logics.isSuited(otherHand))
						return 87;
					// KK vs K2	KK wins 94%
					return 94;
				} else if (Logics.isSuitConnected(otherHand))
					// AA vs 76 suited	AA wins 77%
					return 77;
				else
					// AA vs 78	AA wins 77%
					return 85;
			}
		} else {
			int mminN = myHand.get(0).getNum();
			int mmajN = myHand.get(1).getNum();
			if (mminN > mmajN) {
				aux = mminN;
				mminN = mmajN;
				mmajN = aux;
			}
			if (Logics.isPocketPair(otherHand)) {
				int oN = myHand.get(0).getNum();
				//AK vs 22	22 wins 51%
				if (oN < mminN) 
					return 49;
				//AK vs KK	KK wins 66%
				else if (oN == mminN)
					return 44;
				else if (oN == mmajN) {
					//AK vs AA	AA wins 87%
					if (Logics.isConnected(myHand) || Logics.isSuited(myHand))
						return 13;
					// KK vs K2	KK wins 94%
					return 6;
				} else if (Logics.isSuitConnected(myHand))
					// AA vs 76 suited	AA wins 77%
					return 33;
				else
					// AA vs 78	AA wins 85%
					return 15;
			} else {
				int ominN = otherHand.get(0).getNum();
				int omajN = otherHand.get(1).getNum();
				if (ominN > omajN) {
					aux = ominN;
					ominN = omajN;
					omajN = aux;
				}
				if (mminN < ominN) {
					if (mmajN < ominN) {
						// AK vs 76 suited	AK wins 60%
						if (Logics.isSuitConnected(myHand)) 
							return 40;
						//AK vs QJ	AK wins 64%
						if (Logics.isConnected(myHand))
							return 36;
						// AK vs 78	AK wins 65%
						else
							return 35;
					} else if (mmajN == ominN) {
						// AK vs KQ	AK wins 73%
						return 27;
					} else if (mmajN < omajN) {						
						// AQ vs KJ	AQ wins 60%
						return 30;
					} else if (mmajN == omajN) {
						// AK vs A9	AK wins 74%
						return 16;
					} else {
						if (Logics.isConnected(otherHand)) {
							//AT vs KQ	AT wins 58%
							if (Logics.isTop(otherHand, NumEnum._Q))
								return 58;
							//A2 vs JT	A2 wins 55%
							else
								return 55;
						} else
							return 54;
					}
				} else if (mminN == ominN) {
					//AQ vs KQ	AQ wins 70%
					if (mmajN < omajN) {
						return 30;
					//AQ vs AQ	AQ wins 100%
					} else if (mmajN == omajN) {
						return 100;
					//AQ vs KQ	AQ wins 70%
					} else {
						return 70;						
					}
				} else {
					if (mminN > omajN) {
						// AK vs 76 suited	AK wins 60%
						if (Logics.isSuitConnected(myHand)) 
							return 60;
						//AK vs QJ	AK wins 64%
						if (Logics.isConnected(myHand))
							return 64;
						// AK vs 78	AK wins 65%
						else
							return 65;
					} else if (mminN == omajN) {
						// AK vs KQ	AK wins 73%
						return 73;
					} else if (mmajN > omajN) {						
						// AQ vs KJ	AQ wins 60%
						return 60;
					} else if (mmajN == omajN) {
						// AK vs A9	AK wins 74%
						return 74;
					} else {
						if (Logics.isConnected(myHand)) {
							//AT vs KQ	AT wins 58%
							if (Logics.isTop(myHand, NumEnum._Q))
								return 42;
							//A2 vs JT	A2 wins 55%
							else
								return 45;
						} else
							return 45;
					}
				}
			}
		}			
	}
	
	public static double getPreflopStats(CardsEnum a, CardsEnum b) {
		if (a == null || b == null)
			return 0.0;
		if (a.getSuit() == b.getSuit())
			return suiteds[a.getNum()-1][b.getNum()-1];
		return unsuiteds[a.getNum()-1][b.getNum()-1];
	}
	
	public static double getPreflopOrder(CardsEnum a, CardsEnum b) {
		if (a == null || b == null)
			return 10000.0;
		if (a.getSuit() == b.getSuit())
			return Math.floor(oSuiteds[a.getNum()-1][b.getNum()-1]);
		return Math.floor(oUnsuiteds[a.getNum()-1][b.getNum()-1]);
	}

	public static List<CardsEnum[]> getOutHands(List<CardsEnum> board, List<CardsEnum> hand) {
		Hand bestHand = Logics.getBestHand(board, hand);
		if (bestHand == null)
			return null;
		double myHandStrength = bestHand.getStrength();
		List<CardsEnum[]> ret = new ArrayList<CardsEnum[]>();
		CardsEnum[] values = CardsEnum.values();
		for (int i = 0; i < values.length; i++) {
			if (!board.contains(values[i]) && !hand.contains(values[i]))
				for (int j = i+1; j < values.length; j++) 
					if (!board.contains(values[j]) && !hand.contains(values[j])) {
						double otherHandStrength = Logics.getBestHand(board, Arrays.asList(values[i], values[j])).getStrength();
						if (otherHandStrength > myHandStrength) {
							CardsEnum[] pair = {values[i], values[j]};
							ret.add(pair);
						}
					}
		}
		return ret;
	}
	
	public static double getStrenghtImprovementOverSupremumPerc(Table table) {
		List<CardsEnum> future = new ArrayList<CardsEnum>(table.board);
		if (table.isPreflop() || table.isRiver())
			return 0.0;
		double strenghtOfBestPair = getBestHandOfBoard(table.board);
		double d = getStrenghtImprovement(table, future);
		return Math.min(100*d/strenghtOfBestPair, 100.0);
	}

	public static double getStrenghtImprovementPerc(Table table) {
		List<CardsEnum> future = new ArrayList<CardsEnum>(table.board);
		if (table.isPreflop() || table.isRiver())
			return 0.0;
		Hand bestHand = Logics.getBestHand(future, table.hand);
		if (bestHand == null)
			return 0.0;
		double v = bestHand.getStrength();
		double d = getStrenghtImprovement(table, future);
		return Math.min(100.0*(d-v)/BEST_IMPROVEMENT, 100.0);
	}

	private static double getStrenghtImprovement(Table table, List<CardsEnum> future) {
		double m = 0, n = 0;
		if (table.isFlop()) {
			CardsEnum[] values = CardsEnum.values();
			for (int i = 0; i < values.length; i++) {
				if (!table.board.contains(values[i]) && !table.hand.contains(values[i]))
					for (int j = i+1; j < values.length; j++) 
						if (!table.board.contains(values[j]) && !table.hand.contains(values[j])) {
							future.add(values[i]); future.add(values[j]);
							m += Logics.getBestHand(future, table.hand).getStrength();
							n++;
							future.remove(values[i]); future.remove(values[j]);
					}
			}
		}
		else if (table.isTurn()) {
			for (CardsEnum card : CardsEnum.values()) 
				if (!table.board.contains(card) && !table.hand.contains(card)) {
						future.add(card);
						m += Logics.getBestHand(future, table.hand).getStrength();
						n++;
						future.remove(card);
				}
		}
		return m/n;
	}

	private static double getBestHandOfBoard(List<CardsEnum> board, CardsEnum ... pair) {
		CardsEnum[] values = CardsEnum.values();
		double maxStrength = 0;
		for (int i = 0; i < values.length; i++) {
			if (!board.contains(values[i]))
				for (int j = i+1; j < values.length; j++) 
					if (!board.contains(values[j])) {
						double otherHandStrength = Logics.getBestHand(board, Arrays.asList(values[i], values[j])).getStrength();
						if (otherHandStrength > maxStrength) {
							if (pair != null && pair.length == 2) {
								pair[0] = values[i];
								pair[1] = values[j];
							}
							maxStrength = otherHandStrength;
						}
					}
		}
		return maxStrength;
	}
	
	public static int getNumberOfOutHands(Table table) {
		return getNumberOfOutHands(table.board, table.hand);
	}
	
	public static int getNumberOfOutHands(List<CardsEnum> board, List<CardsEnum> hand) {
		List<CardsEnum[]> outHands = getOutHands(board, hand);
		return outHands == null ? TOTAL_CARDS_COMBI : outHands.size();
	}

	public static double getNumberOfOutHandsPerc(Table table) {
		return getNumberOfOutHandsPerc(table.board, table.hand);
	}

	public static double getNumberOfOutHandsPerc(List<CardsEnum> board, List<CardsEnum> hand) {
		return 100.0*getNumberOfOutHands(board, hand)/TOTAL_CARDS_COMBI;
	}
	
}