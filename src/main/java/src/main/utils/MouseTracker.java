package src.main.utils;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Toolkit;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JLabel;

public class MouseTracker extends JFrame implements MouseListener, MouseMotionListener {

	private static final long serialVersionUID = 1L;

	int numClicks, N;
	float opacity = 0.05f;
	List<MouseEvent> ret;
	List<String> labels;
	JLabel label = null;

	public MouseTracker(List<String> labels) {
		this.labels = labels;
		this.N = labels.size();
		this.ret = new ArrayList<MouseEvent>();
	}
	
	public MouseTracker(int N) {
		this.labels = new ArrayList<String>();
		this.N = N;
		this.ret = new ArrayList<MouseEvent>();
	}
	
	public List<MouseEvent> start() {
		addMouseListener(this);
		addMouseMotionListener(this);
		setLayout(new FlowLayout(1));
		Dimension size = Toolkit.getDefaultToolkit().getScreenSize();
		setSize(size);
		setUndecorated(true);
		setVisible(true);
		setOpacity(opacity);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		while (numClicks < N) {
			Util.sleep();
			putLabel();
		}
		return ret;
	}

	

	private void putLabel() {
		if (labels.size() == 0)
			return;
		if (label != null)
			remove(label);
		if (numClicks < N) {
			label = new JLabel("LAB");
			label.setText(labels.get(numClicks));
			label.setForeground(Color.red);
			label.setSize(100, 100);
			add(label);
			validate();
		    repaint();
		}
	}

	public void mouseClicked(MouseEvent e) {
		setOpacity((float) 0.5);
		System.out.println("Mouse clicado na coordenada : [" + e.getX() + "," + e.getY() + "]");
		Util.sleep();
		setOpacity(opacity);
		ret.add(e);
		numClicks++;
		if (numClicks >= N)
			dispose();
	}
	public void mouseEntered(MouseEvent e) {}
	public void mouseExited(MouseEvent e) {}
	public void mousePressed(MouseEvent e) {}
	public void mouseReleased(MouseEvent e) {}
	public void mouseDragged(MouseEvent e) {}
	public void mouseMoved(MouseEvent e) {}
}