package src.main.utils;

import java.awt.Color;
import java.awt.Image;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.awt.image.Raster;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class WindowReader {

	private Image image;
	private boolean negative;

	public WindowReader() {
		negative = false;
		List<MouseEvent> e = new MouseTracker(2).start();
		image = Bot.getScreenShot(e.get(0).getX(), e.get(0).getY(), e.get(1).getX(), e.get(1).getY());
	}
	
	public WindowReader(Point top, Point dim) {
		negative = false;
		image = Bot.getScreenShot(top.x, top.y, top.x+dim.x, top.y+dim.y);
	}

	public WindowReader showImage() {
		JFrame frame = new JFrame();
		ImageIcon icon = new ImageIcon(image, "");
		JLabel label = new JLabel(icon);
		frame.add(label);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.pack();
		frame.setVisible(true);
		return this;
	}

	private BufferedImage getImage() {
		return (BufferedImage)image;
	}
	
	public int getWidth() {
		return getImage().getWidth();
	}

	public int getHeight() {
		return getImage().getHeight();
	}

	public Raster getData() {
		return getImage().getData();
	}
	
	public Integer[][][] toArray() {
		Raster data = getData();
		int width = getWidth();
		int height = getHeight();
		Integer ret[][][] = new Integer[height][width][3];
		for (int h = 0; h < height; h++) 
			for (int w = 0; w < width; w++)
				for (int g = 0; g < 3; g++)
					ret[h][w][g] = data.getSample(w,h,g);
		return ret;
	}
		
	public Integer [][][] binarize() {
		return binarize(Util.thrsNumber);
	}
	
	public Integer [][][] binarize(double thrsnumber) {
		Integer img[][][] = toArray();
		thrsnumber = 3*thrsnumber+1;
		for (int h = 0; h < getHeight(); h++) {
			for (int w = 0; w < getWidth(); w++) {
				int v = 0;
				for (int g = 0; g < 3; g++) 
					v += img[h][w][g];
				if ((v > thrsnumber && !negative) || (v < thrsnumber && negative)) {
					img[h][w][0] = 255;
					img[h][w][1] = 255;
					img[h][w][2] = 255;
					getImage().setRGB(w, h, Color.WHITE.getRGB());
				} else {
					img[h][w][0] = 0;
					img[h][w][1] = 0;
					img[h][w][2] = 0;
					getImage().setRGB(w, h, Color.BLACK.getRGB());
				}				
			}
		}
		return img;
	}

	public void negative() {
		negative = true;
	}
	
	public List<Point[]> connectedComponents(Integer [][][] mat) {
		List<Point[]> ret = new ArrayList<Point[]>();
		int H = getHeight(), W = getWidth();
		for (int w = 0; w < W; w++) {
			for (int h = 0; h < H; h++) {
				if (mat[h][w][0] == 255) {
					Stack<Point> stack = new Stack<Point>();
					registerPointInStack(h, w, stack, mat);
					Point max = new Point(h,w);
					Point min = new Point(h,w);
					while (!stack.isEmpty()) {
						Point curP = stack.pop();
						for (int i = -1; i <= 1; i++) 
							for (int j = -1; j <= 1; j++) {
								int candX = curP.x + i;
								int candY = curP.y + j;
								if (candX >= 0 && candX < H && candY >= 0 && candY < W && mat[candX][candY][0] == 255) {
									registerPointInStack(candX, candY, stack, mat);
									if (max.x < candX) max.x = candX;
									if (max.y < candY) max.y = candY;
									if (min.x > candX) min.x = candX;
									if (min.y > candY) min.y = candY;
								}
							}
					}
					ret.add(new Point[]{min,max});
				}
			}
		}
		return ret;
	}

	private void registerPointInStack(int h, int w, Stack<Point> stack, Integer[][][] mat) {
		stack.push(new Point(h,w));
		mat[h][w][0] = 254;		
	}
			
	public List<Integer> getFeatures(int m, int n) {
		Integer img[][][] = toArray();
		return getFeatures(m, n, img);
	}

	public List<Integer> getFeatures(int m, int n, Integer[][][] img) {
		return getFeatures(m, n, img, new Point(0,0), new Point(getHeight(), getWidth()));
	}
	
	public List<Integer> getFeatures(int m, int n, Integer[][][] img, Point min, Point max) {
		Integer feat[][] = new Integer[m][n];
		for (int i=0; i<m; i++) 
			for (int j=0; j<n; j++)
				feat[i][j] = 0;
		for (int h = min.x; h < max.x; h++) {
			int i = (int) Math.floor(((h-min.x)*1.0/(max.x-min.x))*m);
			for (int w = min.y; w < max.y; w++) {
				int j = (int) Math.floor(((w-min.y)*1.0/(max.y-min.y))*n);
				for (int g = 0; g < 3; g++) {
					feat[i][j] += img[h][w][g];
				}
			}
		}
		List<Integer> ret = new ArrayList<Integer>();
		for (int i=0; i<m; i++) 
			for (int j=0; j<n; j++) 
				ret.add(feat[i][j]);
		return ret;
	}

	public WindowReader boundingBox() {
		Integer[][][] x = binarize();
		List<Point[]> p = connectedComponents(x);
		if (p.size() != 1)
			throw new RuntimeException("Era esperada apenas uma componente.");
		WindowReader windowReader = new WindowReader(p.get(0)[0],Util.sub(p.get(0)[1], p.get(0)[0]));
		return windowReader;
	}

	public Point findFirstPixmap(Integer[] color) {
		Integer[][][] img = toArray();
		for (int i = 0; i < getHeight(); i++) 
			for (int j = 0; j < getWidth(); j++) {
				double dist = Math.abs(img[i][j][0] - color[0]) + Math.abs(img[i][j][1] - color[1]) + Math.abs(img[i][j][2] - color[2]);
				//System.out.println(i+", " +j + ": <" + img[i][j][0] + ", "+ img[i][j][1] + ", "+ img[i][j][2] + ">");
				if (dist/3 < 1) 
					return new Point(j,i);
			}
		return null;
		
	}

}