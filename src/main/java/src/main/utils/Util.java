package src.main.utils;

import java.awt.Point;
import java.math.BigInteger;
import java.util.List;
import java.util.Random;

public class Util {
	
	public final static double thrsCards = 750;
	public final static double thrsNumber = 88;
	public final static double normLog = 0.8823529411764706;
	
	public static void sleep() {
		sleep(100);
	}
	
	public static Point sum(Point p1, Point p2) {
		Point ret = new Point(p1);
		ret.x += p2.x;
		ret.y += p2.y;
		return ret;
	}
	
	public static Point sub(Point p1, Point p2) {
		Point ret = new Point(p1);
		ret.x -= p2.x;
		ret.y -= p2.y;
		return ret;
	}
	
	public static double rand() {
		return Math.random();
	}
	
	public static int rand(int min, int max) {
		return new Random().nextInt(max - min + 1) + min;
	}
	
	public static double nrand() {
		return norm(Math.random());
	}
	
	public static double norm(double x) {
		return (x-0.5)*2;
	}

	public static double denorm(double x) {
		return x/2 + 0.5;
	}

	public static double lnorm(double x) {
		double e = Math.pow(2,-4*x);
		return (2/(1+e)-1)/normLog;
	}

	public static double round(double x) {
		return Math.round(x*100.0)/100.0;
	}
	
	public static void sleep(int n) {
		try {
			Thread.sleep(n);
		} catch (InterruptedException e) {}
	}
	
	public static double score(List<Integer> a, List<Integer> b) {
		double ret = 0.0;
		for (int k = 0; k < a.size(); k ++) 
			ret += Math.abs(a.get(k)-b.get(k))/a.size();
		return ret;
	}	
	
	public static double list2double(List<Integer> a) {
		double v = 0.0, dig = 1;
		boolean c = false;
		for (Integer i : a) {
			if (c)
				dig *= 10;
			if (i == 12) 
				c = true;
			else if (i < 10) {
				v *= 10;
				v += i;
			}
		}
		return v/dig;
	}
	
	public static double combi(int n, int p) {
		BigInteger num = factorial(new BigInteger(String.valueOf(n))), 
				   dn1 = factorial(new BigInteger(String.valueOf(p))), 
				   dn2 = factorial(new BigInteger(String.valueOf(n-p)));
		return num.doubleValue()/(dn1.doubleValue()*dn2.doubleValue());
	}
	
	public static BigInteger factorial(BigInteger n) {
	    BigInteger factorial = BigInteger.valueOf(1);
	    for (int i = 1; i <= n.intValue(); i++) 
	        factorial = factorial.multiply(BigInteger.valueOf(i));
	    return factorial;
	} 
}