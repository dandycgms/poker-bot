package src.main.actions;

import java.awt.Point;
import java.util.List;

import src.main.model.CardsEnum;
import src.main.model.Profile;
import src.main.model.TableEvent;

public interface Listener {

	List<CardsEnum> getBoardCards();

	List<CardsEnum> getHandCards();

	List<Integer> getTableId();
	
	Double getCall();

	Double getPot();

	List<Double> getRank();
	
	Double getTotalPlayers();

	List<Double> getStakes();
	
	List<Profile> getOpponents();

	List<TableEvent> getTableEvents();

	Double getMyStack();

	double getLargest();

	double getAverage();

	double getSmallest();

	boolean isPasswordRegister();

	boolean isMyTurn();

	boolean isInfoOn();
	
	boolean isMinimized();

	boolean isTableClosed();

	boolean isInsideLobby();

	boolean isTourneyLobbyOpen();

	boolean isRegisterOpen();

	boolean isSittingOut();

	boolean canBet();

	Point getTable(int n);
	
	Point getCloseWindowPoint();

	Point getTourneyLobbyOpenPoint();

	Point getInfoPoint();

	Point getMaximizePoint();

	Point getSittingOutPoint();

	Point getFoldPoint();

	Point getCallPoint();

	Point getBetInputPoint();

	Point getAllinPoint();
	
	Point getBetPoint();

	Point getBetpotPoint();

	Point getLobby();
	
	Point getTourneyOnLobby();
	
	Point getRegisterPoint1();
	
	Point getRegisterPoint2();

	Point getNothing();

	Point getAbortPasswordRegisterPoint();

	void calibratePointInfo();

	Point getAbortTicketRegisterPoint();

	boolean isTicketRegister();

	int getButtonPosition();

}