package src.main.actions;

import java.awt.Point;
import java.util.List;

import src.main.graphics.tigergaming.TGCardGraph;
import src.main.graphics.tigergaming.TGNumberGraph;
import src.main.graphics.tigergaming.TGWindowGraph;
import src.main.model.CardsEnum;
import src.main.model.Profile;
import src.main.model.TableEvent;
import src.main.utils.Util;

public class TGListener implements Listener {
	
	public List<CardsEnum> getBoardCards() {
		return TGCardGraph.getBoardCards();
	}
	
	public List<CardsEnum> getHandCards() {
		return TGCardGraph.getHandCards();
	}

	public List<Integer> getTableId() {
		return TGNumberGraph.getTableID();
	}

	public Double getCall() {
		return TGNumberGraph.getCall();
	}
	
	public Double getPot() {
		return TGNumberGraph.getPot();
	}
	
	public List<Double> getRank() {
		return TGNumberGraph.getRank();
	}
	
	public Double getTotalPlayers() {
		return TGNumberGraph.getTotalPlayers();
	}
	
	public List<Double> getStakes() {
		return TGNumberGraph.getStakes();
	}
	
	public double getLargest() {
		return TGNumberGraph.getLargest();
	}
	
	public double getAverage() {
		return TGNumberGraph.getAverage();
	}
	
	public double getSmallest() {
		return TGNumberGraph.getSmallest();
	}
	
	public Double getMyStack() {
		return TGNumberGraph.getMyStake();
	}
	
	public void calibratePointInfo() {
		TGWindowGraph.calibratePointInfo();
	}
	
	public boolean isInfoOn() {
		return TGWindowGraph.isInfoOn();
	}
	
	public boolean isMyTurn() {
		return TGWindowGraph.isMyTurn();
	}
	
	public boolean isMinimized() {
		return TGWindowGraph.isMinimized();
	}
	
	public boolean isRegisterOpen() {
		return TGWindowGraph.isRegisterOpen();
	}
	
	public boolean isPasswordRegister() {
		return TGWindowGraph.isPasswordRegister();
	}

	public boolean isTicketRegister() {
		return TGWindowGraph.isTicketRegister();
	}

	public boolean isTableClosed() {
		return TGWindowGraph.isTableClosed();
	}

	public boolean isInsideLobby() {
		return TGWindowGraph.isInsideLobby();
	}
	
	public boolean isTourneyLobbyOpen() {
		return TGWindowGraph.isTourneyLobbyOpen();
	}

	public boolean isSittingOut() {
		return TGWindowGraph.isSittingOut();
	}

	public Point getLobby() {
		return TGWindowGraph.lobbyOpenPoint;
	}
	
	public Point getTourneyOnLobby() {
		return TGWindowGraph.tourneyOnLobbyPoint;
	}
	
	public Point getNothing() {
		return TGWindowGraph.nothingPoint;
	}

	public Point getRegisterPoint1() {
		return TGWindowGraph.registerPoint1;
	}
	
	public Point getRegisterPoint2() {
		return TGWindowGraph.registerPoint2;
	}
	
	public Point getTable(int n) {
		return Util.sum(TGWindowGraph.lobbyOpenPoint, new Point(n*136,0));
	}

	public Point getAbortPasswordRegisterPoint() {
		return TGWindowGraph.abortPasswordRegisterPoint;
	}

	public Point getAbortTicketRegisterPoint() {
		return TGWindowGraph.abortTicketRegisterPoint;
	}

	public Point getCloseWindowPoint() {
		return TGWindowGraph.closeWindowPoint;
	}
	
	public Point getTourneyLobbyOpenPoint() {
		return TGWindowGraph.closeTourneyLobbyOpenPoint;
	}
	
	public Point getInfoPoint() {
		return TGWindowGraph.infoPoint;
	}

	public Point getMaximizePoint() {
		return TGWindowGraph.maximizePoint;
	}

	public Point getSittingOutPoint() {
		return TGWindowGraph.sittingOutPoint;
	}

	public Point getFoldPoint() {
		return TGWindowGraph.foldPoint;
	}

	public Point getCallPoint() {
		return TGWindowGraph.callPoint;
	}

	public Point getBetInputPoint() {
		return TGWindowGraph.betInputPoint;
	}

	public Point getAllinPoint() {
		return TGWindowGraph.allinPoint;
	}

	public boolean canBet() {
		return TGWindowGraph.canBet();
	}

	public Point getBetPoint() {
		return TGWindowGraph.betPoint;
	}

	public Point getBetpotPoint() {
		return TGWindowGraph.betpotPoint;
	}
	
	public int getButtonPosition() {
		return TGWindowGraph.getButtonPosition();
	}
	
	public int getMyRelativeTablePosition() {
		// TODO Auto-generated method stub
		return 0;
	}

	public List<Profile> getOpponents() {
		// TODO Auto-generated method stub
		return null;
	}

	public List<TableEvent> getTableEvents() {
		// TODO Auto-generated method stub
		// BETs & CALLs
		return null;
	}	

}