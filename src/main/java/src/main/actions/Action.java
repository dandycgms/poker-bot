package src.main.actions;

import java.lang.reflect.InvocationTargetException;

import src.main.model.Table;
import src.main.model.TableMap;
import src.main.strategy.Strategy;
import src.main.utils.Bot;
import src.main.utils.Util;

public abstract class Action {

	private int numberOfTables = 0;
	private int currentTable = 0, countWindowControl = 0, cicleWindowControlForLobbyAction = 10;
	
	private Table table; 
	private Listener listener;
	private Strategy strategy;

	public Action(Listener listener, Class<? extends Strategy> main) {
		this.listener = listener;
		this.table = new Table(listener);
		try {
			strategy = main.getDeclaredConstructor(Action.class).newInstance(this);
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		}
	}

	public void run() {
		new Thread() {
			@Override
			public void run() {
				while(true) {
					try {
						windowControl();
						Action.wait(1500);
					} catch(Exception e) {
						e.printStackTrace();
					}
				}
			}
		}.start();
		while(true) {
			try {
				wait(100);
				table.update();
				wait(100);
				if (table.myTurn)
					strategy.kernel();				
				wait(100);
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
	}

	private void windowControl() {
		currentTable = 0;
		while (currentTable <= numberOfTables + 1) {
			lookAround();
			currentTable++;
			wait(3500);
		}
		countWindowControl++;
	}

	private void lookTable() {
		if (listener.isSittingOut()) 
			sittingIn();
		if (listener.isMinimized()) { 
			maximize();
			wait(500);
			numberOfTables++;
			System.out.println("@" + numberOfTables + " TABLES ARE OPEN");
		}
	}

	public void lookAround() {
		if (listener.isTableClosed()) {
			closeWindow();
			if (numberOfTables > 0)
				numberOfTables--;
			System.out.println("@" + numberOfTables + " TABLES ARE OPEN");
		}
		if (currentTable == 0) {
			if (countWindowControl >= cicleWindowControlForLobbyAction) {
				performRegister();
				countWindowControl = 0;
			}
		} else {
			Bot.leftClick(listener.getTable(currentTable));
			while (listener.isTourneyLobbyOpen()) {
				Bot.leftClick(listener.getTourneyLobbyOpenPoint());
				Bot.leftClick(listener.getTable(currentTable));
			}
			escapePointer();
		}
		lookTable();
	}

	public void escapePointer() {
		Bot.leftClick(listener.getNothing());
	}

	public void performRegister() {
		registerOpenLobby();
		if (listener.isPasswordRegister()) {
			Bot.leftClick(listener.getAbortPasswordRegisterPoint());
			wait(500);
			registerOpenLobby();
		}
		if (listener.isTicketRegister()) {
			Bot.leftClick(listener.getAbortTicketRegisterPoint());
			wait(500);
			registerOpenLobby();
		}
		if (listener.isRegisterOpen())
			register();
	}

	private void registerOpenLobby() {
		if (!listener.isInsideLobby())
			Bot.leftClick(listener.getLobby());
		Bot.leftClick(listener.getTourneyOnLobby());
		wait(500);
	}

	public void register() {
		System.out.println("Registering...");
		Bot.leftClick(listener.getRegisterPoint1());
		wait(500);
		Bot.leftClick(listener.getRegisterPoint2());
		wait(500);
	}
	
	public void closeWindow() {
		System.out.println("Close window...");
		TableMap.remove(table);
		Bot.leftClick(listener.getCloseWindowPoint());
	}
		
	public void maximize() {
		System.out.println("Maximize...");
		Bot.leftClick(listener.getMaximizePoint());
	}

	public void sittingIn() {
		System.out.println("Sitting In...");
		Bot.leftClick(listener.getSittingOutPoint());
	}
	
	public void fold() {
		System.out.println("Fold...");
		Bot.leftClick(listener.getFoldPoint());
	}
	
	public void callany() {
		call(null, -1);
	}
	
	public void check(Table table) {
		call(table, 0);
	}
	
	public void call(Table table, double until) {
		if (until == -1 || table.call <= until) {
			if (until == -1)
				System.out.println("Call ANY...");
			else
				System.out.println("Call/Check...");
			Bot.leftClick(listener.getCallPoint());
		} else
			fold();
	}
	
	public void betblind(Table table, int n) {
		System.out.println("Bet Blind x" + n + "...");
		if (!listener.canBet())
			callany();
		else {
			Bot.leftClick(listener.getBetInputPoint());
			setBet(table.bigBlind * n);
			Bot.leftClick(listener.getBetPoint());
		}
	}
	
	public void betpot() {
		System.out.println("Bet...");
		if (!listener.canBet())
			callany();
		else {
			Bot.leftClick(listener.getBetpotPoint());
			Bot.leftClick(listener.getBetPoint());
		}
	}
	
	public Action tablesOpen(int n) {
		numberOfTables = n;
		return this;
	}
	
	public static void wait(int n) {
		Util.sleep(n);
	}
	
	public void allin() {
		if (!listener.canBet())
			callany();
		else {
			System.out.println("All In..." );
			Bot.leftClick(listener.getAllinPoint());
			Bot.leftClick(listener.getBetPoint());
		}
	}
	
	public void raisebet(double val) {
		if (!listener.canBet())
			callany();
		else {
			System.out.println("Raise/Bet:" + val);
			Bot.leftClick(listener.getBetInputPoint());
			setBet(val);
			Bot.leftClick(listener.getBetPoint());
		}
	}
	
	public void setBet(double val) {
		Bot.keyPress("\b\b\b\b\b\b\b\b\b");
		Bot.keyPress(String.valueOf((int)val));
	}

	public Table getTable() {
		return table;
	}

	public Listener getListener() {
		return listener;
	}
}