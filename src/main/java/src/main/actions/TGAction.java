package src.main.actions;

import src.main.strategy.Strategy;

public class TGAction extends Action {

	public TGAction(Class<? extends Strategy> main) {
		super(new TGListener(), main);
	}
}