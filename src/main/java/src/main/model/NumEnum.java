package src.main.model;

public enum NumEnum {

	_A,
	_K,
	_Q,
	_J,
	_T,
	_9,
	_8,
	_7,
	_6,
	_5,
	_4,
	_3,
	_2;
	
	public int getNum() {
		switch (this) {
		case _A:
			return 13;
		case _K:
			return 12;
		case _Q:
			return 11;
		case _J:
			return 10;
		case _T:
			return 9;
		case _9:
			return 8;
		case _8:
			return 7;
		case _7:
			return 6;
		case _6:
			return 5;
		case _5:
			return 4;
		case _4:
			return 3;
		case _3:
			return 2;
		default:
			return 1;
		}
	}
}
