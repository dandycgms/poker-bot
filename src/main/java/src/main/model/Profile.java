package src.main.model;

import java.util.ArrayList;
import java.util.List;

import src.main.indicator.Indicator;
import src.main.strategy.enums.ProfilesEnum;
import src.main.utils.Util;

public class Profile {
	
	private Indicator weight;
	
	private Point call, bet;
	private KindOfCurve kindOfCurve;
	
	private List<ProfilesEnum> profiles;
	private List<HandHistory> history;

	public Profile() {
		getProfiles();
		getWeight();
		getHistory();
		call = new Point(Util.rand(), Util.rand(), Util.rand(), Util.rand());
		bet = new Point(Util.rand(), Util.rand(), Util.rand(), Util.rand());
		kindOfCurve = Util.nrand() > 0 ? KindOfCurve.Linear : KindOfCurve.Log;
	}

	public double getLinearCurve(Indicator i, Table table) {
		return weight.getLinCurve(i, table);
	}
	
	public double getLogCurve(Indicator i, Table table) {
		return weight.getLogCurve(i, table);
	}

	public double getCurveAction(Table table, Point act) {
		double curve = getCurveValue(table), amount = 0;
		if (curve <= act.x0)
			amount = act.y0*table.myStack;
		else if (curve >= act.xf)
			amount = act.yf*table.myStack;
		else
			amount = act.y0 + (act.yf - act.y0) * (curve - act.x0  + 0.1)/(act.xf - act.x0 + 0.1);
		return Math.round(amount);
	}

	public double getCurveValue(Table table) {
		Indicator indicator = new Indicator(table);
		return kindOfCurve.equals(KindOfCurve.Linear) ? getLinearCurve(indicator, table) : getLogCurve(indicator, table);
	}
	
	public double getBetAction(Table table) {
		return getCurveAction(table, bet); 
	}
	
	public double getCallAction(Table table) {
		return getCurveAction(table, call);
	}

	public Indicator getWeight() {
		if (weight == null)
			setWeight(new Indicator());
		return weight;
	}

	public void setWeight(Indicator weight) {
		this.weight = weight;
	}
	
	public List<HandHistory> getHistory() {
		if (history == null)
			setHistory(new ArrayList<HandHistory>());
		return history;
	}

	public void setHistory(List<HandHistory> history) {
		this.history = history;
	}

	public List<ProfilesEnum> getProfiles() {
		if (profiles == null)
			setProfiles(new ArrayList<ProfilesEnum>());
		return profiles;
	}

	public void setProfiles(List<ProfilesEnum> profiles) {
		this.profiles = profiles;
	}
	
	public void setDescriptors(KindOfCurve linear, Point power_call, Point power_bet) {
		this.kindOfCurve = linear; 
		this.call = power_call;
		this.bet = power_bet;
	}
	
	public String toStringDescriptors() {
		return kindOfCurve + ", \nBet: <" + bet.x0 + ", " + bet.y0 + "> ~ <" + bet.xf  + ", " + bet.yf + ">, \nCall: <" + call.x0  + ", " + call.y0 + "> ~ <" + call.xf  + ", " + call.yf + ">\n\n"
				+ "d(Profile.KindOfCurve." + kindOfCurve + ", new Profile().new Point("
				+ call.x0 + "," + call.y0 + "," + call.xf  + ", " + call.yf 
				+"), new Profile().new Point("
				+ bet.x0 + "," + bet.y0 + "," + bet.xf  + ", " + bet.yf 
				+ "))";
	}
	
	public enum KindOfCurve {
		Linear, Log;
	}
	
	public class Point {
		public Point(double x0, double y0, double xf, double yf) {
			if (x0 > xf) {
				double aux = x0;
				x0 = xf;
				xf = aux;
			}
			this.x0 = Util.round(x0);
			this.y0 = Util.round(y0);
			this.xf = Util.round(xf);
			this.yf = Util.round(yf);
		}
		double x0, y0, xf, yf;
	}
}