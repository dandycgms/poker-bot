package src.main.model;

import java.util.ArrayList;
import java.util.List;

public class HandHistory {
	
	private List<TableEvent> events;
	
	public HandHistory() {
		// TODO Auto-generated constructor stub
		setEvents(new ArrayList<TableEvent>());
	}

	public List<TableEvent> getEvents() {
		return events;
	}

	public void setEvents(List<TableEvent> events) {
		this.events = events;
	}
	
}
