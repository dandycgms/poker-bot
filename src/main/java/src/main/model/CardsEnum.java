package src.main.model;

import java.util.HashMap;
import java.util.Map;

import src.main.utils.Util;


public enum CardsEnum {

	sA(13,0), hA(13,1), dA(13,2), cA(13,3),
	sK(12,0), hK(12,1), dK(12,2), cK(12,3),
	sQ(11,0), hQ(11,1), dQ(11,2), cQ(11,3),
	sJ(10,0), hJ(10,1), dJ(10,2), cJ(10,3),
	sT(9,0), hT(9,1), dT(9,2), cT(9,3),
	s9(8,0), h9(8,1), d9(8,2), c9(8,3),
	s8(7,0), h8(7,1), d8(7,2), c8(7,3),
	s7(6,0), h7(6,1), d7(6,2), c7(6,3),
	s6(5,0), h6(5,1), d6(5,2), c6(5,3),
	s5(4,0), h5(4,1), d5(4,2), c5(4,3),
	s4(3,0), h4(3,1), d4(3,2), c4(3,3),
	s3(2,0), h3(2,1), d3(2,2), c3(2,3),
	s2(1,0), h2(1,1), d2(1,2), c2(1,3);
	
	private int num, suit;
	
	private CardsEnum(int num, int suit) {
		this.num = num;
		this.suit = suit;
	}
	
	public static Map<CardsEnum,Boolean> getVisitorMap() {
		Map<CardsEnum,Boolean> visitor = new HashMap<CardsEnum, Boolean>();
		for (CardsEnum c : values()) 
			visitor.put(c, false);
		return visitor;
	}
	
	public static CardsEnum getRandomCard() {
		return values()[Util.rand(0, 51)];
	}
	
	public int getNum() {
		return num;
	}

	public int getSuit() {
		return suit;
	}
}
