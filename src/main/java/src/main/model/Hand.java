package src.main.model;

import java.util.Arrays;
import java.util.List;

public class Hand {

	private HandsEnum rank;
	private double score;
	private List<CardsEnum> cards;
	
	public Hand(List<CardsEnum> cards, HandsEnum rank, double score) {
		this.rank = rank;
		this.score = score;
		this.cards = cards;
	}
	
	public static Hand getHandRank(List<CardsEnum> fiveCards) {
		Integer n[] = new Integer[5], s[] = new Integer[5], i = 0;
		for (CardsEnum card : fiveCards) {
			n[i] = card.getNum();
			s[i] = card.getSuit();
			i++;
		}
		Arrays.sort(n);
		Arrays.sort(s);
		boolean hasStraight = n[1]-n[0] == 1 && n[2]-n[1] == 1 && n[3]-n[2] == 1 && 
				(n[4]-n[3] == 1 || (n[3] == 4 && n[4] == 13));
		if (hasStraight && n[3] == 4 && n[4] == 13) {
			n[4] = 0;
			Arrays.sort(n);
		}
		double sum = getPowerHand(n);
		boolean hasFlush = s[0] == s[1] && s[1] == s[2] && s[2] == s[3] && s[3] == s[4];
		if (hasStraight && hasFlush)
			return new Hand(fiveCards,HandsEnum.STRAIGHT_FLUSH, 14*HandsEnum.STRAIGHT_FLUSH.strenght() + sum);
		boolean hasFour = (n[0] == n[1] && n[1] == n[2] && n[2] == n[3]) || 
				  (n[1] == n[2] && n[2] == n[3] && n[3] == n[4]);
		if (hasFour) {
			sum = (14*n[2] + sum)/15;
			return new Hand(fiveCards,HandsEnum.FOUR_OF_A_KIND, 14*HandsEnum.FOUR_OF_A_KIND.strenght() + sum);
		}
		boolean hasFullen = (n[0] == n[1] && n[1] == n[2] && n[3] == n[4]) || 
				   			(n[0] == n[1] && n[2] == n[3] && n[3] == n[4]);
		if (hasFullen) {
			if (n[1] == n[2])
				sum = (14*n[2] + n[3])/15.0;
			else
				sum = (14*n[2] + n[1])/15.0;
			return new Hand(fiveCards,HandsEnum.FULL_HOUSE, 14*HandsEnum.FULL_HOUSE.strenght() + sum);
		}
		if (hasFlush)
			return new Hand(fiveCards,HandsEnum.FLUSH, 14*HandsEnum.FLUSH.strenght() + sum);
		if (hasStraight)
			return new Hand(fiveCards,HandsEnum.STRAIGHT, 14*HandsEnum.STRAIGHT.strenght() + sum);
		boolean hasThree = (n[0] == n[1] && n[1] == n[2]) || 
						   (n[1] == n[2] && n[2] == n[3]) || 
						   (n[2] == n[3] && n[3] == n[4]);
		if (hasThree) {
			if (n[0] == n[1])
				sum = (14*n[0] + sum)/15;
			else if (n[1] == n[2])
				sum = (14*n[1] + sum)/15;
			else
				sum = (14*n[2] + sum)/15;
			return new Hand(fiveCards,HandsEnum.THREE_OF_A_KIND, 14*HandsEnum.THREE_OF_A_KIND.strenght() + sum);
		}
		int countOfPairs = 0, pairs[] = new int[2];
		for (i = 0; i < 4; i++) 
			if (n[i] == n[i+1]) 
				pairs[countOfPairs++] = n[i]; 
		if (countOfPairs == 2) {
			if (n[0] > n[1]) 
				sum = (196*pairs[0] + 14*pairs[1] + sum)/211; 
			else
				sum = (196*pairs[1] + 14*pairs[0] + sum)/211;
			return new Hand(fiveCards,HandsEnum.TWO_PAIR, 14*HandsEnum.TWO_PAIR.strenght() + sum);
		}
		if (countOfPairs == 1) {
			sum = (14*pairs[0] + sum)/15;
			return new Hand(fiveCards,HandsEnum.ONE_PAIR, 14*HandsEnum.ONE_PAIR.strenght() + sum);
		}
		return new Hand(fiveCards,HandsEnum.HIGH_CARD, 14*HandsEnum.HIGH_CARD.strenght() + sum);
	}

	private static double getPowerHand(Integer[] n) {
		Integer i;
		double k = 1;
		double sum = 0;
		for (i = 0; i< n.length; i++) {
			k *= 14;
			sum += n[i] * k; 
		}
		sum /= k;
		return sum;
	}
	
	public double getScore() {
		return score;
	}
	
	public double getStrength() {
		return score/126.0;
	}

	public List<CardsEnum> getCards() {
		return cards;
	}

	public HandsEnum getRank() {
		return rank;
	}

	@Override
	public String toString() {
		return "Hand [rank=" + rank + ", score=" + score + ", cards=" + cards + ", strenght=" + getStrength()
				+ "]";
	}
}
