package src.main.model;

import java.util.List;

import src.main.actions.Action;
import src.main.actions.Listener;
import src.main.indicator.Indicator;
import src.main.kernel.Logics;
import src.main.kernel.Statistics;
import src.main.utils.Bot;
import src.main.utils.Util;

public class Table {

	private Listener  listener;
	
	public List<Integer> id;
	
	public double 	smallBlind, 
					bigBlind, 
					rank, 
					remainingPlayers, 
					totalPlayers,
					largestStack, 
					smallestStack, 
					averageStack,
					preflopStats,
					preflopOrder,
					headsUp,
					call,
					mavgcall,
					pot,
					mavgpot,
					myStack;
	
	public int buttonPosition;
	
	public List<CardsEnum> board, hand;
	
	public boolean myTurn;

	public Table() {}
	
	public Table(Listener listener) {
		this.listener = listener;
	}
	
	public Hand getHandRank() {
		return Logics.getBestHand(board, hand);
	}
	
	public double getHandStrenght() {
		Hand handRank = getHandRank();
		return handRank != null ? handRank.getStrength() : 0;
	}

	public double getImpliedOdds() {
		return (call+0.00001)/(pot+0.00001);
	}
	
	public double getPercCallOverMyStack() {
		return 100.0*call/myStack;
	}

	public double getPercMyStackOverBigBlind() {
		return 100.0*myStack/bigBlind;
	}

	public double getPercMyStackOverAverageStack() {
		return 100.0*myStack/averageStack;
	}
	
	public boolean isPreflop() {
		return board == null || board.get(0) == null || board.get(1) == null || board.get(2) == null;
	}
	
	public boolean isFlop() {
		return board.get(0) != null && board.get(1) != null && board.get(2) != null && board.get(3) == null;
	}
	
	public boolean isTurn() {
		return board.get(0) != null && board.get(1) != null && board.get(2) != null && board.get(3) != null;
	}
	
	public boolean isRiver() {
		return board.get(0) != null && board.get(1) != null && board.get(2) != null && board.get(3) != null && board.get(4) != null;
	}

	public void infoPoint() {
		Bot.leftClick(listener.getInfoPoint());
		Action.wait(500);
		Bot.leftClick(listener.getNothing());
		Action.wait(200);
	}

	public Table update() {
		if (listener.isMyTurn() && !listener.isInfoOn())
			infoPoint();
		listener.calibratePointInfo();
		return update(listener.isMyTurn(), 
				listener.getCall(),
				listener.getMyStack(),
				listener.getRank(),
				listener.getPot(), 
				listener.getStakes(),
				listener.getLargest(),
				listener.getAverage(),
				listener.getSmallest(),
				listener.getTableId(), 
				listener.getBoardCards(), 
				listener.getHandCards(), 
				listener.getButtonPosition(),
				listener.getTotalPlayers());
	}
	
	public Table update(boolean myTurn, Double call, Double myStack,
		   List<Double> rankGeneral, Double pot, List<Double> stakes,
		   double largestStack, double averageStack, double smallestStack,
		   List<Integer> tableId, List<CardsEnum> board, List<CardsEnum> hand,
		   int buttonPosition, Double totalPlayers) {
		this.myTurn = myTurn;
		this.preflopStats = 0.0;
		this.headsUp = 0.0;
		this.preflopOrder = 1000;
		if (myTurn) {
			this.id = tableId;
			TableMap.add(this);
			this.board = board;
			this.hand = hand;
			this.buttonPosition = buttonPosition;
			if (hasHand()) {
				this.preflopStats = Statistics.getPreflopStats(hand.get(0), hand.get(1));
				this.preflopOrder = Statistics.getPreflopOrder(hand.get(0), hand.get(1));
				this.headsUp = Statistics.getHeadsUpPerc(hand);
			}
			this.call = call;
			this.pot = pot;
			this.mavgcall = TableMap.getMovingAverageCall(this);
			this.mavgpot = TableMap.getMovingAveragePot(this);
		}
		this.myStack = myStack;
		if (rankGeneral.size() > 1) {
			this.rank = rankGeneral.get(0);
			this.remainingPlayers = rankGeneral.get(1);
		}
		if (stakes.size() > 1) {
			this.smallBlind = stakes.get(0); 
			this.bigBlind = stakes.get(1);
		}
		this.totalPlayers = totalPlayers;
		this.largestStack = largestStack;
		this.averageStack = averageStack;
		this.smallestStack = smallestStack;
		this.averageStack = averageStack > largestStack ? averageStack/100 : averageStack;
		return this;
	}

	private boolean hasHand() {
		return hand.get(0) != null && hand.get(1) != null;
	}

	public double getPercMyStackOverAverageTableStack() {
		// TODO Auto-generated method stub
		return 0;		
	}

	public double getPercMyStackOverHigherStackBesidesMe() {
		// TODO Auto-generated method stub
		return 0;		
	}

	public double getPercMyStackOverLowerStackBesidesMe() {
		// TODO Auto-generated method stub
		return 0;		
	}

	public double getMovingAverageStackPot() {
		// TODO Auto-generated method stub
		return 0;
	}	

	public int getActivePlayersOnTable(Table table) {
		// TODO Auto-generated method stub
		// número de jogadores na mesa
		return 0;		
	}

	public int getPlayersOnTable(Table table) {
		// TODO Auto-generated method stub
		// número de jogadores na mão
		return 0;		
	}

	public String toStringIndicator() {
		return new Indicator(this).toString();
	}
	
	public String toString() {

		return "Table # " + id + " \n   position=" + rank + "/" + remainingPlayers + " of " + totalPlayers
				+ ", myStack=" + myStack + ", \n   table=" + board + ", hand=(" + hand + ", " 
				+ preflopStats + "%, " + preflopOrder + "ª), " + Statistics.getNumberOfOutHands(this) + " outs ("+ Statistics.getNumberOfOutHandsPerc(this) + "%)" 
				+ "\n   call=" + call + "(CAVG " + Util.round(mavgcall) + "), pot=" + pot
				+ "(PAVG " + Util.round(mavgpot) + "), \n   blind=(" + smallBlind + ", " + bigBlind
				+ "), \n   stacks <" + smallestStack + ", " + averageStack + ", " + largestStack + ">";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Table other = (Table) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
}