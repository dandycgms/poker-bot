package src.main.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TableMap {

	public static final int mmavg = 5; 
	public static Map<Table,List<TableTemperature>> map = new HashMap<Table, List<TableTemperature>>();
	
	public static void add(Table table) {
		if (table != null) {
			if (!map.containsKey(table)) 
				map.put(table, new ArrayList<TableMap.TableTemperature>());
			map.get(table).add(new TableMap().new TableTemperature(table.pot, table.call));
		}
	}

	public static void remove(Table table) {
		if (table != null) 
			map.remove(table);
	}
	
	public static double getMovingAveragePot(Table table) {
		double ammount = 0, count = 0;
		if (map.containsKey(table))
			for (int i = map.get(table).size()-1; i >= 0 && count <= mmavg; i --) {
				ammount += map.get(table).get(i).pot;
				count ++;
			}
		return count > 0 ? ammount/count : 0;
	}

	public static double getMovingAverageCall(Table table) {
		double ammount = 0, count = 0;
		if (map.containsKey(table))
			for (int i = map.get(table).size()-1; i >= 0 && count <= mmavg; i --) {
				ammount += map.get(table).get(i).call;
				count ++;
			}
		return count > 0 ? ammount/count : 0;
	}
	
	private class TableTemperature {
		public TableTemperature(double pot, double call) {
			this.pot = pot;
			this.call = call;
		}

		double pot, call;
	}
}