package src.main.strategy;

import src.main.actions.Action;
import src.main.kernel.Logics;
import src.main.kernel.Statistics;
import src.main.model.NumEnum;
import src.main.model.Table;

public abstract class Strategy {

	private Action action;
	
	public Strategy() {}
	
	public Strategy(Action action) {
		this.action = action;
	}
	
	public void kernel() {
		System.out.print(">>>>>>>> " + getTable() + "\n   ");
		randomize();
		if (isPreflop())
			preflopStrategy();
		else if (isFlop())
			flopStrategy();
		else if (isTurn())
			turnStrategy();
		else 
			riverStrategy();
		getAction().escapePointer();
	}

	protected boolean isRiver() {
		return getTable().isRiver();
	}
	
	protected boolean isTurn() {
		return getTable().isTurn();
	}

	protected boolean isFlop() {
		return getTable().isFlop();
	}

	protected boolean isPreflop() {
		return getTable().isPreflop();
	}

	protected void preflopStrategy() {
		if (isAllinPreflopRequired()) {allin(); return; }
		if (isRaisePreflopRequired()) {raise(getRaisePreflopRequired()); return;}
		if (isBetPreflopRequired()) {raise(getBetPreflopRequired()); return;}
		if (isCallPreflopRequired()) {call(getCallPreflopRequired()); return;}
		check();
	}
	
	protected boolean isRaisePreflopRequired() {
		return isNotNullValue(getRaisePreflopRequired());
	}

	private boolean isNotNullValue(Double x) {
		return x != null && x > 0.00001;
	}
	
	protected boolean isBetPreflopRequired() {
		return isNotNullValue(getBetPreflopRequired());
	}
	
	protected boolean isCallPreflopRequired() {
		return getCallPreflopRequired() > 0;
	}

	protected boolean isRaiseRequired() {
		return isNotNullValue(getRaiseRequired());
	}
	
	protected boolean isBetRequired() {
		return isNotNullValue(getBetRequired());
	}
	
	protected boolean isCallRequired() {
		return getCallRequired() > 0;
	}

	protected void riverStrategy() {
		theSame();
	}

	protected void turnStrategy() {
		theSame();
	}

	protected void flopStrategy() {
		theSame();
	}

	private void theSame() {
		if (isAllinRequired()) 	{allin(); return;}
		if (isRaiseRequired()) 	{raise(roundBlind(getRaiseRequired())); return;}
		if (isBetRequired()) 	{raise(roundBlind(getBetRequired())); return;}
		if (isCallRequired()) 	{call(Math.round(getCallRequired()/blind())*blind()); return;}
		check();
	}

	private double roundBlind(Double x) {
		return Math.round(x/blind())*blind();
	}
	
	protected Table getTable() {
		return action.getTable();
	}

	protected Action getAction() {
		return action;
	}

	protected abstract void randomize();

	protected abstract boolean isAllinPreflopRequired();
	protected abstract Double getRaisePreflopRequired();
	protected abstract Double getBetPreflopRequired();
	protected abstract double getCallPreflopRequired();
	
	protected abstract boolean isAllinRequired();
	protected abstract Double getRaiseRequired();
	protected abstract Double getBetRequired();
	protected abstract double getCallRequired();

	/** ACTIONS **/
	protected void callAny() {
		getAction().callany();
	}
	
	protected void call(double x) {
		getAction().call(getTable(), x);
	}

	protected void check() {
		getAction().check(getTable());
	}
	
	protected void raise(double x) {
		getAction().raisebet(x);
	}

	protected void allin() {
		getAction().allin();
	}

	/** INDICATORS **/
	protected boolean hasAce() {
		return Logics.hasAce(getTable().hand);
	}

	protected boolean hasFigure() {
		return Logics.hasFigure(getTable().hand);
	}
	
	protected boolean hasOnlyFigure() {
		return Logics.hasOnlyFigure(getTable().hand);
	}
	
	protected boolean isSuitConected() {
		return Logics.isSuitConnected(getTable().hand);
	}

	protected boolean isSuited() {
		return Logics.isSuited(getTable().hand);
	}

	protected boolean isConnected() {
		return Logics.isConnected(getTable().hand);
	}
	
	protected boolean isTop(NumEnum n) {
		return Logics.isTop(getTable().hand, n);
	}
	
	protected int buttonPosition() {
		return getTable().buttonPosition;
	}
	
	protected boolean isPocketPair() {
		return Logics.isPocketPair(getTable().hand);
	}

	protected double getHeadsUpPerc() {
		return Statistics.getHeadsUpPerc(getTable().hand);
	}

	protected double getStrenghtImprovementPerc() {
		return Statistics.getStrenghtImprovementPerc(getTable());
	}

	protected double getImpliedOdds() {
		return getTable().getImpliedOdds();
	}
	
	protected double getPercCallOverMyStack() {
		return getTable().getPercCallOverMyStack();
	}

	protected double getPercMyStackOverBigBlind() {
		return getTable().getPercMyStackOverBigBlind();
	}

	protected double getPercMyStackOverAverageStack() {
		return getTable().getPercMyStackOverAverageStack();
	}

	protected double getNumberOfOutHandsPerc() {
		return Statistics.getNumberOfOutHandsPerc(getTable());
	}

	protected int getNumberOfOutHands() {
		return Statistics.getNumberOfOutHands(getTable());
	}

	protected double getRelativeOverCurrentStrenghtImprovementPerc() {
		return Statistics.getStrenghtImprovementOverSupremumPerc(getTable());
	}

	protected double call() {
		return getTable().call;
	}

	protected double preflopOrder() {
		return getTable().preflopOrder;
	}

	protected double preflopStats() {
		return getTable().preflopStats;
	}
	
	protected double blind() {
		return getTable().bigBlind;
	}
	
	protected double sblind() {
		return getTable().smallBlind;
	}

	protected double myStack() {
		return getTable().myStack;
	}
	
	protected double remainingPlayers() {
		return getTable().remainingPlayers;
	}

	protected double rank() {
		return getTable().rank;
	}

	protected double pot() {
		return getTable().pot;
	}

	protected double averageStack() {
		return getTable().averageStack;
	}	

	protected double largestStack() {
		return getTable().largestStack;
	}

	protected double smallestStack() {
		return getTable().smallestStack;
	}
}