package src.main.strategy.enums;

public enum MomentEnum {
	Finals, Late, Medium, Early, Starts;
}
