package src.main.strategy.enums;

public enum ProfilesEnum {

	TIGHTNESS, 		// Quanto se entra numa mão
	AGGRESSIVITY, 	// Quanto se agride 
	BLUFFNESS, 		// Quanto se blefa
	CURIOSITY;		// Quanto se espera boa mão
		
	private ProfilesEnum() {}
	
	private ProfilesEnum(int level) {
		this.level = level;
	}
	
	private double level;
	
	public double level() {
		return level;
	}
}
