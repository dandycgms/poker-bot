package src.main.strategy.enums;

public enum HungryEnum {
	Starving, Greedy, Normal, Pleased, Full;
}
