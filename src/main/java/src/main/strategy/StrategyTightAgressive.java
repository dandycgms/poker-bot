package src.main.strategy;

import src.main.actions.Action;
import src.main.kernel.Logics;
import src.main.model.NumEnum;

public class StrategyTightAgressive extends Strategy {

	double facthand;

	public StrategyTightAgressive(Action action) {
		super(action);
	}
	
	@Override
	protected void randomize() {
		facthand = 0.75 + Math.random() * 0.25;
	}
	
	public boolean isCriticalSituation() {
		return myStack() < averageStack()/10 || (remainingPlayers() > 100 && myStack() <= 10 * blind()) || 
			   rank()/remainingPlayers() >= 0.9;
	}

	public boolean isDangerousSituation() {
		return myStack() > averageStack()/4 || (remainingPlayers() > 100 && myStack() <= 20 * blind()) || 
				   rank()/remainingPlayers() < 0.9;
	}
	
	public boolean isNormalSituation() {
		return myStack() > averageStack()/2 || 
				   rank()/remainingPlayers() < 0.75;
	}

	public boolean isGoodSituation() {
		return myStack() > averageStack() ||
				   rank()/remainingPlayers() <= 0.5;
	}
	
	public boolean isRichSituation() {
		return rank()/remainingPlayers() <= 0.1;
	}


	private boolean isBettable() {
		return (isPreflop() && call() <= blind()) || (!isPreflop() && call() == 0);
	}

	private boolean isRaisable() {
		return (isPreflop() && call() > blind()) || (!isPreflop() && call() > 0);
	}
	
	private boolean isCuriousHand() {
		return buttonPosition() <= 2 && (isTop(NumEnum._T) || (hasAce() && (isSuited() || isTop(NumEnum._8))) || (isSuitConected() && isTop(NumEnum._7)) || (isTop(NumEnum._T) && isConnected()));
	}

	private boolean isWaitingMode() {
		return (remainingPlayers() > 10 && rank() <= 10) || rank()/(remainingPlayers()+0.1) <= 0.1;
	}

	@Override
	protected boolean isAllinPreflopRequired() {
		System.out.println("--------------------------------------------------------------------------------------------------");
		System.out.print(isRichSituation() ? "(R) " : isGoodSituation() ? "(G) " : isNormalSituation() ? "(N) " : isDangerousSituation() ? "(D) " : "(C) ");
		if (isRichSituation())
			return preflopOrder() <= 5;
		if (isGoodSituation())
			return preflopOrder() <= 10;
		if (isNormalSituation())
			return preflopOrder() <= 20;
		if (isDangerousSituation())
			return (preflopOrder() <= 30 && (hasAce() || hasOnlyFigure() || isPocketPair() || getHeadsUpPerc() > 85.0));
		return (preflopOrder() <= 40 && (hasAce() || isPocketPair() || getHeadsUpPerc() > 85.0)) || (preflopStats() >= 75 || getHeadsUpPerc() > 75.0);
	}

	@Override
	protected Double getRaisePreflopRequired() {
		if (isBettable() || isWaitingMode()) 
			return 0.0;
		if (isRichSituation())
			return preflopOrder() <= 10 ? Math.min(myStack()/5, 10*blind()) : 0;
		if (isGoodSituation())
			return preflopOrder() <= 15 ? Math.min(myStack()/4, 20*blind()) : 0;
		if (isNormalSituation())
			return preflopOrder() <= 20 ? Math.min(myStack()/3, 10*blind()) : 0;
		if (isDangerousSituation()) 
			return preflopOrder() <= 30 ? myStack()/2 :
						(getHeadsUpPerc() > 75.0) ? myStack()/5 :
						(preflopOrder() <= 20) 					 ? myStack()/6 :
						(preflopOrder() <= 25 && hasAce()) 		 ? myStack()/7 : 0;
		return (getHeadsUpPerc() > 75.0) ? Math.min(myStack()/5, 10*blind()) :  
			(preflopOrder() <= 20 && call() < myStack()/2) ? myStack()/2 :
			(preflopStats() >= 50 && hasAce()) ? myStack()/4 :
			(isPocketPair() && isTop(NumEnum._8)) ? myStack()/8 : 0;
	}

	@Override
	protected Double getBetPreflopRequired() {
		if (isRaisable() || isWaitingMode())
			return 0.0;
		return rank()/remainingPlayers() > 0.25 && (preflopOrder() <= 30 && (hasAce() || isPocketPair())) ? myStack()/10 : 0;
	}

	@Override
	protected double getCallPreflopRequired() {
		if (isRichSituation())
			return (getHeadsUpPerc() >= 75) ? 5*blind() :
				   ((isPocketPair() && isTop(NumEnum._8)) || preflopOrder() <= 25) ? Math.max(myStack()/10, 10*blind()) :
				   ((hasAce() || isTop(NumEnum._T)) && blind() < myStack()/10) ? 4*blind() : 
					   (hasAce() || isTop(NumEnum._8)) ? sblind() : 0;
		if (isGoodSituation())
			return (getHeadsUpPerc() >= 65) ? 10*blind() :
				   ((isPocketPair() && isTop(NumEnum._7)) || preflopOrder() <= 35) ? Math.max(myStack()/5, 50*blind()) :
				   ((hasAce() || isTop(NumEnum._T)) && blind() < myStack()/5) ? 3*blind() : 
					   (hasAce() || isTop(NumEnum._8)) ? sblind() : 0;
		if (isNormalSituation()) 
			return (getHeadsUpPerc() >= 75.0) ? myStack()/2 :
				   ((isPocketPair() && isTop(NumEnum._9)) || preflopOrder() <= 25) ? Math.max(myStack()/2, 50*blind()) :
				   isCuriousHand() && blind() < myStack()/10 ? blind() : (hasAce() || isTop(NumEnum._8)) ? sblind() : 0;
		if (isDangerousSituation()) 
			return (getTable().getPercCallOverMyStack() >= 0.5 && getImpliedOdds() <= 0.25 && 
			             preflopOrder() <= 65 && (hasAce() || hasOnlyFigure() || (isPocketPair() && isTop(NumEnum._8)))) ? myStack() :  
				   ((isPocketPair() && isTop(NumEnum._T)) || preflopOrder() <= 20 || getHeadsUpPerc() >= 75) ? myStack() :
				   (preflopOrder() <= 50 && (hasAce() || hasOnlyFigure())) ? myStack() :
				   isCuriousHand() && blind() < myStack()/5 ? blind() : 0 ;
		return blind();
	}

	@Override
	protected boolean isAllinRequired() {
		if (isRichSituation())
			return Logics.isNuts(getTable());
		if (isGoodSituation())
			return getNumberOfOutHandsPerc() <= 2.5;
		if (isNormalSituation())
			return getNumberOfOutHandsPerc() <= 5;
		if (isDangerousSituation())
			return getNumberOfOutHandsPerc() <= 10;
		return getNumberOfOutHandsPerc() <= 20.0;
	}

	@Override
	protected Double getRaiseRequired() {
		if (isBettable()) return 0.0;
		if (Logics.isNuts(getTable()) )
			return (call() < myStack()/2) ? Math.max(myStack()/2,2*pot()) : myStack();
		if (isRichSituation()) 
			return getNumberOfOutHandsPerc() <= 5.0 ? myStack()/3 : 0;
		if (isGoodSituation()) 
			return getNumberOfOutHandsPerc() <= 7.5 ? myStack()/5 : 0;
		if (isNormalSituation()) 
			return getNumberOfOutHandsPerc() <= 10.0 ? Math.min(myStack()/8,2*pot()) : 0;
		if (isDangerousSituation()) 
			return getNumberOfOutHandsPerc() <= 15.0 ? (call() < myStack()/10 ? Math.max(myStack()/10,4*pot()) : myStack()/10) : 0;   
		return isTop(NumEnum._T) && getNumberOfOutHandsPerc() <= 20.0 && call() < myStack()/4 ? Math.max(myStack()/4,pot()) : 0;
	}

	@Override
	protected Double getBetRequired() {
		if (isRaisable())
			return 0.0;
		if (Logics.isStraight(getTable()) || Logics.isFlush(getTable()))
			return Math.max(myStack()/2,20*blind());
		if (Logics.isPocketTriple(getTable()))
			return Math.max(myStack()/4,10*blind());
		if (Logics.isCardsUpPocketPair(getTable()) || Logics.isFullDoubleCardsPocketPair(getTable()))
			return Math.max(myStack()/8,5*blind());
		if (Logics.isTopPocketPair(getTable()))
			return Math.max(myStack()/16,3*blind());
		return preflopOrder() < 30 ? blind() : 0;
	}

	@Override
	protected double getCallRequired() {
		if (isWaitingMode())
			return 0;
		if (isRichSituation())
			return  getNumberOfOutHandsPerc() <= 5.0 ? myStack() : 
					(getNumberOfOutHandsPerc() <= 10.0 || (getStrenghtImprovementPerc() > 75.0 && getImpliedOdds() <= 2)) 
					? Math.max(myStack()/2,2*pot()) : 0;
		if (isGoodSituation())
			return  getNumberOfOutHandsPerc() <= 5.0 ? myStack() :
					(getNumberOfOutHandsPerc() <= 7.5 || (getStrenghtImprovementPerc() > 70.0 && getImpliedOdds() <= 2)) 
					? Math.max(myStack()/2,2*pot())
					: 0;
		if (isNormalSituation())
			return  getNumberOfOutHandsPerc() <= 10.0 ? myStack() : 
					(getNumberOfOutHandsPerc() <= 10.0 || (getStrenghtImprovementPerc() > 65.0 && getImpliedOdds() <= 1)) 
					? Math.max(myStack()/2,pot())
					: preflopOrder() < 30 && blind() < myStack()/20 ? blind() : 0;
		if (isDangerousSituation())
			return Logics.isFlush(getTable()) ? myStack() :
				   Logics.isStraight(getTable()) ? myStack() :
				   Logics.isPocketTriple(getTable()) ? myStack() :
				   getImpliedOdds() < 0.1 && getRelativeOverCurrentStrenghtImprovementPerc() > 110.0 && preflopOrder() <= 20  ? myStack() :
				   Logics.isFullDoubleCardsPocketPair(getTable()) ? myStack()/1.5 :
				   Logics.isSingleDoubleCardsPocketPair(getTable()) ? myStack()/1.7 :
				   Logics.isCardsUpPocketPair(getTable()) ? myStack()/2 :
				  (Logics.isTopPocketPair(getTable()) && isTop(NumEnum._T)) ? myStack()/2 :
				  getNumberOfOutHands() <= 100 ? myStack()/3 : 
				  getNumberOfOutHandsPerc() <= 20 ? myStack()/10 :
				  preflopOrder() < 30 && blind() < myStack()/20 ? blind() : 0; 
		double d = 10.0*myStack()/getNumberOfOutHandsPerc();
		return d > blind() ? d : preflopOrder() < 30 ? blind() : preflopOrder() < 30 && blind() < myStack()/20 ? blind() : 0;
	}

}