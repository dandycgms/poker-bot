package src.main.strategy;

import src.main.actions.Action;
import src.main.model.NumEnum;

public class StrategyManiac extends Strategy {

	double facthand;

	public StrategyManiac(Action action) {
		super(action);
	}

	@Override
	protected void randomize() {
		facthand = 0.75 + Math.random() * 0.25;
	}

	protected double stageFactor() {
		return isFlop() ? 1 : isFlop() ? 1.1 : isTurn() ? 1.2 : 1;
	}

	private double riskFact() {
		if (remainingPlayers() > 50)
			return isCriticalSituation() ? 1.0 :
				isDangerousSituation() ? 1.1 :
					isNormalSituation() ? 1.15 :
					isGoodSituation() ? 1.2 : 1.0;
		return isCriticalSituation() ? 1.25 :
			isDangerousSituation() ? 1.2 :
				isNormalSituation() ? 1.15 :
				isGoodSituation() ? 1.1 :
			remainingPlayers() > 25 ? 0.75 : 1.0;
		/* 
		return 	isCriticalSituation() ? 0.2 :
			isDangerousSituation() ? 0.4 :
			isNormalSituation() ? 0.6 :
			isGoodSituation() ? 0.8 :
			remainingPlayers() > 20 ? 1 : remainingPlayers() > 10 ? 0.5 : 0.25; */
		/* 
		return 	isCriticalSituation() ? 1 :
				isDangerousSituation() ? 0.25 :
				isNormalSituation() ? 0.5 :
				isGoodSituation() ? 0.25 :
				remainingPlayers() > 20 ? 1 : remainingPlayers() > 10 ? 0.5 : 0.25;*/
		/*  
		 return isCriticalSituation() ? 2 :
			isDangerousSituation() ? 1.5 :
			isNormalSituation() ? 1 :
			isGoodSituation() ? 0.75 :
			remainingPlayers() > 20 ? 1 : remainingPlayers() > 10 ? 0.5 : 0.25;*/
	}
	
	public boolean isCriticalSituation() {
		return myStack() <= averageStack()/4 || (remainingPlayers() > 100 && myStack() <= 10 * blind()) || 
			   rank()/remainingPlayers() >= 0.8;
	}

	public boolean isDangerousSituation() {
		return myStack() > averageStack()/4 || (remainingPlayers() > 100 && myStack() <= 20 * blind()) || 
				   rank()/remainingPlayers() < 0.65;
	}
	
	public boolean isNormalSituation() {
		return myStack() > averageStack()/2 || 
				   rank()/remainingPlayers() < 0.5;
	}

	public boolean isGoodSituation() {
		return myStack() > averageStack() ||
				   rank()/remainingPlayers() <= 0.35;
	}
	
	public boolean isRichSituation() {
		return rank()/remainingPlayers() <= 0.2;
	}


	private boolean isBettable() {
		return (isPreflop() && call() <= blind()) || (!isPreflop() && call() == 0);
	}

	private boolean isRaisable() {
		return !isBettable();
	}

	private double getInvPerc(double i, double f) {
		return Math.ceil(facthand*(i + riskFact() * (f - i)));
	}
	
	@Override
	protected boolean isAllinPreflopRequired() {
		System.out.println("--------------------------------------------------------------------------------------------------");
		return (preflopOrder() <= getInvPerc(5, 15) && (hasAce() || isPocketPair()));
	}

	@Override
	protected Double getRaisePreflopRequired() {
		if (isBettable() || (!hasAce() && !isPocketPair())) 
			return null;
		return preflopOrder() <=  getInvPerc(10, 30) &&
				(hasAce() || (isPocketPair() && isTop(NumEnum._J))) ? 
						1.0*Math.round(myStack()/4.0) : null;
	}

	@Override
	protected Double getBetPreflopRequired() {
		if (isRaisable())
			return null;
		if (isCriticalSituation() && preflopOrder() <= getInvPerc(15, 30))
			return myStack();
		return preflopOrder() <= getInvPerc(20, 40) && 
				(hasAce() || hasOnlyFigure() || isPocketPair()) ? 
						1.0*Math.round(myStack()/3) : null;
	}

	@Override
	protected double getCallPreflopRequired() {
		if (isCriticalSituation())
			if (preflopOrder() <= getInvPerc(20, 70))
				return myStack();
			else 
				return blind()/2;
		else
			return (preflopOrder() <= getInvPerc(20, 50) && (hasAce() || isPocketPair())) ? myStack() :
					   (preflopOrder() <= getInvPerc(10, 20)) ? 1.0*Math.round(myStack()/2) :
					   (hasOnlyFigure()) ? Math.max(myStack()/2, 20*blind()) :
					   (isPocketPair() && isTop(NumEnum._7)) ? Math.min(myStack()/10, 10*blind()) :
					   (hasAce() && isTop(NumEnum._T)) ? Math.min(myStack()/15, 5*blind()) : 
					   (hasFigure() && isTop(NumEnum._8)) ? Math.min(myStack()/20, blind()) : 
					   ((isPocketPair() || hasAce() || (isSuited() && hasFigure()) || isSuitConected()) && blind() < myStack()/50) 
					   		? blind() : 0;
	}

	@Override
	protected boolean isAllinRequired() {
		return getNumberOfOutHands() <= stageFactor() * getInvPerc(1, 15);
	}

	@Override
	protected Double getRaiseRequired() {
		if (isBettable()) return null;
		return getNumberOfOutHands() <= getInvPerc(10, 30) ? myStack()/4 : null;
	}

	@Override
	protected Double getBetRequired() {
		if (isRaisable()) return null;
		return getNumberOfOutHands() <= stageFactor() * getInvPerc(20, 60) ? myStack()/2 : null;
	}

	@Override
	protected double getCallRequired() {
		return  getNumberOfOutHands() <= stageFactor() * getInvPerc(50, 120) ||
				getStrenghtImprovementPerc() > stageFactor() * getInvPerc(50, 85)
				? myStack() : 0;
	}
}