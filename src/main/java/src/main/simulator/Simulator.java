package src.main.simulator;

import java.util.ArrayList;
import java.util.List;

import src.main.indicator.Indicator;
import src.main.model.Profile;

public class Simulator {
	
	private static final int BLINDS_UP = 20;
	public final static int NUM_OF_PLAYERS_ON_TOURNEY = 1000;
	public final static int NUM_OF_PLAYERS_ON_TABLE = 10;
	public final static double INITIAL_STAKES = 1000;
	
	public final static boolean verbose = false;
	
	private static int vagas = 0;

	protected List<SimProfile> players;
	protected List<SimTable> tables;

	protected Double blind;
	protected Double remainingPlayers;
	protected Double largestStack;
	protected Double smallestStack;
	
	public Simulator() {
		players = new ArrayList<SimProfile>();
		tables = new ArrayList<SimTable>();
	}
	
	public static void main(String[] args) {
		Simulator simulation = new Simulator().setupTourney();
		int count = 0;
		while(simulation.tables.size() > 0) {
			for (SimTable table : simulation.tables) 
				table.simulate();
			simulation.reseating();
			count ++;
			if (count == BLINDS_UP) {
				count = 0;
				simulation.blind = simulation.blind * 2;
			}
		}
	}

	private void reseating() {
		List<SimTable> toRem = new ArrayList<SimTable>();
		remainingPlayers = 0.0;
		largestStack = 0.0;
		smallestStack = Double.MAX_VALUE;
		for (SimTable simTable : tables) {
			remainingPlayers += simTable.getNumberOfPlayers();
			if (smallestStack > simTable.getSmallestStackFromPlayers())
				smallestStack = simTable.getSmallestStackFromPlayers();
			if (largestStack < simTable.getLargestStackFromPlayers())
				largestStack = simTable.getLargestStackFromPlayers();
		}
System.out.print(".");
		for (SimTable simTable : tables) {
			int numberOfPlayers = simTable.getNumberOfPlayers();
			if (tables.size() == 1) {
				if (simTable.getNumberOfPlayers() == 1.0) {
					SimProfile winner = simTable.getWinner();
					System.out.println(winner + ": " + winner.getWeight());
					System.out.println(winner.toStringDescriptors());
					winner.showHistory();
					toRem.add(simTable);
				}		
			} else if (numberOfPlayers <= 5 && vagas >= 5) {
				toRem.add(simTable);
				for (SimProfile player : simTable.getActives())
					reseatingWinner(player, simTable);
System.out.println("\n---------------------------------------- Left: " + remainingPlayers);
			} else
				vagas += NUM_OF_PLAYERS_ON_TABLE - numberOfPlayers;
		}
		if (toRem.size() > 0)
			tables.removeAll(toRem);
	}

	private void reseatingWinner(SimProfile win, SimTable simTable) {
		for (SimTable otable : tables) 
			if (otable != simTable && otable.getNumberOfPlayers() < NUM_OF_PLAYERS_ON_TABLE) { 
				for (int i = 0; i < otable.players.length; i ++) 
					if (otable.players[i] == null) {
						otable.players[i] = win;
						win.setMyPosition(i);
						vagas--;
						break;
					}
				break;
			}
	}
	
	private Simulator setupTourney() {
		for (int i = 0; i < NUM_OF_PLAYERS_ON_TOURNEY; i++)
			players.add(new	 SimProfile(this,INITIAL_STAKES,i%NUM_OF_PLAYERS_ON_TABLE));
		customizePlayer0();
		int id_player = 0;
		for (int i = 0; i < NUM_OF_PLAYERS_ON_TOURNEY/NUM_OF_PLAYERS_ON_TABLE; i++) {
			SimTable simTable = new SimTable(this, i);
			simTable.setPlayers(id_player);
			tables.add(simTable);
			id_player += NUM_OF_PLAYERS_ON_TABLE;
		}
		blind = 20.0;
		remainingPlayers = (double) NUM_OF_PLAYERS_ON_TOURNEY;
		largestStack = INITIAL_STAKES;
		smallestStack = INITIAL_STAKES;
		return this;
	}

	private void customizePlayer0() {
		Indicator weight = new Indicator
				//(1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
				(-0.31, -0.58, -0.05, -0.72, 0.82, 0.24, 0.0, -0.97, -0.03, 0.26, -0.55, -0.81, 0.73, 0.69, -0.38, -0.56, 0.3, 0.57);
		players.get(0).setWeight(weight);
		players.get(0).setDescriptors
				//(Profile.KindOfCurve.Linear, new Profile().new Point(0.5, 0, 0.5, 1), new Profile().new Point(0.75, 0, 0.75, 1));
				(Profile.KindOfCurve.Log, new Profile().new Point(0.73,0.81,0.8, 0.39), new Profile().new Point(0.47,0.88,0.8, 0.38));
	}

}
