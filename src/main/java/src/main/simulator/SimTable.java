package src.main.simulator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import src.main.kernel.Logics;
import src.main.model.CardsEnum;
import src.main.model.Table;

public class SimTable extends Table {
		
	private Simulator simulate;
	
	protected SimProfile [] players;
	private CardsEnum [] board;
	private Map<CardsEnum,Boolean> visitor;
	
	private int id;
	private int buttonPosition;
	private int turn;
	private Double pot;
	private Double call;
	
	public SimTable(Simulator simulate, int id) {
		this.simulate = simulate;
		this.id = id;
		this.buttonPosition = -1;
		this.board = new CardsEnum[5];
	}

	public void setPlayers(int n) {
		players = new SimProfile[Simulator.NUM_OF_PLAYERS_ON_TABLE];
		for (int i = 0; i < Simulator.NUM_OF_PLAYERS_ON_TABLE; i++)
			players[i] = simulate.players.get(n+i); 
	}

	public void simulate() {
		upgrade();
		Map<SimProfile,Double> paidMap = new HashMap<SimProfile, Double>();
		Set<SimProfile> actives = getActives();
		
		for (int i = 0; i < 4; i++) {
			Map<SimProfile,Double> callMap = new HashMap<SimProfile, Double>();
			int lastBetter = loadRound(i);
			if (actives.size() == 1)
				break;
			turn = normPosition(getFirstPosition());
			if (i == 0) {
				SimProfile sb = getSBPlayer();
				pay(sb, Math.min(simulate.blind/2, sb.getMyStack()), callMap);
				SimProfile bb = getBBPlayer();
				pay(bb, Math.min(simulate.blind, bb.getMyStack()), callMap);
			}
			while (lastBetter != turn && actives.size() > 1) {
				SimProfile p =  getPlayer(turn);
				if (p != null) 
					lastBetter = actionPerform(p, lastBetter, callMap, actives);
				turn = normPosition(turn+1);
			}
			for (SimProfile simProfile : actives)
				simProfile.go(i == 0 ? "PF" : i == 1 ? "_F" : i == 2 ? "_T" : "_R");
			for (Entry<SimProfile, Double> entry : callMap.entrySet()) {
				if (!paidMap.containsKey(entry.getKey()))
					paidMap.put(entry.getKey(), 0.0);
				paidMap.put(entry.getKey(), paidMap.get(entry.getKey()) + entry.getValue());
			}
			if (Simulator.verbose) {
				int mm = 0;
				for (SimProfile simProfile : players)
					if (simProfile != null)
						mm += simProfile.getMyStack();
				for (double d : paidMap.values())
					mm += d;
				System.out.println("\n------------------------------------ : " + mm);
			}
		}
		potWinner(paidMap, actives);
		flushPlayers();
	}

	public Set<SimProfile> getActives() {
		Set<SimProfile> actives = new HashSet<SimProfile>();
		for (SimProfile player : players)
			if (player != null) 
				actives.add(player);
		return actives;
	}

	private void flushPlayers() {
		List<Integer> toRem = new ArrayList<Integer>();
		int i = 0;
		for (SimProfile simProfile : players) {
			if (simProfile != null && simProfile.getMyStack() == 0.0)
				toRem.add(i);
			i++;
		}
		for (Integer integer : toRem) {
			players[integer] = null;
		}
	}

	private void pay(SimProfile p, Double callDiff, Map<SimProfile, Double> callMap) {
		p.addPay(callDiff);
		if (!callMap.containsKey(p))
			callMap.put(p, 0.0);
		callMap.put(p, callMap.get(p) + callDiff);
		pot += callDiff;
	}

	private void potWinner(Map<SimProfile, Double> paidMap, Set<SimProfile> actives) {
		if (actives.size() == 1) {
			SimProfile winner = actives.iterator().next();
			winner.addMyStack(pot);
			if (Simulator.verbose)
				for (SimProfile simProfile : players)
					System.out.println(simProfile == null ? "OUT" : simProfile + " ---> " + simProfile.getMyStack());
			return;
		}
		List<SimProfile> champions = new ArrayList<SimProfile>(actives);
		Collections.sort(champions, new Comparator<SimProfile>() {
			public int compare(SimProfile o1, SimProfile o2) {
				return (int)(10000.0*(o2.getBestHand().getScore() - o1.getBestHand().getScore()));
			}
		});
		for (SimProfile winner : champions)
			takePotAway(paidMap, winner);
		if (Simulator.verbose) 
			for (SimProfile simProfile : players)
				System.out.println(simProfile == null ? "OUT" : simProfile + " ---> " + simProfile.getMyStack());
		for (SimProfile simProfile : actives)
			simProfile.go("TK");
	}

	private void takePotAway(Map<SimProfile, Double> paidMap, SimProfile winner) {
		double fact = paidMap.get(winner), pot = 0;
		for (Entry<SimProfile, Double> entry : paidMap.entrySet()) {
			pot += Math.min(fact, entry.getValue());
			entry.setValue(Math.max(entry.getValue()-fact, 0.0));
		}
		//FIXME: falta dar o split pot e o roundBlind
		winner.addMyStack(pot);
	}

	private int actionPerform(SimProfile p, int lastBetter, Map<SimProfile, Double> callMap, Set<SimProfile> actives) {
		Table table = newTable(p);
		p.setBestHand(Logics.getBestHand(table));
		double calldiff = getCallDiff(p, callMap, call);
		if (!actives.contains(p) || p.getMyStack() == 0) 
			return updateLastBetter(p, lastBetter, calldiff);
		double bet = p.getBetAction(table), kall = p.getCallAction(table);
//System.out.print(" bet:" + bet + " kall:" + kall + " calldiff:" + calldiff);
		// bet, a aposta é a diferença; kall: até qto se paga. 
		if (bet > call) {
			call = bet;
			double raise = getCallDiff(p, callMap, bet);
//System.out.print(" raise:" +raise);
			pay(p, raise, callMap);
			lastBetter = turn;
			if (Simulator.verbose) {
				if (calldiff > simulate.blind)
					System.out.println("  > Raise #" + p.getId() + " hand:" + p.getHandCards() + " pot:" + pot);
				else
					System.out.println("  > Bet #" + p.getId() + " hand:" + p.getHandCards() + " pot:" + pot);
			}
		} else if (kall >= calldiff) {
			pay(p, calldiff, callMap);
			lastBetter = updateLastBetter(p, lastBetter, calldiff);
			if (Simulator.verbose) {
				if (calldiff == 0)
					System.out.println("  > Check #" + p.getId() + " hand:" + p.getHandCards() + " pot:" + pot);
				else
					System.out.println("  > Call #" + p.getId() + " hand:" + p.getHandCards() + " pot:" + pot);
			}
		} else {
			actives.remove(p);
			if (Simulator.verbose) 
				System.out.println("  > Fold #" + p.getId() + " hand:" + p.getHandCards() + " pot:" + pot);
		}
//System.out.println("    callmap:" +callMap + '\n');

		return lastBetter;
	}

	private Table newTable(SimProfile p) {
		Table table = new Table().update(true, getCall(), p.getMyStack(), p.getRank(), getPot(), getStakes(), getLargest(), getAverage(), getSmallest(), getTableId(), getBoardCards(), p.getHandCards(), p.getMyButtonPosition(getButtonPosition()), getTotalPlayers());
		return table;
	}

	private int updateLastBetter(SimProfile p, int lastBetter, double calline) {
		if (getBBPlayer().equals(p) && (calline == 0.0 || p.getMyStack() == 0) && lastBetter == -1)
			lastBetter = normPosition(turn + 1);
		return lastBetter;
	}

	private double getCallDiff(SimProfile p, Map<SimProfile, Double> callMap, double v) {
		double amount;
		if (callMap.containsKey(p))
			amount = v - callMap.get(p);
		else
			amount = v;
		return amount;
	}

	private int loadRound(int i) {
		call = i == 0 ? simulate.blind : 0;
		if (i == 1) { // flop
			board[0] = genCard();
			board[1] = genCard();
			board[2] = genCard();
		} else if (i == 2) //turn
			board[3] = genCard();
		else if (i == 3)// river
			board[4] = genCard();
		if (Simulator.verbose) 
			System.out.println("********** " +getBoardCards() + " **********" + simulate.smallestStack + " " + simulate.largestStack);
		return -1;
	}

	private void upgrade() {
		buttonPosition = normPosition(buttonPosition + 1);
		pot = 0.0;
		visitor = CardsEnum.getVisitorMap();
		cardsDistribute();
		for (int i = 0; i < 5; i++) board[i] = null;
	}
	
	private SimProfile getPlayer(int n) {
		return players[normPosition(n)];
	}
	
	private int getFirstPosition() {
		int count = 1;
		for (int i = buttonPosition + 1; i <= buttonPosition + 2*Simulator.NUM_OF_PLAYERS_ON_TABLE; i ++) {
			SimProfile player = getPlayer(i);
			if (player != null) {
				if (count > 2)
					return player.getMyPosition();
				else
					count++;
			}
		}
		return 0;
	}
	
	private SimProfile getBBPlayer() {
		boolean found = false;
		for (int i = buttonPosition + 1; i <= buttonPosition + Simulator.NUM_OF_PLAYERS_ON_TABLE; i ++) {
			SimProfile player = getPlayer(i);
			if (player != null) {
				if (found)
					return player;
				else
					found = true;
			}
		}
		return null;
	}

	private SimProfile getSBPlayer() {
		for (int i = buttonPosition + 1; i <= buttonPosition + Simulator.NUM_OF_PLAYERS_ON_TABLE; i ++) {
			SimProfile player = getPlayer(i);
			if (player != null)
				return player;
		}
		return null;
	}
	
	private int normPosition(int n) {
		return (Simulator.NUM_OF_PLAYERS_ON_TABLE + n) % Simulator.NUM_OF_PLAYERS_ON_TABLE;
	}

	private void cardsDistribute() {
		for (SimProfile p : players) {
			if (p != null) {
				p.setPay(0.0);
				p.setHands(genCard(), genCard());
			}
		}
	}
	
	private CardsEnum genCard() {
		CardsEnum randomCard = CardsEnum.getRandomCard();
		while(visitor.get(randomCard))
			randomCard = CardsEnum.getRandomCard();
		visitor.put(randomCard, true);
		return randomCard;
	}

	/** SIM TOURNEY **/
	private List<Integer> getTableId() {
		return Arrays.asList(id);
	}
	private List<CardsEnum> getBoardCards() {
		return Arrays.asList(board);
	}
	private List<Double> getStakes() {
		return Arrays.asList(simulate.blind/2, simulate.blind);
	}
	private Double getTotalPlayers() {
		return (double)Simulator.NUM_OF_PLAYERS_ON_TOURNEY;
	}
	private Double getLargest() {
		return simulate.largestStack;
	}
	private Double getAverage() {
		return Simulator.INITIAL_STAKES * Simulator.NUM_OF_PLAYERS_ON_TOURNEY / simulate.remainingPlayers;
	}
	private Double getSmallest() {
		return simulate.smallestStack;
	}
	
	public double getLargestStackFromPlayers() {
		double largest = 0;
		for (SimProfile pl : getActives()) {
			if (pl.getMyStack() > largest)
				largest = pl.getMyStack(); 
		}
		return largest;
	}

	public double getSmallestStackFromPlayers() {
		double smallest = Double.MAX_VALUE;
		for (SimProfile pl : getActives()) {
			if (pl.getMyStack() < smallest)
				smallest = pl.getMyStack(); 
		}
		return smallest;
	}

	/** SIM TABLE **/
	private Double getPot() {
		return pot;
	}
	private int getButtonPosition() {
		return buttonPosition; 
	}
	private Double getCall() {
		return call;
	}
	public int getNumberOfPlayers() {
		return getActives().size();
	}
	public SimProfile getWinner() {
		return getNumberOfPlayers() == 1 ? getActives().iterator().next() : null;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((call == null) ? 0 : call.hashCode());
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		SimTable other = (SimTable) obj;
		if (call == null) {
			if (other.call != null)
				return false;
		} else if (!call.equals(other.call))
			return false;
		if (id != other.id)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Table #" + id;
	}

}
