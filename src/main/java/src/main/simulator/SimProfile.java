package src.main.simulator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import src.main.model.CardsEnum;
import src.main.model.Hand;
import src.main.model.Profile;

public class SimProfile extends Profile {

	private static int ID = 0;
	
	private Simulator simulation;
	private int myPosition;
	private int id;

	private Double pay;
	private CardsEnum [] hand;
	private Double myStack;
	private Hand bestHand;
	private List<String> historyHands;
	private List<Double> historyStack;

	public SimProfile(Simulator simulation, double myStack, int position) {
		super();
		this.myStack = myStack;
		this.myPosition = position;
		this.simulation = simulation;
		this.hand = new CardsEnum[2];
		this.id = ID++;
		this.historyHands = new ArrayList<String>();
		this.historyStack = new ArrayList<Double>();
		go("PF");
	}

	public void go(String phase) {
		this.historyHands.add("@" + phase + "<" + hand[0] + ", " + hand[1] + ">");
		this.historyStack.add(myStack);
	}
	
	public void showHistory() {
		for (int i = 0; i < historyHands.size(); i++) 
			System.out.println("  " + this.historyHands.get(i) + ": " + this.historyStack.get(i));
	}
	
	public void setPay(double pay) {
		this.pay = 0.0;
	}
	
	public void addPay(double pay) {
		this.pay += pay;
		this.myStack -= pay; 
	}
	
	public Double getPay() {
		return pay;
	}
	
	public double getMyStack() {
		return myStack;
	}
	
	public void setMyStack(double x) {
		myStack = x;
	}
	
	public void addMyStack(double x) {
		myStack += x;
	}

	public boolean isMyTurn(int turn) { 
		return myPosition == turn;
	}

	public int getId() {
		return id;
	}
	
	public int getMyPosition() {
		return myPosition; 
	}

	public void setMyPosition(int i) {
		myPosition = i;
	}
	
	public int getMyButtonPosition(int buttonPosition) {
		return buttonPosition >= myPosition ? buttonPosition - myPosition : buttonPosition - myPosition + Simulator.NUM_OF_PLAYERS_ON_TABLE; 
	}

	public List<Double> getRank() {
		Double rank = 0.0;
		for (SimProfile p : simulation.players) 
			if (getMyStack() <= p.getMyStack())
				rank ++;
		return Arrays.asList(rank,simulation.remainingPlayers);
	}
	
	public void setHands(CardsEnum c1, CardsEnum c2) {
		hand[0] = c1;
		hand[1] = c2;
	}
	
	public List<CardsEnum> getHandCards() {
		return Arrays.asList(hand);
	}

	public void setBestHand(Hand bestHand) {
		this.bestHand = bestHand;
	}
	
	public Hand getBestHand() {
		return this.bestHand;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SimProfile other = (SimProfile) obj;
		if (id != other.id)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Player #" + id;
	}

}