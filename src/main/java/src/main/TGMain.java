package src.main;

import java.awt.Point;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import src.main.actions.TGAction;
import src.main.graphics.tigergaming.TGCardPointDef;
import src.main.graphics.tigergaming.TGNumberPointDef;
import src.main.strategy.StrategyManiac;
import src.main.utils.Util;
import src.main.utils.WindowReader;

public class TGMain {
	public static void main(String args[]) {
		new TGAction(StrategyManiac.class).tablesOpen(0).run();
		//new TGAction(StrategyTightAgressive.class).tablesOpen(0).run();
		//System.out.println(new WindowReader().showImage().getFeatures(3, 3));
		//cardHandCalibration();
		//System.out.println(TGCardGraph.getHandCards());
		//System.out.println(new TGListener().getHandCards());
		//cardTableCalibration();
		//System.out.println(TGCardGraph.getBoardCards());
		//System.out.println(new TGListener().getBoardCards());
	}

	public static void statsCalibration() {
		Point top = new Point(94,578);
		WindowReader windowReader = new WindowReader(top, new Point(527-94,698-578));

		Integer color[] = {73,225,120};
		Point P = windowReader.findFirstPixmap(color);
		P = Util.sum(P, top);
		 
		TGNumberPointDef.shft = P.x - 5;
		
		windowReader = new WindowReader(new Point(TGNumberPointDef.rank.x + TGNumberPointDef.shft, TGNumberPointDef.rank.y) , new Point(101,120));
		windowReader.showImage();
		windowReader = new WindowReader(new Point(TGNumberPointDef.largest.x + TGNumberPointDef.shft, TGNumberPointDef.largest.y) , new Point(101,120));
		windowReader.showImage();
		windowReader = new WindowReader(new Point(TGNumberPointDef.average.x + TGNumberPointDef.shft, TGNumberPointDef.average.y) , new Point(101,120));
		windowReader.showImage();
		windowReader = new WindowReader(new Point(TGNumberPointDef.smallest.x + TGNumberPointDef.shft, TGNumberPointDef.smallest.y) , new Point(101,120));
		windowReader.showImage();
		windowReader = new WindowReader(new Point(TGNumberPointDef.stakes.x + TGNumberPointDef.shft, TGNumberPointDef.stakes.y) , new Point(101,120));
		windowReader.showImage();		
	}
	
	public static void cardHandCalibration() {
		while (true) {
			new WindowReader(TGCardPointDef.num1Hand, TGCardPointDef.dimCard).showImage();
			new WindowReader(TGCardPointDef.suit1Hand, TGCardPointDef.dimCard).showImage();
			new WindowReader(TGCardPointDef.num2Hand, TGCardPointDef.dimCard).showImage(); 
			new WindowReader(TGCardPointDef.suit2Hand, TGCardPointDef.dimCard).showImage();

			scanf();

			System.out.println("left suit" + new WindowReader(TGCardPointDef.suit1Hand, TGCardPointDef.dimCard).getFeatures(3, 3));
			System.out.println("right suit" + new WindowReader(TGCardPointDef.suit2Hand, TGCardPointDef.dimCard).getFeatures(3, 3));
			System.out.println("left num" + new WindowReader(TGCardPointDef.num1Hand, TGCardPointDef.dimCard).getFeatures(3, 3));
			System.out.println("right num" + new WindowReader(TGCardPointDef.num2Hand, TGCardPointDef.dimCard).getFeatures(3, 3));
			

		}
	}

	public static String scanf() {
		try{
		    BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
		    return bufferRead.readLine();
		}
		catch(IOException e) {
		    e.printStackTrace();
		}
		return null;
	}
	
	public static void cardTableCalibration() {
		while (true) {
			scanf();
			System.out.println("s1" + new WindowReader(TGCardPointDef.suit1Flop, TGCardPointDef.dimCard).getFeatures(3, 3));
			System.out.println("s2" + new WindowReader(TGCardPointDef.suit2Flop, TGCardPointDef.dimCard).getFeatures(3, 3));
			System.out.println("s3" + new WindowReader(TGCardPointDef.suit3Flop, TGCardPointDef.dimCard).getFeatures(3, 3));
			System.out.println("st" + new WindowReader(TGCardPointDef.suitTurn, TGCardPointDef.dimCard).getFeatures(3, 3));
			System.out.println("sr" + new WindowReader(TGCardPointDef.suitRiver, TGCardPointDef.dimCard).getFeatures(3, 3));

			System.out.println("n1" + new WindowReader(TGCardPointDef.num1Flop, TGCardPointDef.dimCard).getFeatures(3, 3));
			System.out.println("n2" + new WindowReader(TGCardPointDef.num2Flop, TGCardPointDef.dimCard).getFeatures(3, 3));
			System.out.println("n3" + new WindowReader(TGCardPointDef.num3Flop, TGCardPointDef.dimCard).getFeatures(3, 3));
			System.out.println("nt" + new WindowReader(TGCardPointDef.numTurn, TGCardPointDef.dimCard).getFeatures(3, 3));
			System.out.println("nr" + new WindowReader(TGCardPointDef.numRiver, TGCardPointDef.dimCard).getFeatures(3, 3));
		}
	}
}